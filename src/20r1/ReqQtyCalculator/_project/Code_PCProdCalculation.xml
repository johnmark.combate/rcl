﻿<Graph ClassName="PCProdCalculation" Source="#CDATA" IsNew="True" FileType="NewDac">
    <CDATA name="Source"><![CDATA[using System;
using PX.Data;
using JAMS.AM;
using PX.Objects.IN;
using JAMS.AM.Attributes;
using System.Collections;
using System.Collections.Generic;

namespace ReqQtyCalculator
{
    /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 8/19/2021 9:10:36 AM
    [PURPOSE]: DAC FOR FORMULA POPUP*/
    [Serializable]
    [PXCacheName("PCProdCalculation")]
    public class PCProdCalculation : IBqlTable
    {
        #region LineNbr
        [PXDBInt(IsKey = true)]
        [PXUIField(DisplayName = "Line Nbr", Visible = false, Enabled = false)]
        [PXLineNbr(typeof(AMProdOper.lineCntrOvhd))]
        public virtual int? LineNbr { get; set; }
        public abstract class lineNbr : PX.Data.BQL.BqlInt.Field<lineNbr> { }
        #endregion

        #region OrderType
        [AMOrderTypeField(IsKey = true, Visible = false, Enabled = false)]
        [PXDBDefault(typeof(AMProdOper.orderType))]
        public virtual string OrderType { get; set; }
        public abstract class orderType : PX.Data.BQL.BqlString.Field<orderType> { }
        #endregion

        #region ProdOrdID
        [ProductionNbr(IsKey = true, Visible = false, Enabled = false)]
        [PXDBDefault(typeof(AMProdOper.prodOrdID))]
        public virtual string ProdOrdID { get; set; }
        public abstract class prodOrdID : PX.Data.BQL.BqlString.Field<prodOrdID> { }
        #endregion

        #region ProdCalcID
        [PXDBString(15, IsKey = true, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Attribute ID")]
        [ProdMatlSelector]
        public virtual string ProdCalcID { get; set; }
        public abstract class prodCalcID : PX.Data.BQL.BqlString.Field<prodCalcID> { }
        #endregion

        #region ProdMatlLineNbr
        [PXDBInt(IsKey = true)]
        [PXUIField(DisplayName = "Production Material Line Nbr.")]
        [PXDefault(typeof(PCProdItem.prodMatlLineNbr))]
        [PXParent(typeof(Select<
            PCProdItem,
            Where<PCProdItem.prodMatlLineNbr, Equal<Current<prodMatlLineNbr>>,
                And<PCProdItem.orderType, Equal<Current<orderType>>,
                And<PCProdItem.prodOrdID, Equal<Current<prodOrdID>>>>>>))]
        public virtual int? ProdMatlLineNbr { get; set; }
        public abstract class prodMatlLineNbr : PX.Data.BQL.BqlInt.Field<prodMatlLineNbr> { }
        #endregion

        #region Value
        [PXDBDecimal(4)]
        [PXUIField(DisplayName = "Value")]
        [PXDefault(TypeCode.Decimal, "0.0000")]
        public virtual Decimal? Value { get; set; }
        public abstract class value : PX.Data.BQL.BqlDecimal.Field<value> { }
        #endregion

        #region Operation
        [PXDBString(1, IsFixed = true, InputMask = "")]
        [PXUIField(DisplayName = "Operation")]
        [PXDefault("+")]
        [PXStringList(new string[] { "+", "-", "/", "*" }, new string[] { "+", "-", "/", "*" })]
        public virtual string Operation { get; set; }
        public abstract class operation : PX.Data.BQL.BqlString.Field<operation> { }
        #endregion

        #region OpenBrackets
        [PXDBString(5, InputMask = "")]
        [PXUIField(DisplayName = "Brackets")]
        [PXStringList(new string[] { "(", "((", "(((", "((((", "(((((" }, new string[] { "(", "((", "(((", "((((", "(((((" })]
        public virtual string OpenBrackets { get; set; }
        public abstract class openBrackets : PX.Data.BQL.BqlString.Field<openBrackets> { }
        #endregion

        #region CloseBrackets
        [PXDBString(5, InputMask = "")]
        [PXUIField(DisplayName = "Brackets")]
        [PXStringList(new string[] { ")", "))", ")))", "))))", ")))))" }, new string[] { ")", "))", ")))", "))))", ")))))" })]
        public virtual string CloseBrackets { get; set; }
        public abstract class closeBrackets : PX.Data.BQL.BqlString.Field<closeBrackets> { }
        #endregion


        #region Tstamp
        [PXDBTimestamp()]
        [PXUIField(DisplayName = "Tstamp")]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion

        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion
    }

    /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 8/19/2021 9:35:45 AM
    [PURPOSE]: CUSTOM SELECTOR TO ONLY SHOW CORRESPONDING ATTRIBUTES SETUP FOR CURRENT ITEM FOR PRODUCTION ORDER MATERIAL*/
    public class ProdMatlSelectorAttribute : PXCustomSelectorAttribute
    {
        public ProdMatlSelectorAttribute()
          : base(typeof(PCProdAttribute.prodCalcID), typeof(PCProdAttribute.prodCalcID), typeof(PCProdAttribute.descr))
        {
            this.DescriptionField = typeof(PCProdAttribute.descr);
        }
        protected virtual IEnumerable GetRecords()
        {
            PXCache prodItemCache = _Graph.Caches[typeof(PCProdItem)];
            PCProdItem prodItem = (PCProdItem)prodItemCache.Current;
            if (prodItem == null) yield return new List<PCProdAttribute>() { };

            AMProdMatl prodMatl = AMProdMatl.PK.Find(this._Graph, prodItem.OrderType, prodItem.ProdOrdID, prodItem.OperationID, prodItem.ProdMatlLineNbr);
            if (prodMatl != null)
            {
                foreach (PCProdAttribute prodAttrib in PXSelect<PCProdAttribute, Where<PCProdAttribute.isActive, Equal<True>>>.Select(this._Graph))
                {
                    PCProdInventoryAnswer answer = PCProdInventoryAnswer.PK.Find(this._Graph, prodMatl.InventoryID, prodAttrib.ProdCalcID);

                    if (answer != null)
                        if (answer.IsActive == true)
                            yield return prodAttrib;
                }
            }
            else
                yield return new List<PCProdAttribute>() { };
        }
    }
}]]></CDATA>
</Graph>