﻿<Graph ClassName="InventoryItemMaintExtPC" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PX.Api;
using PX.Api.Models;
using PX.Common;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.GL;
using PX.Objects.PO;
using PX.Objects.RUTROT;
using PX.Objects.SO;
using PX.Objects.Common.Discount;
using PX.SM;
using PX.Web.UI;
using CRLocation = PX.Objects.CR.Standalone.Location;
using ItemStats = PX.Objects.IN.Overrides.INDocumentRelease.ItemStats;
using SiteStatus = PX.Objects.IN.Overrides.INDocumentRelease.SiteStatus;
using PX.Objects;
using PX.Objects.IN;

using ReqQtyCalculator;

namespace PX.Objects.IN
{
    /*[COMMENT START]
	[DEVELOPER NAME]: JOHN COMBATE
	[DATE TIME]: 8/19/2021 12:38:44 AM
	[PURPOSE]: CUSTOM SELECT FOR THE USER TO SEE ALL ACTIVE ATTRIBUTES BEFORE INPUTTING. JUST LIKE IN ATTRIBUTES STANDARD FUNCTION*/
    public class PXSelectClassPCAttribute : PXSelectBase<PCProdAttribute>
    {
        public PXSelectClassPCAttribute(PXGraph graph)
        {
            _Graph = graph;
            View = new PXView(_Graph, false, new Select<PCProdAttribute>(), new PXSelectDelegate(ViewDelegate));

            _Graph.RowSelected.AddHandler(typeof(PCProdAttribute), PCProdAttributeRowSelected);
            _Graph.FieldUpdated.AddHandler(typeof(PCProdAttribute), typeof(PCProdAttribute.value).Name, PCProdAttributeValueFieldUpdated);
            _Graph.FieldUpdated.AddHandler(typeof(PCProdAttribute), typeof(PCProdAttribute.isActiveAnswer).Name, PCProdAttributeIsActiveAnswerFieldUpdated);
            _Graph.FieldUpdated.AddHandler(typeof(PCProdAttribute), typeof(PCProdAttribute.specialValueAnswer).Name, PCProdAttributeSpecialValueAnswerFieldUpdated);
        }

        protected virtual IEnumerable ViewDelegate()
        {
            PXCache inventoryCache = _Graph.Caches[typeof(InventoryItem)];
            InventoryItem item = (InventoryItem)inventoryCache.Current;

            foreach (PCProdAttribute prodAttribute in PXSelect<
                PCProdAttribute,
                Where<PCProdAttribute.isActive, Equal<True>>>
                .Select(_Graph))
            {
                PCProdInventoryAnswer answer = PXSelect<
                    PCProdInventoryAnswer,
                    Where<PCProdInventoryAnswer.inventoryID, Equal<Required<InventoryItem.inventoryID>>,
                        And<PCProdInventoryAnswer.prodCalcID, Equal<Required<PCProdInventoryAnswer.prodCalcID>>>>>
                    .Select(_Graph, item.InventoryID, prodAttribute.ProdCalcID);
                //PCProdInventoryAnswer answer = PCProdInventoryAnswer.PK.Find(this._Graph, item.InventoryID, prodAttribute.ProdCalcID);
                if (answer != null)
                {
                    prodAttribute.Value = answer.Value;
                    prodAttribute.IsActiveAnswer = answer.IsActive;
                    prodAttribute.SpecialValueAnswer = answer.SpecialValue == null ? PCAttribute.PCAttributeSpecialValue.None : answer.SpecialValue;
                    yield return prodAttribute;
                }
                else
                {
                    prodAttribute.Value = 0m;
                    prodAttribute.IsActiveAnswer = false;
                    prodAttribute.SpecialValueAnswer = prodAttribute.SpecialValue;
                    yield return prodAttribute;
                }
            }
        }

        /*[COMMENT START]
		[DEVELOPER NAME]: JOHN COMBATE
		[DATE TIME]: 8/19/2021 12:39:14 AM
		[PURPOSE]: DISABLE/ENABLE FIELDS FOR DATA VALIDATION*/
        protected virtual void PCProdAttributeRowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            PCProdAttribute answer = (PCProdAttribute)e.Row;
            if (answer == null) return;

            PXUIFieldAttribute.SetEnabled<PCProdAttribute.isActiveAnswer>(_Graph.Caches[typeof(PCProdAttribute)], null, true);
            PXUIFieldAttribute.SetEnabled<PCProdAttribute.prodCalcID>(_Graph.Caches[typeof(PCProdAttribute)], null, false);
            PXUIFieldAttribute.SetEnabled<PCProdAttribute.descr>(_Graph.Caches[typeof(PCProdAttribute)], null, false);
            PXUIFieldAttribute.SetEnabled<PCProdAttribute.value>(_Graph.Caches[typeof(PCProdAttribute)], null, true);
            //PXUIFieldAttribute.SetEnabled<PCProdAttribute.specialValue>(_Graph.Caches[typeof(PCProdAttribute)], null, false);
        }

        /*[COMMENT START]
		[DEVELOPER NAME]: JOHN COMBATE
		[DATE TIME]: 8/19/2021 12:40:08 AM
		[PURPOSE]: TO BE ABLE TO INSERT/UPDATE/DELETE RECORDS IN CACHE WHEN VALUE IS UPDATED*/
        protected virtual void PCProdAttributeValueFieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            PCProdAttribute row = (PCProdAttribute)e.Row;
            if (row == null) return;

            PXCache itemCache = _Graph.Caches[typeof(InventoryItem)];
            InventoryItem item = (InventoryItem)itemCache.Current;
            if (item == null) return;

            //PCProdInventoryAnswer answer = PCProdInventoryAnswer.PK.Find(this._Graph, item.InventoryID, row.ProdCalcID);

            PCProdInventoryAnswer answer = PXSelect<
                PCProdInventoryAnswer,
                Where<PCProdInventoryAnswer.inventoryID, Equal<Required<InventoryItem.inventoryID>>,
                    And<PCProdInventoryAnswer.prodCalcID, Equal<Required<PCProdInventoryAnswer.prodCalcID>>>>>
                .Select(_Graph, item.InventoryID, row.ProdCalcID);
            /*[COMMENT START]
            [DEVELOPER NAME]: JOHN COMBATE
            [DATE TIME]: 8/18/2021 12:40:26 PM
            [PURPOSE]: IF NO ANSWER IS FOUND, INSERT NEW RECORD*/
            if (answer == null)
                cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Insert(
                    new PCProdInventoryAnswer { InventoryID = item.InventoryID, ProdCalcID = row.ProdCalcID, Value = row.Value, SpecialValue = row.SpecialValueAnswer });
            else
            {
                answer.Value = row.Value;
                cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Update(answer);

                answer.SpecialValue = row.SpecialValueAnswer;
                cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Update(answer);
            }
        }

        /*[COMMENT START]
		[DEVELOPER NAME]: JOHN COMBATE
		[DATE TIME]: 8/18/2021 12:40:26 PM
		[PURPOSE]: SAVE ACTIVE STATUS OF ANSWER*/
        protected virtual void PCProdAttributeIsActiveAnswerFieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            PCProdAttribute row = (PCProdAttribute)e.Row;
            if (row == null) return;

            PXCache itemCache = _Graph.Caches[typeof(InventoryItem)];
            InventoryItem item = (InventoryItem)itemCache.Current;
            if (item == null) return;

            //PCProdInventoryAnswer answer = PCProdInventoryAnswer.PK.Find(this._Graph, item.InventoryID, row.ProdCalcID);

            PCProdInventoryAnswer answer = PXSelect<
                PCProdInventoryAnswer,
                Where<PCProdInventoryAnswer.inventoryID, Equal<Required<InventoryItem.inventoryID>>,
                    And<PCProdInventoryAnswer.prodCalcID, Equal<Required<PCProdInventoryAnswer.prodCalcID>>>>>
                .Select(_Graph, item.InventoryID, row.ProdCalcID);

            if (answer == null && row.IsActive == true)
                cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Insert(
                    new PCProdInventoryAnswer { InventoryID = item.InventoryID, ProdCalcID = row.ProdCalcID, Value = row.Value, IsActive = true, SpecialValue = row.SpecialValue });
            else if (answer != null && row.IsActiveAnswer == false)
            {
                    cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Delete(answer);
            }
        }

        /*[COMMENT START]
	    [DEVELOPER NAME]: JOHN COMBATE
	    [DATE TIME]: 9/24/2021 9:58:41 AM
	    [PURPOSE]: SAVE SPECIAL VALUE */
        protected virtual void PCProdAttributeSpecialValueAnswerFieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            PCProdAttribute row = (PCProdAttribute)e.Row;
            if (row == null) return;

            PXCache itemCache = _Graph.Caches[typeof(InventoryItem)];
            InventoryItem item = (InventoryItem)itemCache.Current;
            if (item == null) return;

            //PCProdInventoryAnswer answer = PCProdInventoryAnswer.PK.Find(this._Graph, item.InventoryID, row.ProdCalcID);

            PCProdInventoryAnswer answer = PXSelect<
                PCProdInventoryAnswer,
                Where<PCProdInventoryAnswer.inventoryID, Equal<Required<InventoryItem.inventoryID>>,
                    And<PCProdInventoryAnswer.prodCalcID, Equal<Required<PCProdInventoryAnswer.prodCalcID>>>>>
                .Select(_Graph, item.InventoryID, row.ProdCalcID);

            if (answer == null)
                cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Insert(
                    new PCProdInventoryAnswer { InventoryID = item.InventoryID, ProdCalcID = row.ProdCalcID, Value = row.Value, IsActive = true, SpecialValue = row.SpecialValueAnswer });
            else
            {
                answer.Value = row.Value;
                cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Update(answer);

                answer.SpecialValue = row.SpecialValueAnswer;
                cache.Graph.Caches[typeof(PCProdInventoryAnswer)].Update(answer);
            }
        }
    }

    public class InventoryItemMaintExtPC : PXGraphExtension<InventoryItemMaint>
    {
        public PXSelectClassPCAttribute PCProdAttributes;
        public PXSelect<
            PCProdInventoryAnswer, 
            Where<PCProdInventoryAnswer.inventoryID, Equal<Current<InventoryItem.inventoryID>>>> 
            PCProdAnswers;

        public PXSelect<
            PCProdInventoryAnswer,
            Where<PCProdInventoryAnswer.inventoryID, Equal<Required<InventoryItem.inventoryID>>,
                And<PCProdInventoryAnswer.prodCalcID, Equal<Required<PCProdInventoryAnswer.prodCalcID>>>>>
            PCProdAnswerSelect;

        #region Event Handlers

        #endregion
    }
}]]></CDATA>
</Graph>