<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="AM201010.aspx.cs" Inherits="Page_AM201010" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="ReqQtyCalculator.PCProdAttributeMaint"
        PrimaryView="Attributes"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Primary" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="Attributes">
			    <Columns>
				<px:PXGridColumn Type="CheckBox" DataField="IsActive" Width="60" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ProdCalcID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Descr" Width="180" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SpecialValue" Width="70" />
				<px:PXGridColumn DataField="LastModifiedByID" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CreatedByID" Width="220" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	
		<Mode AllowUpload="True" AllowUpdate="True" ></Mode></px:PXGrid>
</asp:Content>
