<%@ Page Language="C#" Debug="true" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="JO301000.aspx.cs" Inherits="Page_JO301000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="RCLJO.JobOrderEntry"
        PrimaryView="JobOrders"
        >
		<CallbackCommands>
                    <px:PXDSCallbackCommand Name="ClearChecklist" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
                    <px:PXDSCallbackCommand Name="RefreshChecklist" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
                    <px:PXDSCallbackCommand Name="OpenChecklist" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="joAction" /></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="JobOrders" Width="100%" Height="160px" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector2" DataField="JORefNbr" ></px:PXSelector>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox38" DataField="Approved" ></px:PXCheckBox>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox43" DataField="Hold" ></px:PXCheckBox>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit5" DataField="RequestedDate" ></px:PXDateTimeEdit>
			<px:PXLayoutRule runat="server" ID="CstLayoutRule9" ColumnSpan="2" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit6" DataField="Project" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule8" StartColumn="True" ></px:PXLayoutRule>
			<px:PXDropDown runat="server" ID="CstPXDropDown52" DataField="Status" ></px:PXDropDown>
			<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector3" DataField="ReqRefNbr" ></px:PXSelector>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox7" DataField="SampleProvided" ></px:PXCheckBox></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab ID="tab" runat="server" Width="100%" Height="500px" DataSourceID="ds" AllowAutoHide="false">
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	
	
		
		<Items>
			<px:PXTabItem Text="Service Report" >
				<Template>
<px:PXFormView RenderStyle="Simple" ID="formService" runat="server" DataSourceID="ds" DataMember="JobOrders" Width="100%" Height="150px" AllowAutoHide="false">
	<Template>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule59" StartRow="True" ></px:PXLayoutRule>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule60" StartColumn="True" ></px:PXLayoutRule>
		<px:PXLayoutRule ControlSize="M" LabelsWidth="M" runat="server" ID="CstPXLayoutRule67" StartGroup="True" GroupCaption="Service Information" ></px:PXLayoutRule>
		<px:PXDropDown runat="server" ID="CstPXDropDown66" DataField="POType" ></px:PXDropDown>
		<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector65" DataField="PONbr" ></px:PXSelector>
		<px:PXSelector runat="server" ID="CstPXSelector27" DataField="VendorID" ></px:PXSelector>
		<px:PXTextEdit runat="server" ID="CstPXTextEdit28" DataField="VendorID_description" ></px:PXTextEdit>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule62" StartColumn="True" ></px:PXLayoutRule>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule68" StartGroup="True" GroupCaption="Service Information" LabelsWidth="M" ControlSize="M" ></px:PXLayoutRule>
		<px:PXTextEdit Height="80px" TextMode="MultiLine" runat="server" ID="CstPXTextEdit63" DataField="ServDescr" ></px:PXTextEdit></Template></px:PXFormView>
<px:PXFormView RenderStyle="Simple" ID="formFindingRecommend" runat="server" DataSourceID="ds" DataMember="JobOrders" Width="100%" Height="210px" AllowAutoHide="false">
	
						<Template>
								<px:PXLayoutRule runat="server" ID="CstPXLayoutRule6" StartRow="True" ></px:PXLayoutRule>
								<px:PXLayoutRule runat="server" ID="CstPXLayoutRule7" StartColumn="True" ></px:PXLayoutRule>
							<px:PXLayoutRule ControlSize="M" LabelsWidth="M" runat="server" ID="CstPXLayoutRule84" StartGroup="True" GroupCaption="Assessment or Findings" ></px:PXLayoutRule>
<px:PXTextEdit TextMode="MultiLine" Height="70px" runat="server" SuppressLabel="False" ID="CstPXTextEdit87" DataField="Assessment" ></px:PXTextEdit>							<px:PXLayoutRule ControlSize="M" GroupCaption="Recommendation" LabelsWidth="M" runat="server" ID="CstPXLayoutRule85" StartGroup="True" ></px:PXLayoutRule>
								<px:PXTextEdit runat="server" ID="CstPXTextEdit9" DataField="Recommendation" Height="70px" TextMode="MultiLine" ></px:PXTextEdit>
								<px:PXLayoutRule runat="server" ID="CstPXLayoutRule8" StartColumn="True" ></px:PXLayoutRule>
								<px:PXLayoutRule runat="server" ID="CstPXLayoutRule10" StartGroup="True" ControlSize="M" LabelsWidth="M" GroupCaption="Certification of Work" ></px:PXLayoutRule>
								<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit11" DataField="CompletionDate_Date" ></px:PXDateTimeEdit>
								<px:PXDateTimeEdit TimeMode="True" runat="server" ID="CstPXDateTimeEdit12" DataField="CompletionDate_Time" ></px:PXDateTimeEdit>
								<px:PXSelector runat="server" ID="CstPXSelector30" DataField="PreparedBy" ></px:PXSelector>
								<px:PXSelector runat="server" ID="CstPXSelector29" DataField="ConformedBy" ></px:PXSelector>
								<px:PXTextEdit runat="server" ID="CstPXTextEdit15" DataField="AcknowledgedBy" ></px:PXTextEdit>
								<px:PXTextEdit runat="server" ID="CstPXTextEdit16" DataField="NotedBy" ></px:PXTextEdit></Template></px:PXFormView></Template>
</px:PXTabItem>
<px:PXTabItem Text="Approval Details" BindingContext="form" RepaintOnDemand="false">
    <Template>
        <px:PXGrid ID="gridJOApproval" runat="server" DataSourceID="ds" Width="100%" SkinID="DetailsInTab" NoteIndicator="True" Style="left: 0px; top: 0px;">
            <AutoSize Enabled="True" ></AutoSize>
            <Mode AllowAddNew="False" AllowDelete="False" AllowUpdate="False" ></Mode>
            <Levels>
                <px:PXGridLevel DataMember="Approval">
                    <Columns>
                        <px:PXGridColumn DataField="ApproverEmployee__AcctCD" Width="160px" ></px:PXGridColumn>
                        <px:PXGridColumn DataField="ApproverEmployee__AcctName" Width="160px" ></px:PXGridColumn>
                        <px:PXGridColumn DataField="ApprovedByEmployee__AcctCD" Width="100px" ></px:PXGridColumn>
                        <px:PXGridColumn DataField="ApprovedByEmployee__AcctName" Width="160px" ></px:PXGridColumn>
                        <px:PXGridColumn DataField="ApproveDate" Width="90px" ></px:PXGridColumn>
                        <px:PXGridColumn DataField="Status" AllowNull="False" AllowUpdate="False" RenderEditorText="True"></px:PXGridColumn>
                        <px:PXGridColumn DataField="WorkgroupID" Width="150px" ></px:PXGridColumn>
                    </Columns>
                </px:PXGridLevel>
            </Levels>
        </px:PXGrid>
    </Template>
</px:PXTabItem>
							<px:PXTabItem Text="Machine and Equipment Service" >
								<Template>
									<px:PXGrid DataSourceID="ds" SyncPosition="True" SkinID="DetailsInTab" Width="100%" runat="server" ID="gridRCLMachineServ">
						<Levels>
							<px:PXGridLevel DataMember="MachineServices" >
								<Columns>
									<px:PXGridColumn CommitChanges="True" DataField="BranchID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="AssetID" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AssetID_description" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn DataField="FADetails__SerialNumber" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="MachineService" Width="280" ></px:PXGridColumn></Columns>
							
								<RowTemplate>
									<px:PXSelector runat="server" ID="CstPXSelector31" DataField="AssetID" AutoRefresh="True" ></px:PXSelector></RowTemplate></px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True" ></AutoSize>
						</px:PXGrid>
								</Template></px:PXTabItem>
							<px:PXTabItem Text="Infrastructure Service" >
								<Template>
									<px:PXGrid DataSourceID="ds" SyncPosition="True" SkinID="DetailsInTab" Width="100%" runat="server" ID="gridRCLInfraServ">
						<Levels>
							<px:PXGridLevel DataMember="InfraServices" >
								<Columns>
									<px:PXGridColumn CommitChanges="True" DataField="BranchID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="SiteID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InfraService" Width="280" ></px:PXGridColumn></Columns>
							
								<RowTemplate>
									<px:PXSelector runat="server" ID="CstPXSelector32" DataField="SiteID" AutoRefresh="True" ></px:PXSelector></RowTemplate></px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True"></AutoSize>
						</px:PXGrid>
								</Template></px:PXTabItem>
							<px:PXTabItem Text="Bill of Materials" >
								<Template>
									<px:PXGrid DataSourceID="ds" SyncPosition="True" SkinID="DetailsInTab" Width="100%" runat="server" ID="gridRCLBoM">
						<Levels>
							<px:PXGridLevel DataMember="BillOfMaterials">
							
								<Columns>
									<px:PXGridColumn CommitChanges="True" DataField="RecvReceiptType" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="RecvReceiptNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="RecvReceiptLine" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="InventoryID" Width="120" ></px:PXGridColumn>
																<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn>
																<px:PXGridColumn DataField="InventoryItem__BaseUnit" Width="72" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Qty" Width="100" ></px:PXGridColumn>
																<px:PXGridColumn DataField="POReceipt__OwnerID_EPEmployee_acctName" Width="220" ></px:PXGridColumn>
																<px:PXGridColumn DataField="POReceipt__ReceiptDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="RetReceiptType" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="RetReceiptNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="RetReceiptLine" Width="140" ></px:PXGridColumn>
																<px:PXGridColumn DataField="ReturnedBy" Width="140" ></px:PXGridColumn>
																<px:PXGridColumn DataField="ReturnedDate" Width="120" ></px:PXGridColumn></Columns>
								<RowTemplate>
									<px:PXSelector AutoRefresh="True" runat="server" ID="CstPXSelector33" DataField="RecvReceiptLine" ></px:PXSelector>
									<px:PXSelector runat="server" ID="CstPXSelector34" DataField="RecvReceiptNbr" AutoRefresh="True" ></px:PXSelector>
									<px:PXSelector AutoRefresh="True" runat="server" ID="CstPXSelector35" DataField="RetReceiptLine" ></px:PXSelector>
									<px:PXSelector AutoRefresh="True" runat="server" ID="CstPXSelector36" DataField="RetReceiptNbr" ></px:PXSelector></RowTemplate></px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True"></AutoSize>
						</px:PXGrid>
											</Template></px:PXTabItem>
							<px:PXTabItem Text="Labor Hours" >
								<Template>
									<px:PXGrid DataSourceID="ds" SyncPosition="True" SkinID="DetailsInTab" Width="100%" runat="server" ID="gridRCLLabor">
						<Levels>
							<px:PXGridLevel DataMember="LaborHours">
							
								<Columns>
									<px:PXGridColumn DataField="StartDate_Date" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn TimeMode="True" DataField="StartDate_Time" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="EndDate_Date" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn TimeMode="True" DataField="EndDate_Time" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Task" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="IsThirdParty" Width="140" Type="CheckBox" ></px:PXGridColumn>
									<px:PXGridColumn DataField="EmployeeID" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ServicePersonnel" Width="250" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Comments" Width="280" ></px:PXGridColumn></Columns></px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True"></AutoSize>
						
										<Mode AllowUpload="True" ></Mode></px:PXGrid>
											</Template></px:PXTabItem>
							<px:PXTabItem Text="Sanitation Conformance" >
								<Template>
									<px:PXGrid DataSourceID="ds" SyncPosition="True" SkinID="DetailsInTab" Width="100%" runat="server" ID="gridRCLSanitation">
						<Levels>
							<px:PXGridLevel DataMember="SanitationConformances">
							
								<Columns>
									<px:PXGridColumn DataField="Checkpoint" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn Type="CheckBox" DataField="IsYes" Width="60" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Comments" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="CheckedBy" Width="100" ></px:PXGridColumn>
																<px:PXGridColumn DataField="EmployeeName" Width="120" ></px:PXGridColumn></Columns></px:PXGridLevel>
						</Levels>
						<AutoSize Enabled="True"></AutoSize>
						<ActionBar ActionsText="False">
          <CustomItems>
            <px:PXToolBarButton Text="Open Checklist" Visible="True">
              <AutoCallBack Command="OpenChecklist" Target="ds" ></AutoCallBack></px:PXToolBarButton>
            <px:PXToolBarButton Text="Refresh" Visible="True">
              <AutoCallBack Command="RefreshChecklist" Target="ds" ></AutoCallBack></px:PXToolBarButton>
            <px:PXToolBarButton Text="Clear" Visible="True">
              <AutoCallBack Command="ClearChecklist" Target="ds" ></AutoCallBack></px:PXToolBarButton>
</CustomItems></ActionBar>
						</px:PXGrid>
											</Template></px:PXTabItem>
							<px:PXTabItem Text="Related Requests">
								<Template>
									<px:PXGrid SkinID="Details" Width="100%" runat="server" ID="RelatedRQCstPXGrid53">
										<Levels>
											<px:PXGridLevel DataMember="RelatedRQ" >
												<Columns>
													<px:PXGridColumn CommitChanges="True" DataField="RQOrderNbr" Width="140" ></px:PXGridColumn>
													<px:PXGridColumn DataField="RQRequest__ReqClassID" Width="120" />
													<px:PXGridColumn DataField="RQRequest__EmployeeID" Width="70" ></px:PXGridColumn>
													<px:PXGridColumn DataField="RQRequest__EmployeeID_BAccountR_acctName" Width="280" ></px:PXGridColumn>
													<px:PXGridColumn DataField="RQRequest__OrderDate" Width="90" ></px:PXGridColumn>
													<px:PXGridColumn DataField="RQRequest__Description" Width="220" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
										<AutoSize Enabled="True" />
										<AutoSize MinHeight="100" />
										<AutoSize MinWidth="100" /></px:PXGrid></Template></px:PXTabItem>
							<px:PXTabItem Text="Other Information" >
								<Template>

<px:PXFormView RenderStyle="Simple" ID="formOtherInfoJO" runat="server" DataSourceID="ds" DataMember="JobOrders" Width="100%" Height="150px" AllowAutoHide="false">
	<Template>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule50" StartRow="True" ></px:PXLayoutRule>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule51" StartColumn="True" ></px:PXLayoutRule>
		<px:PXSelector runat="server" ID="CstPXSelector48" DataField="WorkgroupID" ></px:PXSelector>
		<px:PXSelector runat="server" ID="CstPXSelector47" DataField="OwnerID" ></px:PXSelector></Template></px:PXFormView>

								</Template></px:PXTabItem></Items>
</px:PXTab>
</asp:Content>