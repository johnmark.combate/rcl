﻿<Graph ClassName="SOOrderEntryExtOTC" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects.TX;
using POLine = PX.Objects.PO.POLine;
using POOrder = PX.Objects.PO.POOrder;
using System.Threading.Tasks;
using PX.CarrierService;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects.AR.CCPaymentProcessing.Common;
using PX.Objects.AR.CCPaymentProcessing.Helpers;
using PX.Objects.AR.CCPaymentProcessing.Interfaces;
using ARRegisterAlias = PX.Objects.AR.Standalone.ARRegisterAlias;
using PX.Objects.AR.MigrationMode;
using PX.Objects.Common;
using PX.Objects.Common.Discount;
using PX.Objects.Common.Extensions;
using PX.Objects.IN.Overrides.INDocumentRelease;
using PX.CS.Contracts.Interfaces;
using Message = PX.CarrierService.Message;
using PX.TaxProvider;
using PX.Data.DependencyInjection;
using PX.LicensePolicy;
using PX.Objects.Extensions.PaymentTransaction;
using PX.Objects.SO.GraphExtensions.CarrierRates;
using PX.Objects.SO.Attributes;
using PX.Objects.Common.Bql;
using PX.Objects;
using PX.Objects.SO;

using OTC;

namespace PX.Objects.SO
{
    public class SOOrderEntryExtOTC : PXGraphExtension<SOOrderEntry>
    {
        public PXSelectJoinGroupBy<
            CSAnswers,
            InnerJoin<InventoryItem,
                On<InventoryItem.noteID, Equal<CSAnswers.refNoteID>>,
            InnerJoin<SOLine,
                On<SOLine.inventoryID, Equal<InventoryItem.inventoryID>>,
            InnerJoin<CSAttribute, 
                On<CSAttribute.attributeID, Equal<CSAnswers.attributeID>>>>>,
            Where<SOLine.orderNbr, Equal<Current<SOOrder.orderNbr>>,
                And<SOLine.orderType, Equal<Current<SOOrder.orderType>>>>, 
            Aggregate<
                GroupBy<CSAnswers.attributeID>>>
            ItemAttributes;

        public PXSelectJoin<
            SOPurchaseOrder, 
            LeftJoin<POOrder, 
                On<POOrder.orderType, Equal<SOPurchaseOrder.pOOrderType>, 
                And<POOrder.orderNbr, Equal<SOPurchaseOrder.pOOrderNbr>>>>,
            Where<SOPurchaseOrder.sOOrderType, Equal<Current<SOOrder.orderType>>, 
                And<SOPurchaseOrder.sOOrderNbr, Equal<Current<SOOrder.orderNbr>>>>> 
            PurchaseOrders;

        #region Event Handlers
        public void SOOrder_UsrExpDate_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (SOOrder)e.Row;
            if (row == null) return;

            var setup = Base.sosetup.Current;
            if (setup == null) return;
            var setupExt = PXCache<SOSetup>.GetExtension<SOSetupExtOTC>(setup);

            var orderType = Base.soordertype.Current;
            if (orderType == null) return;
            var orderTypeExt = PXCache<SOOrderType>.GetExtension<SOOrderTypeExtOTC>(orderType);

            if (orderTypeExt.UsrDisplayExpDate == true)
            {
                double daysToAdd = setupExt.UsrDaysBeforeExp == null ? 0.0 : (double)setupExt.UsrDaysBeforeExp;

                e.NewValue = ((DateTime)(row.OrderDate)).AddDays(daysToAdd);
            }
        }

        public void SOOrder_OrderDate_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (SOOrder)e.Row;
            if (row == null) return;

            cache.SetDefaultExt<SOOrderExtOTC.usrExpDate>(row);
        }

        public void SOOrder_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (SOOrder)e.Row;
            if (row == null) return;

            var orderType = Base.soordertype.Current;
            if (orderType == null) return;
            var orderTypeExt = PXCache<SOOrderType>.GetExtension<SOOrderTypeExtOTC>(orderType);

            PXUIFieldAttribute.SetVisible<SOOrderExtOTC.usrExpDate>(cache, row, orderTypeExt.UsrDisplayExpDate == true);
            
            ItemAttributes.Cache.AllowSelect = orderTypeExt.UsrDisplayAttr == true;
            PurchaseOrders.Cache.AllowSelect = orderTypeExt.UsrConnectPO == true;
        }


        public void SOOrder_RowSelected(PXCache cache, PXRowSelectedEventArgs e, PXRowSelected del)
        {
            if (del != null)
                del(cache, e);

            var row = (SOOrder)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetEnabled<SOOrderExtOTC.usrShowCustomerInvCode>(cache, row, true);
        }

        public void SOOrder_WorkgroupID_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (SOOrder)e.Row;
            if (row == null) return;

            EPEmployeeCompanyTreeMember employeeWorkgroup = PXSelect<
                EPEmployeeCompanyTreeMember, 
                Where<EPEmployeeCompanyTreeMember.userID, Equal<Required<EPEmployeeCompanyTreeMember.userID>>, 
                    And<EPEmployeeCompanyTreeMember.active, Equal<True>>>>
                .Select(Base, Base.Accessinfo.UserID);


            if(employeeWorkgroup != null)
                e.NewValue = employeeWorkgroup.WorkGroupID;
        }

        public void SOOrder_RowInserting(PXCache cache, PXRowInsertingEventArgs e)
        {
            var row = (SOOrder)e.Row;
            if (row == null) return;

            cache.SetDefaultExt<SOOrder.workgroupID>(row);
        }

        public void SOLine_InventoryID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (SOLine)e.Row;
            if (row == null) return;

            ApplyContractedPrice(cache, row);
        }
        
        public void ApplyContractedPrice(PXCache cache, SOLine row)
        {
            SOOrderType orderType = Base.soordertype.Current;
            if (orderType != null)
            {
                var orderTypeExt = PXCache<SOOrderType>.GetExtension<SOOrderTypeExtOTC>(orderType);

                if (orderTypeExt.UsrUseContractedPrice == true)
                {
                    InventoryItem item = PXSelect<
                        InventoryItem,
                        Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>
                        .Select(Base, row.InventoryID);

                    if (item != null)
                    {
                        var itemExt = PXCache<InventoryItem>.GetExtension<InventoryItemExtOTC>(item);

                        if (itemExt.UsrSOContractedPrice != null)
                        {
                            if (itemExt.UsrSOContractedPrice > 0m)
                            {
                                cache.SetValueExt<SOLine.manualPrice>(row, true);
                                cache.SetValueExt<SOLine.curyUnitPrice>(row, itemExt.UsrSOContractedPrice);
                                cache.SetValueExt<SOLineExtOTC.usrContractedPrice>(row, itemExt.UsrSOContractedPrice);
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}]]></CDATA>
</Graph>