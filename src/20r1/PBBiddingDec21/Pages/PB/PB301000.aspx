<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="PB301000.aspx.cs" Inherits="Page_PB301000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="PBBidding.PBBiddingEntry"
        PrimaryView="Bidding"
        >
		<CallbackCommands>
			<px:PXDSCallbackCommand DependOnGrid="CstPXGrid11" Name="SuggestChosenVendor" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="AddInvBySite" Visible="False" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="AddInvSelBySite" Visible="False" CommitChanges="true" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="addRequestLine" Visible="false" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="ViewReqDetails" Visible="false" DependOnGrid="CstPXGrid9" /></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Bidding" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule ControlSize="SM" runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector4" DataField="RefNbr" ></px:PXSelector>
			<px:PXDropDown Enabled="False" runat="server" ID="CstPXDropDown5" DataField="Status" ></px:PXDropDown>
			<px:PXCheckBox CommitChanges="True" runat="server" ID="CstPXCheckBox37" DataField="Hold" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" ID="CstLayoutRule6" ColumnSpan="3" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit3" DataField="Description" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstLayoutRule42" ColumnSpan="3" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit41" DataField="Remarks" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule2" StartColumn="True" ></px:PXLayoutRule>
			<px:PXDateTimeEdit Enabled="False" runat="server" ID="CstPXDateTimeEdit8" DataField="StartDate" ></px:PXDateTimeEdit>
			<px:PXDateTimeEdit Enabled="False" runat="server" ID="CstPXDateTimeEdit7" DataField="EndDate" ></px:PXDateTimeEdit>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox38" DataField="Approved" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule40" StartColumn="True" ></px:PXLayoutRule>
			<px:PXNumberEdit Enabled="False" runat="server" DataField="Totals.OrderTotal" ID="OrderTotalNumberEdit" ></px:PXNumberEdit>
			<px:PXNumberEdit Enabled="False" runat="server" DataField="Totals.TotalQty" ID="TotalQtyNumberEdit" ></px:PXNumberEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" AllowAutoHide="false">
		<Items>
			<px:PXTabItem Text="Items for Bidding">
				<Template>
					<px:PXGrid SyncPosition="True" runat="server" ID="CstPXGrid9" SkinID="Details" Width="100%">
						<Levels>
							<px:PXGridLevel DataMember="Allocations" >
								<Columns>
									<px:PXGridColumn LinkCommand="InventoryItemMaint" DataField="InventoryID" Width="120" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="SiteID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="SiteID_description" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="QtyforBid" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="OpenQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ClosedQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CanceledQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn Type="CheckBox" TextAlign="Center" DataField="IsReq" Width="60" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="RQRequestEntry" DataField="OrderNbr" Width="140" ></px:PXGridColumn></Columns>
								<RowTemplate>
									<px:PXSegmentMask AutoRefresh="True" runat="server" ID="CstPXSegmentMask23" DataField="SiteID" ></px:PXSegmentMask></RowTemplate></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="100" MinWidth="100" ></AutoSize>
						<Mode AllowAddNew="False" ></Mode>
						<ActionBar>
							<CustomItems>
<px:PXToolBarButton Text="Add Item" Key="cmdASI">
                                    <AutoCallBack Command="AddInvBySite" Target="ds">
                                        <Behavior PostData="Page" CommitChanges="True" ></Behavior>
                                    </AutoCallBack>
                                </px:PXToolBarButton>
								<px:PXToolBarButton Text="Add Item Request" Tooltip="Add requested items to bidding" Key="ReqItem">
									<AutoCallBack Target="ds" Command="addRequestLine">
										<Behavior CommitChanges="True" ></Behavior></AutoCallBack></px:PXToolBarButton>
								<px:PXToolBarButton Text="View Request Details">
									<AutoCallBack Target="ds" Command="ViewReqDetails" ></AutoCallBack></px:PXToolBarButton></CustomItems></ActionBar></px:PXGrid></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Bidding Vendors">
				<Template>
					<px:PXGrid SkinID="Details" Width="100%" runat="server" ID="CstPXGrid10">
						<Levels>
							<px:PXGridLevel DataMember="BidVendors" >
								<Columns>
									<px:PXGridColumn LinkCommand="VendorMaint" CommitChanges="True" DataField="VendorID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VendorID_description" Width="220" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize>
						<AutoSize MinHeight="100" ></AutoSize>
						<AutoSize MinWidth="100" ></AutoSize></px:PXGrid></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Bidding Responses" >
				<Template>
					<px:PXFormView SkinID="Transparent" DataMember="Filter" runat="server" ID="CstFormView30">
						<Template>
							<px:PXLayoutRule LabelsWidth="S" ControlSize="SM" runat="server" ID="CstPXLayoutRule31" StartColumn="True" ></px:PXLayoutRule>
							<px:PXSelector AutoRefresh="True" CommitChanges="True" runat="server" ID="CstPXSelector36" DataField="VendorID" ></px:PXSelector>
							<px:PXCheckBox CommitChanges="True" runat="server" ID="CstPXCheckBox35" DataField="ShowWinningVendors" ></px:PXCheckBox>
							<px:PXLayoutRule ControlSize="SM" LabelsWidth="S" runat="server" ID="CstPXLayoutRule32" StartColumn="True" ></px:PXLayoutRule>
							<px:PXSelector AutoRefresh="True" CommitChanges="True" runat="server" ID="CstPXSelector34" DataField="InventoryID" ></px:PXSelector></Template></px:PXFormView>
					<px:PXGrid SyncPosition="True" SkinID="Details" Width="100%" runat="server" ID="CstPXGrid11">
						<Levels>
							<px:PXGridLevel DataMember="BidResponses" >
								<Columns>
									<px:PXGridColumn LinkCommand="VendorMaint" DataField="VendorID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VendorID_description" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="InventoryItemMaint" DataField="PBRequestAlloc__InventoryID" Width="120" ></px:PXGridColumn>
									<px:PXGridColumn DataField="PBRequestAlloc__InventoryID_description" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn DataField="PBRequestAlloc__QtyForBid" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="OrigBidQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="BidAmt" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="BidQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="UOM" Width="72" />
									<px:PXGridColumn CommitChanges="True" Type="CheckBox" TextAlign="Center" DataField="ForRequisition" Width="60" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TotalBidAmt" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TargetCost" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="PrevPOPrice" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="RQRequestEntry" DataField="PBRequestAlloc__OrderNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="TaxZoneID" Width="120" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="TaxCategoryID" Width="120" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DeliveryConditions" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="LeadTime" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Warranty" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TestSchedule" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Incoterms" Width="220" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize>
						<AutoSize MinHeight="100" ></AutoSize>
						<AutoSize MinWidth="100" ></AutoSize>
						<Mode AllowAddNew="False" ></Mode>
						<Mode AllowDelete="False" ></Mode>
						<ActionBar>
							<CustomItems>
								<px:PXToolBarButton>

									<AutoCallBack Target="ds" Command="SuggestChosenVendor" ></AutoCallBack></px:PXToolBarButton>

</CustomItems></ActionBar></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Approval Details" BindingContext="form" RepaintOnDemand="false">
				<Template>
					<px:PXGrid runat="server" SkinID="DetailsInTab" Width="100%" ID="gridApproval" DataSourceID="ds" NoteIndicator="True" Style='left:0px;top:0px;'>
						<AutoSize Enabled="True" ></AutoSize>
						<Mode AllowAddNew="False" AllowUpdate="False" AllowDelete="False" ></Mode>
						<Levels>
							<px:PXGridLevel DataMember="Approval">
								<Columns>
									<px:PXGridColumn DataField="ApproverEmployee__AcctCD" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApproverEmployee__AcctName" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApprovedByEmployee__AcctCD" Width="100px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApprovedByEmployee__AcctName" Width="160px" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApproveDate" Width="90px" ></px:PXGridColumn>
									<px:PXGridColumn RenderEditorText="True" DataField="Status" AllowNull="False" AllowUpdate="False" ></px:PXGridColumn>
									<px:PXGridColumn DataField="WorkgroupID" Width="150px" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Requisitions" RepaintOnDemand="False">
				<Template>
					<px:PXGrid Width="100%" SkinID="Inquire" runat="server" ID="CstPXGrid25">
						<Levels>
							<px:PXGridLevel DataMember="Requisitions" >
								<Columns>
									<px:PXGridColumn LinkCommand="RQRequisitionEntry" DataField="ReqNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="OrderDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Description" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="VendorMaint" DataField="VendorID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VendorID_BAccountR_acctName" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="EPEmployeeMaint" DataField="EmployeeID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="EmployeeID_description" Width="220" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize>
						<AutoSize MinHeight="100" ></AutoSize>
						<AutoSize MinWidth="100" ></AutoSize></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Purchase Orders">
				<Template>
					<px:PXGrid SyncPosition="True" Width="100%" SkinID="Inquire" runat="server" ID="POOrdersGrid">
						<Levels>
							<px:PXGridLevel DataMember="POOrders" >
								<Columns>
									<px:PXGridColumn DataField="OrderType" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="POOrderEntry" DataField="OrderNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="OrderDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="" DataField="VendorID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="VendorID_BAccountR_acctName" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn DataField="OrderDesc" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="OrderTotal" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize>
						<AutoSize MinHeight="100" ></AutoSize>
						<AutoSize MinWidth="100" ></AutoSize>
						<Mode AllowAddNew="False" ></Mode>
						<Mode AllowDelete="False" ></Mode>
						<Mode AllowUpdate="False" ></Mode></px:PXGrid></Template></px:PXTabItem></Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	</px:PXTab>
	<px:PXSmartPanel runat="server" DesignView="Hidden" Height="500px" Width="1000px" ID="PanelAddSiteStatus" LoadOnDemand="true" CaptionVisible="true" Caption="Inventory Lookup" Key="sitestatus" AutoCallBack-Enabled="True" AutoCallBack-Target="formSitesStatus" AutoCallBack-Command="Refresh">
		<px:PXFormView runat="server" ID="formSitesStatus" SkinID="Transparent" CaptionVisible="False" Width="100%" DataSourceID="ds" DataMember="sitestatusfilter">
			<Template>
				<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" ></px:PXLayoutRule>
				<px:PXSegmentMask CommitChanges="True" runat="server" ID="CstPXSegmentMask24" DataField="InventoryID" ></px:PXSegmentMask>
				<px:PXTextEdit Visible="False" runat="server" CommitChanges="True" DataField="BarCode" ID="edBarCode" ></px:PXTextEdit>
				<px:PXCheckBox Visible="False" runat="server" Checked="True" CommitChanges="True" DataField="OnlyAvailable" ID="chkOnlyAvailable" ></px:PXCheckBox>
				<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" ></px:PXLayoutRule>
				<px:PXSegmentMask runat="server" DataField="SiteID" CommitChanges="True" ID="edSiteID" ></px:PXSegmentMask>
				<px:PXSegmentMask runat="server" DataField="ItemClass" CommitChanges="True" ID="edItemClassID" ></px:PXSegmentMask>
				<px:PXSegmentMask runat="server" DataField="SubItem" AutoRefresh="true" CommitChanges="True" ID="edSubItem" ></px:PXSegmentMask></Template></px:PXFormView>
		<px:PXGrid runat="server" Height="135px" SkinID="Details" Width="100%" ID="gripSiteStatus" AutoAdjustColumns="true" BatchUpdate="true" DataSourceID="ds" AdjustPageSize="Auto" AllowSearch="True" FastFilterFields="InventoryCD,Descr" FastFilterID="edInventory" Style='left:0px;top:0px;'>
			<ClientEvents AfterCellUpdate="UpdateItemSiteCell" ></ClientEvents>
			<AutoSize Enabled="true" ></AutoSize>
			<ActionBar PagerVisible="False" ></ActionBar>
			<Levels>
				<px:PXGridLevel DataMember="siteStatus">
					<Mode AllowAddNew="false" AllowDelete="false" ></Mode>
					<RowTemplate>
						<px:PXSegmentMask runat="server" DataField="ItemClassID" ID="editemClass" ></px:PXSegmentMask></RowTemplate>
					<Columns>
						<px:PXGridColumn Type="CheckBox" TextAlign="Center" DataField="Selected" AutoCallBack="true" AllowNull="False" AllowCheckAll="true" ></px:PXGridColumn>
						<px:PXGridColumn TextAlign="Right" DataField="QtySelected" AllowNull="False" ></px:PXGridColumn>
						<px:PXGridColumn DataField="SiteID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ItemClassID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="ItemClassDescription" ></px:PXGridColumn>
						<px:PXGridColumn DataField="PriceClassID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="PriceClassDescription" ></px:PXGridColumn>
						<px:PXGridColumn DataField="PreferredVendorID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="PreferredVendorDescription" ></px:PXGridColumn>
						<px:PXGridColumn Width="120" LinkCommand="InventoryItemMaint" DisplayFormat=">AAAAAAAAAA" DataField="InventoryCD" ></px:PXGridColumn>
						<px:PXGridColumn DisplayFormat=">AA-A-A" DataField="SubItemID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Descr" ></px:PXGridColumn>
						<px:PXGridColumn DisplayFormat=">aaaaaa" DataField="PurchaseUnit" ></px:PXGridColumn>
						<px:PXGridColumn TextAlign="Right" DataField="QtyAvailExt" AllowNull="False" ></px:PXGridColumn>
						<px:PXGridColumn TextAlign="Right" DataField="QtyOnHandExt" AllowNull="False" ></px:PXGridColumn>
						<px:PXGridColumn TextAlign="Right" DataField="QtyPOOrdersExt" AllowNull="False" ></px:PXGridColumn>
						<px:PXGridColumn TextAlign="Right" DataField="QtyPOReceiptsExt" AllowNull="False" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels></px:PXGrid>
		<px:PXPanel runat="server" SkinID="Buttons" ID="PXPanel1">
			<px:PXButton runat="server" Text="Add" SyncVisible="false" CommandSourceID="ds" CommandName="AddInvSelBySite" ID="PXButton6" ></px:PXButton>
			<px:PXButton runat="server" Text="Add &amp; Close" DialogResult="OK" ID="PXButton7" ></px:PXButton>
			<px:PXButton runat="server" Text="Cancel" DialogResult="Cancel" ID="PXButton8" ></px:PXButton></px:PXPanel></px:PXSmartPanel>
	<px:PXSmartPanel runat="server" DesignView="Content" Height="470px" Width="900px" ID="pnlAddRequest" LoadOnDemand="true" ShowAfterLoad="true" CaptionVisible="true" Caption="Select Requested Items" Key="SourceRequests" AutoCallBack-Enabled="True" AutoCallBack-Target="formRequest" AutoCallBack-Command="Refresh" CallBackMode-CommitChanges="True" CallBackMode-PostData="Page">
		<px:PXFormView runat="server" ID="formRequest" SkinID="Transparent" CaptionVisible="False" BorderWidth="0px" Width="100%" DataSourceID="ds" DataMember="RequestFilter">
			<Template>
				<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="S" ControlSize="M" ></px:PXLayoutRule>
				<px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
				<px:PXSelector runat="server" DataField="OwnerID" Size="m" CommitChanges="True" ID="edOwnerID" ></px:PXSelector>
				<px:PXCheckBox runat="server" Checked="True" CommitChanges="True" DataField="MyOwner" ID="chkMyOwner" ></px:PXCheckBox>
				<px:PXLayoutRule runat="server" Merge="False" ></px:PXLayoutRule>
				<px:PXLayoutRule runat="server" Merge="True" ></px:PXLayoutRule>
				<px:PXSelector runat="server" DataField="WorkGroupID" Size="m" CommitChanges="True" ID="edWorkGroupID" ></px:PXSelector>
				<px:PXCheckBox runat="server" CommitChanges="True" DataField="MyWorkGroup" ID="chkMyWorkGroup" ></px:PXCheckBox>
				<px:PXLayoutRule runat="server" Merge="False" ></px:PXLayoutRule>
				<px:PXCheckBox runat="server" Checked="True" CommitChanges="True" DataField="AddExists" ID="chkAddExists" ></px:PXCheckBox>
				<px:PXSegmentMask runat="server" DataField="InventoryID" AllowEdit="True" CommitChanges="True" ID="edInventoryID" ></px:PXSegmentMask>
				<px:PXSegmentMask runat="server" DataField="SubItemID" AutoRefresh="true" Size="xs" CommitChanges="True" ID="edSubItemID" ></px:PXSegmentMask>
				<px:PXLayoutRule runat="server" StartColumn="True" LabelsWidth="SM" ControlSize="M" ></px:PXLayoutRule>
				<px:PXSelector runat="server" DataField="ReqClassID" CommitChanges="True" ID="edReqClassID" ></px:PXSelector>
				<px:PXDropDown runat="server" CommitChanges="True" DataField="SelectedPriority" AllowNull="False" ID="edSelectedPriority" ></px:PXDropDown>
				<px:PXSegmentMask runat="server" DataField="EmployeeID" ValueField="AcctCD" AutoRefresh="true" CommitChanges="True" ID="edRequesterID" ></px:PXSegmentMask>
				<px:PXSelector runat="server" DataField="DepartmentID" CommitChanges="True" ID="edDepartmentID" ></px:PXSelector>
				<px:PXSegmentMask runat="server" DataField="VendorID" CommitChanges="True" ID="edVendorID" ></px:PXSegmentMask>
				<px:PXTextEdit runat="server" CommitChanges="True" DataField="Description" ID="edDescription" ></px:PXTextEdit></Template></px:PXFormView>
		<px:PXGrid BatchUpdate="False" runat="server" SkinID="Inquire" Width="100%" ID="addRequest" DataSourceID="ds" AllowPaging="true" AdjustPageSize="Auto" Style='left:0px;top:0px;'>
			<AutoSize Enabled="true" ></AutoSize>
			<Mode AllowAddNew="false" AllowDelete="false" ></Mode>
			<Levels>
				<px:PXGridLevel DataMember="SourceRequests">
					<Columns>
						<px:PXGridColumn Type="CheckBox" TextAlign="Center" DataField="Selected" AutoCallBack="true" AllowNull="False" AllowCheckAll="True" ></px:PXGridColumn>
						<px:PXGridColumn RenderEditorText="True" DataField="RQRequest__Priority" AllowNull="False" ></px:PXGridColumn>
						<px:PXGridColumn DisplayFormat=">aaaaaaaaaa" DataField="RQRequest__ReqClassID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="RQRequest__OrderNbr" ></px:PXGridColumn>
						<px:PXGridColumn DataField="RQRequest__EmployeeID" AllowUpdate="false" ></px:PXGridColumn>
						<px:PXGridColumn TextAlign="Right" DataField="LineNbr" Visible="False" ></px:PXGridColumn>
						<px:PXGridColumn Width="120" DisplayFormat=">AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" DataField="InventoryID" ></px:PXGridColumn>
						<px:PXGridColumn DisplayFormat=">A" DataField="SubItemID" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Description" ></px:PXGridColumn>
						<px:PXGridColumn DisplayFormat=">aaaaaa" DataField="UOM" ></px:PXGridColumn>
						<px:PXGridColumn DataField="RQRequestLine__UsrBidOpenQty" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn TextAlign="Right" DataField="ReqQty" AllowNull="False" AllowUpdate="False" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels></px:PXGrid>
		<px:PXPanel runat="server" SkinID="Buttons" ID="SrcReqPanel">
			<px:PXButton runat="server" Text="Save" DialogResult="OK" ID="btnSave0" ></px:PXButton>
			<px:PXButton runat="server" Text="Cancel" DialogResult="Cancel" ID="btnCancel0" ></px:PXButton></px:PXPanel></px:PXSmartPanel>
	<px:PXSmartPanel AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" AutoCallBack-Target="CstPXGrid27" Width="1200" Height="300" Caption="Request Details" CaptionVisible="True" Key="ReqDetails" runat="server" ID="CstSmartPanel26">
		<px:PXGrid Width="100%" SkinID="Inquire" runat="server" ID="CstPXGrid27">
			<Levels>
				<px:PXGridLevel DataMember="ReqDetails" >
					<Columns>
						<px:PXGridColumn LinkCommand="RQRequestEntry" DataField="OrderNbr" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Description" Width="280" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LineNbr" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UOM" Width="72" ></px:PXGridColumn>
						<px:PXGridColumn DataField="OrderQty" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UsrBidOpenQty" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryEstExtCost" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="RequestedDate" Width="90" ></px:PXGridColumn>
						<px:PXGridColumn DataField="PromisedDate" Width="90" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CreatedByID" Width="220" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
			<AutoSize Enabled="True" ></AutoSize>
			<AutoSize MinHeight="100" ></AutoSize>
			<AutoSize MinWidth="100" ></AutoSize></px:PXGrid>
		<px:PXPanel SkinID="Buttons" runat="server" ID="CstPanel28">
			<px:PXButton DialogResult="OK" Text="OK" runat="server" ID="CstButton29" ></px:PXButton></px:PXPanel></px:PXSmartPanel></asp:Content>