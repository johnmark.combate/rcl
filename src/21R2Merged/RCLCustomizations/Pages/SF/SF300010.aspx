<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="SF300010.aspx.cs" Inherits="Page_SF300010" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="SF.SalesForecastEntry"
        PrimaryView="Forecast"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Forecast" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector4" DataField="RefNbr" ></px:PXSelector>
			<px:PXCheckBox CommitChanges="True" runat="server" ID="CstPXCheckBox10" DataField="Hold" ></px:PXCheckBox>
			<px:PXDropDown CommitChanges="True" runat="server" ID="CstPXDropDown11" DataField="Status" ></px:PXDropDown>
			<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit3" DataField="ForecastDate" ></px:PXDateTimeEdit>
			<px:PXLayoutRule runat="server" ID="CstLayoutRule33" ColumnSpan="2" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit2" DataField="Description" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule7" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector1" DataField="CustomerID" CommitChanges="True" AllowEdit="True" ></px:PXSelector>
			<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector35" DataField="ClusterID" ></px:PXSelector>
			<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector9" DataField="SiteID" ></px:PXSelector>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit40" DataField="ExtRef" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule17" StartColumn="True" ></px:PXLayoutRule>
			<px:PXFormView runat="server" ID="CstFormView18" DataMember="Filter" >
				<Template>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit19" DataField="Apr" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit20" DataField="Aug" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit21" DataField="Dec" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit22" DataField="Feb" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit23" DataField="Jan" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit24" DataField="Jul" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit25" DataField="Jun" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit26" DataField="Mar" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit27" DataField="May" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit28" DataField="Nov" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit29" DataField="Oct" ></px:PXNumberEdit>
					<px:PXNumberEdit CommitChanges="True" runat="server" ID="CstPXNumberEdit30" DataField="Sep" ></px:PXNumberEdit></Template></px:PXFormView>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule36" StartColumn="True" ></px:PXLayoutRule>
			<px:PXNumberEdit runat="server" ID="CstPXNumberEdit37" DataField="OpenQty" Enabled="False" ></px:PXNumberEdit>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox31" DataField="Approved" Enabled="False" ></px:PXCheckBox>
			<px:PXCheckBox Enabled="False" runat="server" ID="CstPXCheckBox32" DataField="Rejected" ></px:PXCheckBox>
			<px:PXNumberEdit runat="server" Enabled="False" DataField="Filter.LineCount" ID="LineCountPXNumberEdit37" /></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" AllowAutoHide="false">
		<Items>
			<px:PXTabItem Text="Document Details">
				<Template>
					<px:PXGrid SkinID="Details" Width="100%" runat="server" ID="ParentLine" SyncPosition="true">
						<Levels>
							<px:PXGridLevel DataMember="Lines" >
								<Columns>
									<px:PXGridColumn CommitChanges="True" DataField="InventoryID" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InventoryID_InventoryItem_descr" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Uom" Width="72" ></px:PXGridColumn>
									<px:PXGridColumn DataField="BaseUOM" Width="72" ></px:PXGridColumn>
									<px:PXGridColumn DataField="UnitPrice" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Qty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="AvailQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="QtyOnHand" Width="100" />
									<px:PXGridColumn CommitChanges="True" DataField="ProdLedger" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="ItemBalances" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="ForDelivery" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DelForecast" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Remaining" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Jan" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Feb" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Mar" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Apr" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="May" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Jun" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Jul" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Aug" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Sep" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Oct" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Nov" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="Dec" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="TotalProd" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="TotalPrice" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="TotalBaseUnit" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="OpenQty" Width="100" ></px:PXGridColumn></Columns>
								<RowTemplate>
									<px:PXSegmentMask runat="server" ID="CstPXSegmentMask12" DataField="InventoryID" AllowEdit="True" ></px:PXSegmentMask></RowTemplate></px:PXGridLevel></Levels>
						<AutoSize MinHeight="300" Enabled="True" ></AutoSize>
						<AutoCallBack Enabled="True" Target="CstPXGrid41" Command="Refresh" ActiveBehavior="True" >
							<Behavior CommitChanges="False" RepaintControlsIDs="CstPXGrid41" ></Behavior></AutoCallBack>
						<AutoCallBack Command="Refresh" ></AutoCallBack>
						<AutoCallBack Target="ChildLine" ></AutoCallBack>
						<Mode AllowUpload="True" /></px:PXGrid>
					<px:PXGrid Caption="Forecast per Month" CaptionVisible="True" runat="server" ID="CstPXGrid41" SkinID="Details" Width="100%">
						<Levels>
							<px:PXGridLevel DataMember="FMonths" >
								<Columns>
									<px:PXGridColumn CommitChanges="True" LinkCommand="InventoryItemMaint" DataField="InventoryID" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="ForecastMonth" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="StartDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="EndDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="QtToProduce" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="150" ></AutoSize>
						<Mode AllowUpload="True" InitNewRow="True" ></Mode></px:PXGrid></Template>
			</px:PXTabItem>
			<px:PXTabItem Visible="False" Text="Forecast Per Month">
				<Template>
					<px:PXGrid Width="100%" SkinID="Details" runat="server" ID="ChildLine">
						<Levels>
							<px:PXGridLevel DataMember="FMonths" >
								<Columns>
									<px:PXGridColumn LinkCommand="InventoryItemMaint" CommitChanges="True" DataField="InventoryID" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="ForecastMonth" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="StartDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="EndDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="QtToProduce" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize>
						<Mode AllowUpload="True" ></Mode></px:PXGrid></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Approval Details" >
				<Template>
					<px:PXGrid Width="100%" SkinID="Inquire" runat="server" ID="CstPXGrid34">
						<Levels>
							<px:PXGridLevel DataMember="Approval" >
								<Columns>
									<px:PXGridColumn DataField="ApproverEmployee__AcctCD" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApproverEmployee__AcctName" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApprovedByEmployee__AcctCD" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApprovedByEmployee__AcctName" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ApproveDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="WorkgroupID" Width="180" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Sales Order Summary" >
				<Template>
					<px:PXGrid SkinID="Inquire" Width="100%" runat="server" ID="CstPXGrid38">
						<Levels>
							<px:PXGridLevel DataMember="SFContent" >
								<Columns>
									<px:PXGridColumn DataField="OrderType" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="OrderNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="SOOrder__OrderDate" Width="90" />
									<px:PXGridColumn DataField="SOOrder__CustomerID" Width="140" />
									<px:PXGridColumn DataField="SOOrder__Status" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="InventoryID" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Qty" Width="100" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize></px:PXGrid></Template></px:PXTabItem></Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	</px:PXTab>
</asp:Content>