<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="SF501000.aspx.cs" Inherits="Page_SF501000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="SF.SalesForecastProcess"
        PrimaryView="Filter"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Filter" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule2" StartColumn="True" />
			<px:PXSegmentMask CommitChanges="True" Size="M" runat="server" ID="CstPXSegmentMask1" DataField="CustomerID" ></px:PXSegmentMask></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="PrimaryInquire" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="OrdersToForecast">
			    <Columns>
				<px:PXGridColumn Type="CheckBox" AllowCheckAll="True" DataField="UsrSFSelected" Width="30" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__OrderType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="SOOrderEntry" DataField="SOOrder__OrderNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__CustomerID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__CustomerID_BAccountR_acctName" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__OrderDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="InventoryItemMaint" DataField="InventoryID" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="OrderQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="UsrSFVarianceQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="UOM" Width="72" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SiteID" Width="140" /></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
