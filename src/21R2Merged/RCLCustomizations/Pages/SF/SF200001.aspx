<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="SF200001.aspx.cs" Inherits="Page_SF200001" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="SF.ClusterMaint"
        PrimaryView="Line"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Primary" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="Line">
			    <Columns>
				<px:PXGridColumn DataField="ClusterCD" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ClusterName" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="DfltSiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="DfltSiteID_description" Width="220" />
				<px:PXGridColumn Type="CheckBox" DataField="IsActive" Width="60" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" />
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
