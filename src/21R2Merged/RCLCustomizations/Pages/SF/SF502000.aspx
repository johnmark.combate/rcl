<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="SF502000.aspx.cs" Inherits="Page_SF502000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="SF.SFAllocateSOProcess"
        PrimaryView="SOLines"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid SkinID="PrimaryInquire" runat="server" ID="CstPXGridSOLines" Height="150px" Width="100%">
		<Levels>
			<px:PXGridLevel DataMember="SOLines" >
				<Columns>
					<px:PXGridColumn Type="CheckBox" DataField="UsrSFSelected" Width="60" ></px:PXGridColumn>
					<px:PXGridColumn DataField="OrderType" Width="70" ></px:PXGridColumn>
					<px:PXGridColumn LinkCommand="SOOrderEntry" DataField="OrderNbr" Width="140" ></px:PXGridColumn>
					<px:PXGridColumn DataField="SOOrder__CustomerID" Width="140" ></px:PXGridColumn>
					<px:PXGridColumn DataField="SOOrder__CustomerID_BAccountR_acctName" Width="280" ></px:PXGridColumn>
					<px:PXGridColumn DataField="SOOrder__OrderDate" Width="90" ></px:PXGridColumn>
					<px:PXGridColumn DataField="InventoryID" Width="70" ></px:PXGridColumn>
					<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn>
					<px:PXGridColumn DataField="OrderQty" Width="100" ></px:PXGridColumn>
					<px:PXGridColumn DataField="UsrSFVarianceQty" Width="100" ></px:PXGridColumn>
					<px:PXGridColumn DataField="UOM" Width="72" ></px:PXGridColumn>
					<px:PXGridColumn DataField="SiteID" Width="140" ></px:PXGridColumn>
					<px:PXGridColumn Type="CheckBox" DataField="UsrSelected" Width="60" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
		<AutoSize Enabled="True" MinHeight="150" Container="Window" ></AutoSize>
		<AutoSize Enabled="True" ></AutoSize>
		<AutoSize MinHeight="150" ></AutoSize></px:PXGrid></asp:Content>
