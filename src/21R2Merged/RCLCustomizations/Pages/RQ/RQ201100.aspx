<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="RQ201100.aspx.cs" Inherits="Page_RQ201100" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="PettyCash.RQRequestTypeMaint"
        PrimaryView="RequestTypes"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="RequestTypes" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXSelector Size="SM" runat="server" ID="CstPXSelector4" DataField="RQTypeCD" CommitChanges="True" ></px:PXSelector>
			<px:PXTextEdit Size="L" runat="server" ID="CstPXTextEdit1" DataField="Description" ></px:PXTextEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab DataMember="CurrentRequestType" ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" AllowAutoHide="false">
		<Items>
			<px:PXTabItem Text="Request Classes">
				<Template>
					<px:PXGrid runat="server" ID="CstPXGrid3" SkinID="Details" Width="100%">
						<Levels>
							<px:PXGridLevel DataMember="RequestTypeClasses">
								<Columns>
									<px:PXGridColumn DataField="ReqClassID" Width="120" CommitChanges="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ReqClassID_description" Width="220" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="100" MinWidth="100" ></AutoSize></px:PXGrid></Template>
			</px:PXTabItem></Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	</px:PXTab>
</asp:Content>
