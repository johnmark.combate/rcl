<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="AR308000.aspx.cs" Inherits="Page_AR308000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="BOC.BillOfCollectionEntry"
        PrimaryView="Collection"
        >
		<CallbackCommands>
			<px:PXDSCallbackCommand Visible="False" Name="clearDocs" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Visible="False" Name="addDocs" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand DependOnGrid="CstPXGridARDocs" Visible="False" Name="removeDoc" ></px:PXDSCallbackCommand></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Collection" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector6" DataField="RefNbr" ></px:PXSelector>
			<px:PXSegmentMask runat="server" ID="CstPXSegmentMask4" DataField="CustomerID" ></px:PXSegmentMask>
			<px:PXSegmentMask runat="server" ID="CstPXSegmentMask2" DataField="BranchID" ></px:PXSegmentMask>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit3" DataField="CounterDate" ></px:PXDateTimeEdit>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit5" DataField="DateSubmitted" ></px:PXDateTimeEdit>
			<px:PXLayoutRule runat="server" ID="CstLayoutRule8" ColumnSpan="2" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit9" DataField="Remarks" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule10" StartColumn="True" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit12" DataField="Location" ></px:PXTextEdit>
			<px:PXSegmentMask runat="server" ID="CstPXSegmentMask13" DataField="PreparedBy" ></px:PXSegmentMask>
			<px:PXSegmentMask runat="server" ID="CstPXSegmentMask11" DataField="Collector" ></px:PXSegmentMask>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit15" DataField="ReceiverName" ></px:PXTextEdit>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit14" DataField="ReceivedDate" ></px:PXDateTimeEdit></Template>
	</px:PXFormView></asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" AllowAutoHide="false">
		<Items>
			<px:PXTabItem Text="Transactions">
				<Template>
					<px:PXGrid SkinID="Details" Width="100%" DataSourceID="ds" runat="server" ID="CstPXGridARDocs">
						<Levels>
							<px:PXGridLevel DataMember="ARDocs" >
								<Columns>
									<px:PXGridColumn DataField="DocType" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="RefNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="FinPeriodID" Width="72" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DocDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DueDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryOrigDocAmt" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ARInvoice__TermsID" Width="120" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ARInvoice__InvoiceNbr" Width="180" ></px:PXGridColumn>
									<px:PXGridColumn DataField="UsrRemarks" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn DataField="UsrExtRefNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ARInvoice__UsrGRRef" Width="140" ></px:PXGridColumn></Columns>
								<RowTemplate>
									<px:PXSelector runat="server" ID="CstPXSelector18" DataField="RefNbr" AllowEdit="True" ></px:PXSelector></RowTemplate></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize>
						<ActionBar>
							<CustomItems>
								<px:PXToolBarButton>
									<AutoCallBack Command="AddDocs" Target="ds" ></AutoCallBack></px:PXToolBarButton>
								<px:PXToolBarButton >
									<AutoCallBack Target="ds" Command="RemoveDoc" ></AutoCallBack>
									<AutoCallBack Target="ds" ></AutoCallBack></px:PXToolBarButton>
								<px:PXToolBarButton >
									<AutoCallBack Target="ds" Command="ClearDocs" ></AutoCallBack>
									<AutoCallBack Target="ds" ></AutoCallBack></px:PXToolBarButton></CustomItems></ActionBar></px:PXGrid></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Follow Up">
				<Template>
					<px:PXGrid Width="100%" DataSourceID="" SkinID="Details" runat="server" ID="CstPXGridFollowUp">
						<Levels>
							<px:PXGridLevel DataMember="FollowUp" >
								<Columns>
									<px:PXGridColumn DataField="Remarks" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn DataField="FollowUpDate" Width="90" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize></px:PXGrid></Template>
			</px:PXTabItem>
		</Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	</px:PXTab>
	<px:PXSmartPanel AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" AutoCallBack-Target="BCDetails" Caption="Update Details" CaptionVisible="True" CommandSourceID="ds" Key="Details" Width="450" runat="server" ID="CstSmartPanel19">
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule20" StartRow="True" ></px:PXLayoutRule>
		<px:PXFormView RenderStyle="Simple" runat="server" ID="BCDetails" DataMember="Details" >
			<Template>
				<px:PXLayoutRule runat="server" ID="CstPXLayoutRule22" StartColumn="True" ></px:PXLayoutRule>
				<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector24" DataField="PreparedBy" ></px:PXSelector>
				<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit23" DataField="DateSubmitted" ></px:PXDateTimeEdit>
				<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit25" DataField="ReceivedDate" ></px:PXDateTimeEdit>
				<px:PXLayoutRule runat="server" ID="CstPXLayoutRule26" StartColumn="True" ></px:PXLayoutRule>
				<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector27" DataField="Collector" ></px:PXSelector>
				<px:PXTextEdit CommitChanges="True" runat="server" ID="CstPXTextEdit28" DataField="ReceiverName" ></px:PXTextEdit></Template></px:PXFormView>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule29" StartRow="True" ></px:PXLayoutRule>
		<px:PXPanel RenderStyle="Simple" runat="server" ID="CstPanel30">
			<px:PXButton DialogResult="OK" Text="Ok" runat="server" ID="CstButton31" ></px:PXButton>
			<px:PXButton runat="server" ID="CstButton32" DialogResult="Cancel" Text="Cancel" ></px:PXButton></px:PXPanel></px:PXSmartPanel>
	<px:PXSmartPanel AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" AutoCallBack-Target="AddDocs" Caption="Add Documents" CaptionVisible="True" CommandSourceID="ds" Height="450" Key="DocsToAdd" runat="server" ID="CstSmartPanel37">
		<px:PXGrid SkinID="Inquire" AllowPaging="False" runat="server" ID="AddDocs">
			<Levels>
				<px:PXGridLevel DataMember="DocsToAdd" >
					<Columns>
						<px:PXGridColumn CommitChanges="True" Type="CheckBox" AllowCheckAll="True" DataField="Selected" Width="60" ></px:PXGridColumn>
						<px:PXGridColumn DataField="DocType" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn DataField="RefNbr" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="DocDate" Width="90" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn DataField="FinPeriodID" Width="72" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryOrigDocAmt" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Customer__AcctCD" Width="140" ></px:PXGridColumn></Columns>
					<RowTemplate>
						<px:PXSelector AllowEdit="True" runat="server" ID="CstPXSelector42" DataField="RefNbr" ></px:PXSelector></RowTemplate></px:PXGridLevel></Levels>
			<AutoSize Enabled="True" ></AutoSize></px:PXGrid>
		<px:PXPanel runat="server" ID="CstPanel39">
			<px:PXButton DialogResult="OK" Text="Ok" runat="server" ID="CstButton40" ></px:PXButton>
			<px:PXButton Text="Cancel" DialogResult="Cancel" runat="server" ID="CstButton41" ></px:PXButton></px:PXPanel></px:PXSmartPanel></asp:Content>