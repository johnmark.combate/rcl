<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="AR501010.aspx.cs" Inherits="Page_AR501010" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="BOC.CreateBillOfCollectionProcess"
        PrimaryView="Transactions"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Filter" Width="100%" Height="100px" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule18" StartColumn="True" />
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector Enabled="True" Size="M" CommitChanges="True" runat="server" ID="CstPXSelector2" DataField="BranchID" ></px:PXSelector>
			<px:PXTextEdit CommitChanges="True" runat="server" ID="CstPXTextEdit4" DataField="Location" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstLayoutRule5" ColumnSpan="4" ></px:PXLayoutRule>
			<px:PXDropDown CommitChanges="True" runat="server" ID="CstPXDropDown17" DataField="SelectedCustomers" ></px:PXDropDown>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule6" StartColumn="True" ></px:PXLayoutRule>
			<px:PXCheckBox CommitChanges="True" AlignLeft="True" runat="server" ID="CstPXCheckBox7" DataField="IncludeINV" ></px:PXCheckBox>
			<px:PXCheckBox CommitChanges="True" AlignLeft="True" runat="server" ID="CstPXCheckBox9" DataField="IncludePY" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule10" StartColumn="True" ></px:PXLayoutRule>
			<px:PXCheckBox CommitChanges="True" AlignLeft="True" runat="server" ID="CstPXCheckBox12" DataField="IncludeDM" ></px:PXCheckBox>
			<px:PXCheckBox CommitChanges="True" AlignLeft="True" runat="server" ID="CstPXCheckBox11" DataField="IncludeCM" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule13" StartColumn="True" ></px:PXLayoutRule>
			<px:PXCheckBox CommitChanges="True" AlignLeft="True" runat="server" ID="CstPXCheckBox14" DataField="IncludePM" ></px:PXCheckBox>
			<px:PXCheckBox AlignLeft="True" CommitChanges="True" runat="server" ID="CstPXCheckBox15" DataField="LoadDocs" ></px:PXCheckBox></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid AllowPaging="False" ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Details" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="Transactions">
			    <Columns>
				<px:PXGridColumn CommitChanges="True" Type="CheckBox" AllowCheckAll="True" DataField="Selected" Width="40" ></px:PXGridColumn>
				<px:PXGridColumn DataField="RefNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="DocType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CustomerID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CustomerID_BAccountR_acctName" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="DocDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="FinPeriodID" Width="72" ></px:PXGridColumn>
				<px:PXGridColumn DataField="DueDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CuryOrigDocAmt" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ARInvoice__TermsID" Width="120" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ARInvoice__InvoiceNbr" Width="180" ></px:PXGridColumn>
				<px:PXGridColumn DataField="BranchID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="UsrExtRefNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="UsrBOCRefNbr" Width="140" ></px:PXGridColumn></Columns>
			
				<RowTemplate>
					<px:PXSelector runat="server" ID="CstPXSelector16" DataField="RefNbr" AllowEdit="True" /></RowTemplate></px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
