<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EP103500.aspx.cs" Inherits="Page_EP103500" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="FleetCard.EPFleetCardDetailMaint"
        PrimaryView="FleetCards"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="FleetCards" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector3" DataField="FleetCardCD" /></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab DataMember="CurrentFleetCard" ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" AllowAutoHide="false">
		<Items>
			<px:PXTabItem Text="General Info">
				<Template>
					<px:PXLayoutRule runat="server" ID="CstPXLayoutRule4" StartColumn="True" ></px:PXLayoutRule>
					<px:PXTextEdit runat="server" ID="CstPXTextEdit8" DataField="FleetCardNo" ></px:PXTextEdit>
					<px:PXSegmentMask runat="server" ID="CstPXSegmentMask5" DataField="CardHolder" ></px:PXSegmentMask>
					<px:PXNumberEdit runat="server" ID="CstPXNumberEdit7" DataField="CreditLimit" ></px:PXNumberEdit>
					<px:PXSegmentMask runat="server" ID="CstPXSegmentMask14" DataField="VendorID" />
					<px:PXSelector runat="server" ID="CstPXSelector6" DataField="CreatedByID" ></px:PXSelector>
					<px:PXSelector runat="server" ID="CstPXSelector9" DataField="LastModifiedByID" ></px:PXSelector></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Vehicle Info">
				<Template>
					<px:PXGrid runat="server" ID="CstPXGrid13" SkinID="Details" Width="100%">
						<Levels>
							<px:PXGridLevel DataMember="Trucks" >
								<Columns>
									<px:PXGridColumn DataField="TruckName" Width="180" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="100" MinWidth="100" ></AutoSize></px:PXGrid></Template>
			</px:PXTabItem>
		</Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	</px:PXTab>
</asp:Content>
