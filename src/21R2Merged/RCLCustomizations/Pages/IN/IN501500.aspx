<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="IN501500.aspx.cs" Inherits="Page_IN501500" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="CreateIssuance.CreateIssuanceProcess"
        PrimaryView="Requests"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Filter" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule5" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector4" DataField="ReqNbr" ></px:PXSelector>
			<px:PXSegmentMask CommitChanges="True" runat="server" ID="CstPXSegmentMask3" DataField="InventoryID" ></px:PXSegmentMask>
			<px:PXSegmentMask CommitChanges="True" runat="server" ID="CstPXSegmentMask2" DataField="EmployeeID" ></px:PXSegmentMask>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule6" StartColumn="True" ></px:PXLayoutRule>
			<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit8" DataField="StartDate" ></px:PXDateTimeEdit>
			<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit7" DataField="EndDate" ></px:PXDateTimeEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid SyncPosition="True" ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="PrimaryInquire" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="Requests">
			    <Columns>
				<px:PXGridColumn Type="CheckBox" AllowCheckAll="True" DataField="Selected" Width="30" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="RQRequestEntry" DataField="ReqNbr" Width="120" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" LinkCommand="InventoryMaint" DataField="InventoryID" Width="120" ></px:PXGridColumn>
				<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="OrderQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="SiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="QtyOnHand" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="QtyOnIssues" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="AvailableIssueQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="QtyToIssue" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ReqClassID" Width="120" />
				<px:PXGridColumn DataField="ReqDesc" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="EmployeeID_description" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ReqDate" Width="90" ></px:PXGridColumn></Columns>
			
				<RowTemplate>
					<px:PXSegmentMask MinDropHeight="800" MaxDropHeight="1200" CommitChanges="True" AutoRefresh="True" runat="server" ID="CstPXSegmentMask1" DataField="SiteID" ></px:PXSegmentMask></RowTemplate></px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
