﻿<Graph ClassName="SOOrderEntryExtSalesPriceByItem" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects.TX;
using POLine = PX.Objects.PO.POLine;
using POOrder = PX.Objects.PO.POOrder;
using System.Threading.Tasks;
using PX.CarrierService;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects.AR.CCPaymentProcessing.Common;
using PX.Objects.AR.CCPaymentProcessing.Helpers;
using PX.Objects.AR.CCPaymentProcessing.Interfaces;
using ARRegisterAlias = PX.Objects.AR.Standalone.ARRegisterAlias;
using PX.Objects.AR.MigrationMode;
using PX.Objects.Common;
using PX.Objects.Common.Discount;
using PX.Objects.Common.Extensions;
using PX.Objects.IN.Overrides.INDocumentRelease;
using PX.CS.Contracts.Interfaces;
using Message = PX.CarrierService.Message;
using PX.TaxProvider;
using PX.Data.DependencyInjection;
using PX.LicensePolicy;
using PX.Objects.Extensions.PaymentTransaction;
using PX.Objects.SO.GraphExtensions.CarrierRates;
using PX.Objects.SO.Attributes;
using PX.Objects.Common.Bql;
using PX.Objects;
using PX.Objects.SO;

namespace PX.Objects.SO
{
    public class SOOrderEntryExtSalesPriceByItem : PXGraphExtension<SOOrderEntry>
    {
        #region Event Handlers
        public void SOLine_OrderQty_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e)
        {
            var row = (SOLine)e.Row;
            if (row == null) return;

            decimal? newPrice = 0m;
            var setup = Base.sosetup.Current;
            if (setup == null) return;
            var setupExt = PXCache<SOSetup>.GetExtension<SOSetupExtSalesPriceByItem>(setup);

            if (setupExt.UsrComputeSalesPriceByItem == true)
            {
                /* Copied and modified from SOLine_CuryUnitPrice_FieldDefaulting in SOOrderEntry */
                bool isPriceUpdateNeeded;
                using (var priceScope = Base.GetPriceCalculationScope())
                    isPriceUpdateNeeded = priceScope.IsUpdateNeeded<SOLine.inventoryID>();

                if (row.TranType == INTranType.Transfer)
                {
                    newPrice = 0m;
                }
                else if (row.InventoryID != null && row.IsFree != true && !sender.Graph.IsCopyPasteContext
                && isPriceUpdateNeeded)
                {
                    /* Get total qty for the current item */
                    decimal? totalItemQty = Base.Transactions.Select().RowCast<SOLine>().Where(r => r.InventoryID == row.InventoryID).Sum(q => q.OrderQty);

                    string customerPriceClass = ARPriceClass.EmptyPriceClass;
                    Location c = Base.location.Select();
                    if (!string.IsNullOrEmpty(c?.CPriceClassID))
                        customerPriceClass = c.CPriceClassID;
                    CurrencyInfo curyInfo = Base.currencyinfo.Select();

                    ARSalesPriceMaint salesPriceMaint = ARSalesPriceMaint.SingleARSalesPriceMaint;
                    bool alwaysFromBaseCury = salesPriceMaint.GetAlwaysFromBaseCurrencySetting(sender);
                    ARSalesPriceMaint.SalesPriceItem priceItem = null;
                    try
                    {
                        priceItem = salesPriceMaint.FindSalesPrice(
                        sender,
                        customerPriceClass,
                        row.CustomerID,
                        row.InventoryID,
                        row.SiteID,
                        curyInfo.BaseCuryID,
                        alwaysFromBaseCury ? curyInfo.BaseCuryID : curyInfo.CuryID,
                        Math.Abs(totalItemQty ?? 0m), /* Use total qty for the computation of sales price of the item */
                        row.UOM,
                        Base.Document.Current.OrderDate.Value);
                    }
                    catch (PXUnitConversionException)
                    {
                    }

                    decimal? price = salesPriceMaint.AdjustSalesPrice(sender, priceItem, row.InventoryID, curyInfo, row.UOM);
                    newPrice = price ?? 0m;

                    ARSalesPriceMaint.CheckNewUnitPrice<SOLine, SOLine.curyUnitPrice>(sender, row, price);

                    if (priceItem?.UOM != row.UOM || priceItem?.CuryID != Base.Document.Current.CuryID)
                    {
                        priceItem = null;
                    }
                    row.PriceType = priceItem?.PriceType;
                    row.IsPromotionalPrice = priceItem?.IsPromotionalPrice ?? false;

                    /* Update lines with the same item */
                    bool isRefreshNeed = false;
                    foreach (SOLine line in Base.Transactions.Select().RowCast<SOLine>().Where(r => r.InventoryID == row.InventoryID))
                    {
                        line.CuryUnitPrice = newPrice;

                        Base.Transactions.Cache.MarkUpdated(line);

                        isRefreshNeed = true;
                    }

                    if (isRefreshNeed)
                        Base.Transactions.View.RequestRefresh();
                }
            }
        }
        #endregion
    }
}]]></CDATA>
</Graph>