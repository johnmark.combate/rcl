﻿<Graph ClassName="RQRequestEntryP2P" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Linq;
using PX.Data;
using PX.Objects.AP;
using System.Collections;
using PX.Objects.IN;
using PX.Objects.AR;

using P2P;
using PX.SM;
using PX.Objects.SO;

namespace PX.Objects.RQ
{
    public class RQRequestEntryP2P : PXGraphExtension<RQRequestEntry>
    {
        public PXSelectJoin<
            RQPrevRequest,
            LeftJoin<RQRequest,
                On<RQRequest.orderNbr, Equal<RQPrevRequest.prevOrderNbr>>>,
            Where<RQPrevRequest.srcOrderNbr, Equal<Current<RQPrevRequest.srcOrderNbr>>>>
            PrevRequests;

        public PXSelect<
            RQOutboundInfo,
            Where<RQOutboundInfo.rQLineNbr, Equal<Current<RQRequestLine.lineNbr>>,
                And<RQOutboundInfo.orderNbr, Equal<Current<RQRequest.orderNbr>>>>>
            ItemOutboundInfo;
        
        public PXSelect<
            RQInboundDocument,
            Where<RQInboundDocument.reqNbr, Equal<Current<RQRequest.orderNbr>>>>
            InboundDocs;

        public PXSelect<
            RQShipmentOutboundInfo, 
            Where<RQShipmentOutboundInfo.reqNbr, Equal<Current<RQRequest.orderNbr>>, 
                And<RQShipmentOutboundInfo.docName, Equal<Current<RQInboundDocument.docName>>>>> 
            ShipmentOutboundInfo;

        public PXSelectJoin<
            RQSOOrder,
            LeftJoin<SOOrder,
                On<RQSOOrder.sOOrderNbr, Equal<SOOrder.orderNbr>>>,
            Where<RQSOOrder.rQOrderNbr, Equal<Current<RQRequest.orderNbr>>,
                And<SOOrder.orderType, Equal<Current<RQRequestClassP2P.usrSOTypeToConnect>>>>>
            SOOrders;

        #region Event Handlers

        public void RQRequest_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (RQRequest)e.Row;
            if (row == null) return;

            if (row.ReqClassID == null) return;

            var reqClassExt = PXCache<RQRequestClass>.GetExtension<RQRequestClassP2P>(Base.reqclass.Current);

            PXUIFieldAttribute.SetVisible<RQRequestLineP2P.usrEmployeeID>(Base.Lines.Cache, null, reqClassExt.UsrEnableLineEmployees == true);

            SOOrders.Cache.AllowSelect = reqClassExt.UsrConnectSO == true;
            InboundDocs.Cache.AllowSelect = reqClassExt.UsrInboundLogistics == true;

            PackingListInfo.SetVisible(reqClassExt.UsrOutboundLogistics == true);

            /*if (!PXNoteAttribute.GetFileNotes(cache, row).Any())
                cache.RaiseExceptionHandling<RQRequest.orderNbr>(row, row.OrderNbr,
                    new PXSetPropertyException("Reminder to attach files as supporting documents necessary for this request.", PXErrorLevel.Warning)); */
        }

        public void RQRequest_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        {
            var row = (RQRequest)e.Row;
            if (row == null) return;

            RQRequestClass rqClass = Base.reqclass.Select(row.ReqClassID);
            if (rqClass != null)
            {
                var rqClassExt = PXCache<RQRequestClass>.GetExtension<RQRequestClassP2P>(rqClass);
                if (rqClassExt.UsrEnableLineEmployees == true)
                {
                    foreach (RQRequestLine line in Base.Lines.Select())
                    {
                        var rowExt = PXCache<RQRequestLine>.GetExtension<RQRequestLineP2P>(line);

                        if (rowExt.UsrEmployeeID == null)
                            throw new PXException("Please select the employee per line.");
                    }
                }
                
                             /* DISABLED (CLIENT'S REQUEST) JULY 13, 2021 
               if (rqClassExt.UsrConnectSO == true)
                {
                    if (SOOrders.Select().Count == 0)
                        throw new PXException("Please select at least one sales order for this request.");
                }
                */
            }
        }

        public void RQRequestLine_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (RQRequestLine)e.Row;
            if (row == null) return;

            RQRequestClass rqClass = Base.reqclass.Current;
            RQRequest req = Base.Document.Current;
            if (rqClass == null) return;

            var rqClassExt = PXCache<RQRequestClass>.GetExtension<RQRequestClassP2P>(rqClass);
            PXUIFieldAttribute.SetVisible<RQRequestLineP2P.usrEmployeeID>(cache, row, rqClassExt.UsrEnableLineEmployees == true);
            if (rqClassExt.UsrEnableLineEmployees == false)
                PXUIFieldAttribute.SetVisibility<RQRequestLineP2P.usrEmployeeID>(cache, row, PXUIVisibility.Invisible);

            PXUIFieldAttribute.SetEnabled<RQRequestLineP2P.usrEmployeeID>(cache, row, rqClassExt.UsrEnableLineEmployees == true && req.Hold == true);
        }

        public void RQRequestLine_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        {
            var row = (RQRequestLine)e.Row;
            if (row == null) return;
            var rq = Base.Document.Current;
            
            if (e.Operation == PXDBOperation.Delete) return;
            if (Base.Accessinfo.ScreenID != "RQ.30.10.00" || rq.Approved == true) return;

            if (row.OrderQty <= 0m)
                cache.RaiseExceptionHandling<RQRequestLine.orderQty>(row, row.OrderQty, 
                    new PXSetPropertyException("Please enter the order qty. for this line."));

            var item = PXSelectorAttribute.Select<RQRequestLine.inventoryID>(cache, row, row.InventoryID) as InventoryItem;
            if (item != null)
            {
                var itemExt = PXCache<InventoryItem>.GetExtension<InventoryItemP2P>(item);

                if (itemExt.UsrCreditLimit == null || itemExt.UsrCreditLimit == 0m) return;

                if (row.CuryEstExtCost > itemExt.UsrCreditLimit)
                    throw new PXSetPropertyException("Extended cost exceeds item credit limit.", PXErrorLevel.RowError);
            }
        }

        public void RQRequest_UsrRQTypeID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (RQRequest)e.Row;
            if (row == null) return;

            var rowExt = PXCache<RQRequest>.GetExtension<RQRequestP2P>(row);
            RQRequestClassType rqtype = PXSelect<
                RQRequestClassType,
                Where<RQRequestClassType.rQTypeID, Equal<Required<RQRequestClassType.rQTypeID>>,
                    And<RQRequestClassType.reqClassID, Equal<Required<RQRequestClassType.reqClassID>>>>>
                .Select(Base, rowExt.UsrRQTypeID, row.ReqClassID);

            if (rqtype == null)
                cache.SetValueExt<RQRequest.reqClassID>(row, null);
        }

        public void RQRequestLine_CuryEstExtCost_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            var row = (RQRequestLine)e.Row;
            if (row == null) return;

            decimal? newvalue = (decimal?)e.NewValue;
            var item = PXSelectorAttribute.Select<RQRequestLine.inventoryID>(cache, row, row.InventoryID) as InventoryItem;
            if (item != null)
            {
                var itemExt = PXCache<InventoryItem>.GetExtension<InventoryItemP2P>(item);

                if (itemExt.UsrCreditLimit == null || itemExt.UsrCreditLimit == 0m) return;

                if (newvalue > itemExt.UsrCreditLimit)
                    cache.RaiseExceptionHandling<RQRequestLine.curyEstExtCost>(row, newvalue,
                        new PXSetPropertyException("Extended cost exceeds item credit limit.", PXErrorLevel.RowError));
            }
        }

        public void RQOutboundInfo_InventoryID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (RQOutboundInfo)e.Row;
            if (row == null) return;

            cache.SetDefaultExt<RQOutboundInfo.cBM>(row);
            cache.SetDefaultExt<RQOutboundInfo.weight>(row);
        }

        public void RQOutboundInfo_CBM_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (RQOutboundInfo)e.Row;
            if (row == null) return;

            if (row.InventoryID == null) return;

            e.NewValue = GetCBM(row.InventoryID);
        }

        public void RQOutboundInfo_Weight_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (RQOutboundInfo)e.Row;
            if (row == null) return;

            if (row.InventoryID == null) return;

            e.NewValue = GetWeight(row.InventoryID);
        }

        public void RQShipmentOutboundInfo_InventoryID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (RQShipmentOutboundInfo)e.Row;
            if (row == null) return;

            cache.SetDefaultExt<RQShipmentOutboundInfo.cBM>(row);
            cache.SetDefaultExt<RQShipmentOutboundInfo.weight>(row);
        }

        public void RQShipmentOutboundInfo_CBM_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (RQShipmentOutboundInfo)e.Row;
            if (row == null) return;

            if (row.InventoryID == null) return;

            e.NewValue = GetCBM(row.InventoryID);
        }

        public void RQShipmentOutboundInfo_Weight_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (RQShipmentOutboundInfo)e.Row;
            if (row == null) return;

            if (row.InventoryID == null) return;

            e.NewValue = GetWeight(row.InventoryID);
        }
        #endregion

        #region Actions
        public PXAction<RQRequest> PackingListInfo;
        [PXUIField(DisplayName = "Packing List")]
        public virtual IEnumerable packingListInfo(PXAdapter adapter)
        {
            ItemOutboundInfo.AskExt();

            return adapter.Get();
        }

        #endregion

        #region Methods
        public decimal? GetVendorPrice(int? inventoryID, int? vendorID)
        {
            APPriceWorksheetDetail detail = PXSelectJoin<
                APPriceWorksheetDetail,
                InnerJoin<APPriceWorksheet,
                    On<APPriceWorksheet.refNbr,
                Equal<APPriceWorksheetDetail.refNbr>>>,
                Where<APPriceWorksheetDetail.inventoryID, Equal<Required<APPriceWorksheetDetail.inventoryID>>,
                    And<APPriceWorksheetDetail.vendorID, Equal<Required<APPriceWorksheetDetail.vendorID>>,
                    And<APPriceWorksheet.status, Equal<SPWorksheetStatus.released>,
                    And<APPriceWorksheet.effectiveDate, GreaterEqual<Current<AccessInfo.businessDate>>>>>>,
                OrderBy<
                    Desc<APPriceWorksheet.effectiveDate>>>
                .Select(Base, inventoryID, vendorID);

            if (detail != null)
                return detail.PendingPrice;
            else
            {
                InventoryItem item = PXSelect<
                    InventoryItem,
                    Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>
                    .Select(Base, inventoryID);
                if (item == null) return 0m;

                return item.BasePrice;
            }
        }

        public decimal? GetCBM(int? inventoryID)
        {
            InventoryItem item = PXSelect<
                InventoryItem,
                Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>
                .Select(Base, inventoryID);
            if (item == null) return 0m;
            var itemExt = PXCache<InventoryItem>.GetExtension<InventoryItemP2P>(item);

            return itemExt.UsrCBM;
        }

        public decimal? GetWeight(int? inventoryID)
        {
            InventoryItem item = PXSelect<
                InventoryItem,
                Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>
                .Select(Base, inventoryID);
            if (item == null) return 0m;

            return item.BaseItemWeight;
        }

        #endregion
    }
}]]></CDATA>
</Graph>