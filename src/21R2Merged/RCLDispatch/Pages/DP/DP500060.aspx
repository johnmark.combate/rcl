<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="DP500060.aspx.cs" Inherits="Page_DP500060" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="Dispatch.ChangeWHProc"
        PrimaryView="SOTransactions"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="FilterSO" Width="100%" Height="100px" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXSelector AutoRefresh="True" FilterByAllFields="True" runat="server" ID="ShipmentNbr" DataField="ShipmentNbr" CommitChanges="True" ></px:PXSelector>
			<px:PXSelector AutoRefresh="True" FilterByAllFields="True" CommitChanges="True" runat="server" ID="SONumber" DataField="SONumber" ></px:PXSelector></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid SyncPosition="True" ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Inquire" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="SOTransactions">
			    <Columns>
				<px:PXGridColumn TextAlign="Center" Type="CheckBox" DataField="UsrSelected" Width="60" CommitChanges="True" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__OrderType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="SOOrderEntry" DataField="SOOrder__OrderNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LineNbr" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__Status" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="SOShipmentEntry" DataField="SOShipment__ShipmentNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__CustomerID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__CustomerID_BAccountR_acctName" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="InventoryID" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SiteID" Width="140" CommitChanges="True" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SiteID_description" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOShipLine__ShippedQty" Width="100" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
