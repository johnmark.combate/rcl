<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="DP305000.aspx.cs" Inherits="Page_DP305000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.BackloadTransferEntry"
        PrimaryView="BackloadTransfers"
        >
		<CallbackCommands>
			<px:PXDSCallbackCommand Name="GoToAdjustment" Visible="False" DependOnGrid="CstPXGrid7" CommitChanges="true" /></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="BackloadTransfers" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule4" StartColumn="True" />
			<px:PXSelector runat="server" ID="CstPXSelector2" DataField="BackloadNbr" ></px:PXSelector>
			<px:PXCheckBox runat="server" ID="CstPXCheckBox3" DataField="Released" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule5" StartColumn="True" />
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit1" DataField="BackloadDate" ></px:PXDateTimeEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Details" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="Transactions">
			    <Columns>
				<px:PXGridColumn DataField="BackloadType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="INTransferEntry" DataField="TransferNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="GoToDoc" DataField="DocNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__InventoryID" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__InventoryID_description" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Qty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__Qty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__INTransitQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__ToSiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__LocationID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__LotSerialNbr" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TranDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TranDesc" Width="280" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	
		<Mode AllowAddNew="False" ></Mode></px:PXGrid>
	<px:PXSmartPanel Height="400" AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" AutoCallBack-Handler="ds" AutoCallBack-Target="CstPXGrid7" CaptionVisible="True" Width="1200" Caption="Adjustments" runat="server" ID="CstSmartPanel6" Key="Adjustments">
		<px:PXGrid SyncPosition="True" SkinID="Inquire" Width="100%" runat="server" ID="CstPXGrid7">
			<Levels>
				<px:PXGridLevel DataMember="Adjustments" >
					<Columns>
						<px:PXGridColumn DataField="INRegister__DocType" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn LinkCommand="GoToAdjustment" DataField="INRegister__RefNbr" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="InventoryID" Width="120" ></px:PXGridColumn>
						<px:PXGridColumn DataField="InventoryID_description" Width="280" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Qty" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UOM" Width="72" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__TranDate" Width="90" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__TranDesc" Width="280" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
			<AutoSize Enabled="True" ></AutoSize>
			<AutoSize MinHeight="100" ></AutoSize>
			<AutoSize MinWidth="100" ></AutoSize>
			<Mode AllowAddNew="False" ></Mode>
			<Mode AllowDelete="False" ></Mode>
			<Mode AllowUpdate="False" ></Mode></px:PXGrid>
		<px:PXPanel SkinID="Buttons" runat="server" ID="CstPanel8">
			<px:PXButton Text="OK" DialogResult="OK" runat="server" ID="CstButton9" ></px:PXButton></px:PXPanel></px:PXSmartPanel></asp:Content>
