<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="LP301600.aspx.cs" Inherits="Page_LP301600" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.LPLoadPlanTransferEmployeeEntry"
        PrimaryView="Filter"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Filter" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule2" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector3" DataField="LoadPlanTransferNbr" CommitChanges="True" ></px:PXSelector>
			<px:PXCheckBox CommitChanges="True" runat="server" ID="CstPXCheckBox4" DataField="ShowAllEmployees" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSegmentMask CommitChanges="True" runat="server" ID="CstPXSegmentMask5" DataField="EmployeeID" ></px:PXSegmentMask></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Details" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="EmployeeLoadPlanTransfer">
			    <Columns>
				<px:PXGridColumn DataField="EmployeeID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="EmployeeID_description" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TransferType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__RefNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__InventoryID" Width="70" LinkCommand="InventoryItemMaint" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__InventoryID_description" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__LotSerialNbr" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__Qty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LoadQty" Width="100" CommitChanges="True" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__UOM" Width="72" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	
		<Mode AllowDelete="True" AllowAddNew="False" ></Mode></px:PXGrid>
</asp:Content>