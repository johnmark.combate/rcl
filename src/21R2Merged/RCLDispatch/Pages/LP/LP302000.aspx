<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="LP302000.aspx.cs" Inherits="Page_LP302000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.LoadPlanTicketTransferEntry"
        PrimaryView="LoadTransfers"
        >
		<CallbackCommands>
			<px:PXDSCallbackCommand DependOnGrid="CstPXGrid12" Name="ViewEmployeeLoadQty" Visible="False" CommitChanges="True" ></px:PXDSCallbackCommand>
			<px:PXDSCallbackCommand Name="AddTransfers" Visible="False" CommitChanges="True" /></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="LoadTransfers" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule ControlSize="S" runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector9" DataField="LoadPlanNbr" ></px:PXSelector>
			<px:PXDropDown runat="server" ID="CstPXDropDown6" DataField="Status" ></px:PXDropDown>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit3" DataField="DocDate" ></px:PXDateTimeEdit>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit8" DataField="Zone" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule2" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector11" DataField="TruckID" ></px:PXSelector>
			<px:PXSelector runat="server" ID="CstPXSelector16" DataField="ContainerNo" />
			<px:PXSelector runat="server" ID="CstPXSelector14" DataField="DriverID" ></px:PXSelector>
			<px:PXCheckBox Enabled="False" runat="server" ID="CstPXCheckBox5" DataField="IsVerified" ></px:PXCheckBox>
			<px:PXLayoutRule LabelsWidth="SM" runat="server" ID="CstPXLayoutRule13" StartColumn="True" ></px:PXLayoutRule>
			<px:PXNumberEdit runat="server" DataField="CurrentTruck.TotalTruckCapacityCBM" ID="TotalTruckCapacityCBMNumberEdit" ></px:PXNumberEdit>
			<px:PXNumberEdit runat="server" DataField="CurrentTruck.TotalTruckCapacityKGS" ID="TotalTruckCapacityKGSNumberEdit" ></px:PXNumberEdit>
			<px:PXNumberEdit runat="server" DataField="CurrentTruck.CurrentCapacityCBM" ID="CurrentCapacityCBMNumberEdit" ></px:PXNumberEdit>
			<px:PXNumberEdit runat="server" DataField="CurrentTruck.CurrentCapacityKGS" ID="CurrentCapacityKGSNumberEdit" ></px:PXNumberEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" AllowAutoHide="false">
		<Items>
			<px:PXTabItem Text="Load Items">
				<Template>
					<px:PXGrid runat="server" ID="CstPXGrid12" SkinID="Details" Width="100%">
						<Levels>
							<px:PXGridLevel DataMember="LoadTransferLines" >
								<Columns>
									<px:PXGridColumn DataField="INRegister__TransferType" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="INTransferEntry" DataField="TransferNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn LinkCommand="InventoryItemMaint" DataField="INTran__InventoryID" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="INTran__TranDesc" Width="280" />
									<px:PXGridColumn DataField="INTran__LotSerialNbr" Width="220" ></px:PXGridColumn>
									<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="INTran__Qty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="INTran__UsrLoadedQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="INTran__UsrOpenLoadQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn CommitChanges="True" DataField="LoadQty" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn Type="CheckBox" DataField="Verified" Width="60" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="100" MinWidth="100" ></AutoSize>
						<Mode AllowAddNew="False" ></Mode>
						<ActionBar>
							<CustomItems>
								<px:PXToolBarButton>
									<AutoCallBack Target="ds" Command="AddTransfers" /></px:PXToolBarButton>
								<px:PXToolBarButton>
									<AutoCallBack Target="ds" Command="ViewEmployeeLoadQty" ></AutoCallBack></px:PXToolBarButton></CustomItems></ActionBar></px:PXGrid></Template>
			</px:PXTabItem>
			<px:PXTabItem Text="Crew">
				<Template>
					<px:PXGrid runat="server" ID="CstPXGrid15" Width="100%" SkinID="Details">
						<Levels>
							<px:PXGridLevel DataMember="Crew" >
								<Columns>
									<px:PXGridColumn DataField="EmployeeID" Width="140" CommitChanges="True" ></px:PXGridColumn>
									<px:PXGridColumn DataField="EmployeeID_description" Width="220" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" MinHeight="100" MinWidth="100" ></AutoSize></px:PXGrid></Template>
			</px:PXTabItem>
		</Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	</px:PXTab>
	<px:PXSmartPanel runat="server" Height="350" Width="600" ID="LoadQtyPerEmployeeSmartPanel1" LoadOnDemand="True" Caption="Load Qty. Count per Employee" Key="LoadQtyPerEmployee" AutoCallBack-Enabled="True" AutoCallBack-Target="LoadQtyPerEmployeePXGrid2" AutoCallBack-Command="Refresh">
		<px:PXGrid runat="server" SkinID="Inquire" Width="100%" ID="LoadQtyPerEmployeePXGrid2" AutoAdjustColumns="True">
			<AutoSize Enabled="True" MinWidth="100" MinHeight="100" ></AutoSize>
			<Levels>
				<px:PXGridLevel DataMember="LoadQtyPerEmployee">
					<Columns>
						<px:PXGridColumn DataField="EmployeeID" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="EmployeeID_description" Width="220" ></px:PXGridColumn>
						<px:PXGridColumn DataField="LoadQty" Width="100" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels></px:PXGrid>
		<px:PXPanel runat="server" SkinID="Buttons" ID="CstPanel3">
			<px:PXButton runat="server" Text="OK" DialogResult="OK" ID="OKCstButton4" ></px:PXButton></px:PXPanel></px:PXSmartPanel>
	<px:PXSmartPanel runat="server" Height="500" Width="800" ID="CstSmartPanel10" CaptionVisible="True" Caption="Add Transfers" Key="Transfers" AutoCallBack-Enabled="True" AutoCallBack-Target="CstPXGrid11" AutoCallBack-Command="Refresh">
		<px:PXGrid runat="server" SkinID="Inquire" Width="100%" ID="CstPXGrid11">
			<AutoSize Enabled="True" />
			<Levels>
				<px:PXGridLevel DataMember="Transfers">
					<Columns>
						<px:PXGridColumn Type="CheckBox" TextAlign="Center" DataField="Selected" Width="30" AllowCheckAll="True" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__TransferType" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__RefNbr" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="InventoryID" Width="70" LinkCommand="InventoryMaint" ></px:PXGridColumn>
						<px:PXGridColumn DataField="TranDesc" Width="280" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__TranDate" Width="90" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__ToSiteID" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Qty" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UsrOpenLoadQty" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__ExtRefNbr" Width="180" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
			<ActionBar>
				<CustomItems /></ActionBar></px:PXGrid>
		<px:PXPanel runat="server" SkinID="Buttons" ID="CstPanel12">
			<px:PXButton runat="server" Text="OK" DialogResult="OK" ID="CstButton13" />
			<px:PXButton runat="server" Text="Cancel" DialogResult="Cancel" ID="CstButton14" /></px:PXPanel></px:PXSmartPanel></asp:Content>
