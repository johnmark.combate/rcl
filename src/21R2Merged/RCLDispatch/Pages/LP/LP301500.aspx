<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="LP301500.aspx.cs" Inherits="Page_LP301500" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.LoadPlanEmployeeEntry"
        PrimaryView="SummaryDetails"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="SummaryDetails" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector2" DataField="LoadPlanNbr" ></px:PXSelector>
			<px:PXCheckBox CommitChanges="True" runat="server" ID="CstPXCheckBox3" DataField="ShowAllEmployees" ></px:PXCheckBox>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule4" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSegmentMask CommitChanges="True" runat="server" ID="CstPXSegmentMask1" DataField="EmployeeID" ></px:PXSegmentMask></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Details" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="EmployeeLoadPlan">
			    <Columns>
				<px:PXGridColumn DataField="EmployeeID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="EmployeeID_description" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LPLoadPlanLine__SOOrderType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LPLoadPlanLine__SOOrderType" Visible="False" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LPLoadPlanLine__SOOrderNbr" Width="140" LinkCommand="MySOLink" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LPLoadPlanLine__InvoiceType" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOLine__OrderQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOLine__OpenQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LoadQty" Width="100" CommitChanges="True" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LPLoadPlanLine__InvoiceNbr" Width="140" LinkCommand="MySILink" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LPLoadPlanLine__TransferRefNbr" Width="140" LinkCommand="TransferNbr" ></px:PXGridColumn>
				<px:PXGridColumn DataField="LPLoadPlanLine__ShipmentNbr" Width="140" LinkCommand="MyShipment" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__CustomerID" Width="140" LinkCommand="MyCustomer" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__CustomerID_BAccountR_acctName" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOLine__InventoryID" Width="70" LinkCommand="MyInventory" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOLine__InventoryID_InventoryItem_descr" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOLine__SiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOLine__SiteID_description" Width="220" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	
		<Mode AllowAddNew="False" AllowDelete="False" InitNewRow="True" ></Mode></px:PXGrid></asp:Content>
