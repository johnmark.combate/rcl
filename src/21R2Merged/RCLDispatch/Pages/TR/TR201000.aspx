<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="TR201000.aspx.cs" Inherits="Page_TR201000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.TruckContainerMaint"
        PrimaryView="TruckContainers"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid SyncPosition="True" ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Primary" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="TruckContainers">
			    <Columns>
				<px:PXGridColumn CommitChanges="True" DataField="ContainerCD" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Description" Width="180" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Model" Width="180" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ContainerCBM" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="ContainerKGS" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="BranchID" Width="140" CommitChanges="True" ></px:PXGridColumn>
				<px:PXGridColumn DataField="BranchID_description" Width="220" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	
		<Mode AllowUpload="True" InitNewRow="True" ></Mode></px:PXGrid>
</asp:Content>
