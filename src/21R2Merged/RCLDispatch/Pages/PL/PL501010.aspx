<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="PL501010.aspx.cs" Inherits="Page_PL501010" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.PLPickListTransferProcess"
        PrimaryView="Transfers"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Filter" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule LabelsWidth="SM" ControlSize="S" runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector3" DataField="TransferNbr" ></px:PXSelector>
			<px:PXSegmentMask CommitChanges="True" runat="server" ID="CstPXSegmentMask5" DataField="SrcSiteID" ></px:PXSegmentMask>
			<px:PXSegmentMask CommitChanges="True" runat="server" ID="CstPXSegmentMask4" DataField="DestSiteID" ></px:PXSegmentMask>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule2" StartColumn="True" ></px:PXLayoutRule>
			<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit6" DataField="DateFrom" ></px:PXDateTimeEdit>
			<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit7" DataField="DateTo" ></px:PXDateTimeEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="PrimaryInquire" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="Transfers">
			    <Columns>
				<px:PXGridColumn AllowCheckAll="True" DataField="Selected" Width="30" Type="CheckBox" TextAlign="Center" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="" DataField="INRegister__RefNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TransferType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="InventoryMaint" DataField="InventoryID" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="TranDesc" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TranDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__ToSiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Qty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__ExtRefNbr" Width="180" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__Status" Width="70" /></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
