﻿<Graph ClassName="LoadPlanTransferProcess" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using PX.Objects.IN;
using Dispatch;
using System.Linq;
using PX.Objects.CS;
using PX.Objects.EP;

namespace DispatchExtension
{
  public class LoadPlanTransferProcess : PXGraph<LoadPlanTransferProcess>
  {
    public PXCancel<INTran> Cancel;

        public LoadPlanTransferProcess()
        {
            Transfers.SetProcessDelegate(list => LoadTransfers(list, CurrentTruck.Current));
        }

        public PXFilter<TruckInfo> CurrentTruck;
        public PXFilter<TransferFilter> Filter;
        public PXProcessingJoin<
            INTran,
            InnerJoin<INRegister,
                On<INTran.refNbr, Equal<INRegister.refNbr>,
                And<INRegister.docType, Equal<INTran.docType>>>>,
            Where2<
                Where<INRegister.docType, Equal<INDocType.transfer>,
                    And<INRegister.released, Equal<False>,
                    And<INTran.qty, Greater<decimal0>,
                    And<INTranExtRCL.usrAvailLoadQty, Greater<decimal0>,
                    And<INRegister.status, Equal<INDocStatus.balanced>,
                        And<INTranExtRCL.usrDispatchType, IsNotNull>>>>>>,
                And2<
                    Where<INRegister.siteID, Equal<Current<TransferFilter.srcSiteID>>,
                        Or<Current<TransferFilter.srcSiteID>, IsNull>>,
                    And2<
                        Where<INRegister.toSiteID, Equal<Current<TransferFilter.destSiteID>>,
                            Or<Current<TransferFilter.destSiteID>, IsNull>>,
                        And2<
                            Where<INRegister.refNbr, Equal<Current<TransferFilter.transferNbr>>,
                                Or<Current<TransferFilter.transferNbr>, IsNull>>,
                                And2<
                                    Where<INRegister.tranDate,
                            GreaterEqual<Current<TransferFilter.dateFrom>>, 
                                        Or<Current<TransferFilter.dateFrom>, IsNull>>, 
                                        And<Where<INRegister.tranDate, LessEqual<Current<TransferFilter.dateTo>>,
                                            Or<Current<TransferFilter.dateTo>, IsNull>>>>>>>>>
            Transfers;

        public decimal? CalcItemTotal(bool isCBM)
        {
            decimal? total = 0m;
            foreach(INTran tran in Transfers.Cache.Updated)
            {
                if (tran.Selected == true)
                {
                    InventoryItem item = PXSelect<
                        InventoryItem, 
                        Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>
                        .Select(this, tran.InventoryID);
                    if (item != null)
                    {
                        var lineExt = PXCache<INTran>.GetExtension<INTranExtRCL>(tran);

                        total += lineExt.UsrLoadQty * (isCBM ? item.BaseItemVolume : item.BaseItemWeight);
                    }
                }
            }

            return total;
        }

        #region Methods
        public static void LoadTransfers(List<INTran> transactions, TruckInfo info)
        {
            if (info.TruckID == null)
                throw new PXException("Truck is required before processing load plan.");

            var graph = PXGraph.CreateInstance<LoadPlanTicketTransferEntry>();

            EPEmployee emp = PXSelectJoin<
                EPEmployee, 
                InnerJoin<Truck, 
                    On<Truck.driverID, Equal<EPEmployee.acctName>>>,
                Where<Truck.truckID, Equal<Required<Truck.truckID>>>>
                .Select(graph, info.TruckID);
            
            graph.LoadTransfers.Insert(new LPLoadPlanTransfer()
            {
                TruckID = info.TruckID,
                ContainerNo = info.ContainerNo,
                DriverID = emp != null ? emp.BAccountID : null
            });

            foreach (INTran tran in transactions)
            {
                var tranExt = PXCache<INTran>.GetExtension<INTranExtRCL>(tran);

                LPLoadPlanTransferLine line = graph.LoadTransferLines.Insert(new LPLoadPlanTransferLine()
                {
                    TransferNbr = tran.RefNbr,
                    TransferLineNbr = tran.LineNbr,
                    LoadQty = tranExt.UsrLoadQty
                });
                graph.AddContent(line, tran, tranExt.UsrLoadQty ?? 0m);
            }

            throw new PXRedirectRequiredException(graph, null);
        }
        #endregion

        #region Truck Info DAC + IEnumerable
        public virtual IEnumerable currentTruck()
        {
            Truck select = PXSelect<
                Truck,
                Where<Truck.truckID, Equal<Required<FilterLoadPlan.truckID>>>>
                .Select(this, CurrentTruck.Current.TruckID);

            if (select != null)
            {
                TruckInfo info = new TruckInfo()
                {
                    TruckID = select.TruckID,
                    DriverName = select.DriverID,
                    TotalTruckCapacityCBM = select.TruckCapacityCMB,
                    TotalTruckCapacityKGS = select.TruckCapacityKGS,
                    CurrentCapacityCBM = CalcItemTotal(true),
                    CurrentCapacityKGS = CalcItemTotal(false)
                };

                info.RemTruckBalCBM = info.TotalTruckCapacityCBM - info.CurrentCapacityCBM;
                info.RemTruckBalKGS = info.TotalTruckCapacityKGS - info.CurrentCapacityKGS;

                return new List<TruckInfo>() { info };
            }
            else
            {
                return new List<TruckInfo>() { new TruckInfo()
                {
                    //null
                } };
            }
        }
        #endregion

        #region Event Handlers
        public void TruckInfo_Truck_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            TruckInfo row = (TruckInfo)e.Row;
            Truck select = PXSelect<
                Truck,
                Where<Truck.truckID, Equal<Required<FilterLoadPlan.truckID>>>>
                .Select(this, row.TruckID);

            if (select == null) return;
            cache.SetValueExt<TruckInfo.totalTruckCapacityCBM>(row, select.TruckCapacityCMB);
            cache.SetValueExt<TruckInfo.totalTruckCapacityKGS>(row, select.TruckCapacityKGS);
            cache.SetValueExt<TruckInfo.driverName>(row, select.DriverID);
        }

        public void INTran_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            INTran row = (INTran)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetEnabled<INTranExtRCL.usrLoadQty>(cache, null, true);
        }
        #endregion
    }

    #region TruckInfo
    [Serializable]
    public class TruckInfo : IBqlTable
    {
        #region Truck
        [PXInt()]
        [PXUIField(DisplayName = "Truck")]
        [PXSelector(typeof(Search<
            Truck.truckID,
            Where<Truck.status, Equal<TruckStatus.active>>>),
                typeof(Truck.truckID), typeof(Truck.truckCD), typeof(Truck.driverID), typeof(Truck.truckCapacityCMB), typeof(Truck.truckCapacityKGS), SubstituteKey = typeof(Truck.truckCD))]
        [PXDefault()]
        public virtual int? TruckID { get; set; }
        public abstract class truckID : IBqlField { }
        #endregion

        #region ContainerNo
        [PXDBInt]
        [PXUIField(DisplayName = "Container No.")]
        [PXSelector(typeof(Search<TruckContainer.containerID, Where<TruckContainer.status, Equal<int1>>>),
            typeof(TruckContainer.containerCD),
            typeof(TruckContainer.description),
            typeof(TruckContainer.model),
            typeof(TruckContainer.containerCBM),
            typeof(TruckContainer.containerKGS),
            SubstituteKey = typeof(TruckContainer.containerCD),
            DescriptionField = typeof(TruckContainer.description))]
        public virtual int? ContainerNo { get; set; }
        public abstract class containerNo : PX.Data.BQL.BqlInt.Field<containerNo> { }
        #endregion

        #region DriverName
        [PXString(50)]
        [PXUIField(DisplayName = "Driver Name", Enabled = false)]
        public virtual string DriverName { get; set; }
        public abstract class driverName : IBqlField { }
        #endregion

        #region KGS Section
        [PXDecimal]
        [PXUIField(DisplayName = "Current Capacity KGS", Enabled = false)]
        public virtual Decimal? CurrentCapacityKGS { get; set; }
        public abstract class currentCapacityKGS : IBqlField { }

        [PXDecimal]
        [PXUIField(DisplayName = "Total Truck Capacity KGS", Enabled = false)]
        public virtual Decimal? TotalTruckCapacityKGS { get; set; }
        public abstract class totalTruckCapacityKGS : IBqlField { }

        [PXDecimal]
        [PXUIField(DisplayName = "Remaining Truck Balance KGS", Enabled = false)]
        public virtual Decimal? RemTruckBalKGS { get; set; }
        public abstract class remTruckBalKGS : IBqlField { }
        #endregion

        #region CBM Section
        [PXDecimal]
        [PXUIField(DisplayName = "Total Truck Capacity CBM", Enabled = false)]
        public virtual Decimal? TotalTruckCapacityCBM { get; set; }
        public abstract class totalTruckCapacityCBM : IBqlField { }

        [PXDecimal]
        [PXUIField(DisplayName = "Remaining Truck Balance CBM", Enabled = false)]
        public virtual Decimal? RemTruckBalCBM { get; set; }
        public abstract class remTruckBalCBM : IBqlField { }

        [PXDecimal]
        [PXUIField(DisplayName = "Current Capacity CBM", Enabled = false)]
        public virtual Decimal? CurrentCapacityCBM { get; set; }
        public abstract class currentCapacityCBM : IBqlField { }
        #endregion
    }
    #endregion
}]]></CDATA>
</Graph>