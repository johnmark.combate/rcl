<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="BR303000.aspx.cs" Inherits="Page_BR303000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="OtherBP.OtherBP"
        PrimaryView="otherBusinessPartner"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid SyncPosition="True" ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Primary" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="otherBusinessPartner">
			    <Columns>
				<px:PXGridColumn DataField="Bpcode" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="VendorName" Width="200" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="Tin" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="BPAddress" Width="200" ></px:PXGridColumn></Columns>
			
				<RowTemplate></RowTemplate></px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	
		<Mode AllowDelete="True" AllowUpdate="True" AllowAddNew="True" ></Mode></px:PXGrid>
</asp:Content>
