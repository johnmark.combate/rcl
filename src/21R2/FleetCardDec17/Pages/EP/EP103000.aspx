<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EP103000.aspx.cs" Inherits="Page_EP103000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="FleetCard.EPFleetCardMaint"
        PrimaryView="FleetCards"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid SyncPosition="True" ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Primary" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="FleetCards">
			    <Columns>
				<px:PXGridColumn LinkCommand="ViewFleetDetails" CommitChanges="True" DataField="FleetCardCD" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="FleetCardNo" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="CardHolder" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CardHolder_description" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CreditLimit" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="VendorID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="VendorID_description" Width="280" />
				<px:PXGridColumn DataField="LastModifiedByID" Width="220" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	
		<Mode AllowUpload="True" ></Mode></px:PXGrid>
</asp:Content>
