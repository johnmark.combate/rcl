﻿<Graph ClassName="ExpenseClaimDetailsEntryExtFC" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using PX.Data;
using PX.Objects;
using PX.Objects.EP;
using PX.Objects.IN;
using FleetCard;
using System.Collections;

namespace PX.Objects.EP
{
    public class ExpenseClaimDetailEntryExtFC : PXGraphExtension<ExpenseClaimDetailEntry>
    {
        #region Selects
        public PXSelect<
                EPFleetCard,
                Where<EPFleetCard.fleetCardID, Equal<Current<EPExpenseClaimDetailsExtFC.usrFleetCardID>>>> 
                CurrentFleetCard;
        #endregion

        #region Event Handlers
        public void EPExpenseClaimDetails_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (EPExpenseClaimDetails)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetVisible<EPExpenseClaimDetailsExtFC.usrFleetCardID>(cache, row, false);
            PXUIFieldAttribute.SetVisible<EPExpenseClaimDetailsExtFC.usrTruckID>(cache, row, false);
            CurrentFleetCard.Cache.AllowUpdate = false;
            PXUIFieldAttribute.SetVisible<EPFleetCard.fleetCardNo>(CurrentFleetCard.Cache, null, false);
            PXUIFieldAttribute.SetVisible<EPFleetCard.creditLimit>(CurrentFleetCard.Cache, null, false);

            InventoryItem item = PXSelect<
                InventoryItem,
                Where<InventoryItem.inventoryID, Equal<Current<EPExpenseClaimDetails.inventoryID>>>>
                .Select(Base);
            if (item != null)
            {
                var itemExt = PXCache<InventoryItem>.GetExtension<InventoryItemExtFC>(item);

                PXUIFieldAttribute.SetVisible<EPExpenseClaimDetailsExtFC.usrFleetCardID>(cache, row, itemExt.UsrIsFleetCard == true);
                PXUIFieldAttribute.SetVisible<EPExpenseClaimDetailsExtFC.usrTruckID>(cache, row, itemExt.UsrIsFleetCard == true);
                PXUIFieldAttribute.SetVisible<EPFleetCard.fleetCardNo>(CurrentFleetCard.Cache, null, itemExt.UsrIsFleetCard == true);
                PXUIFieldAttribute.SetVisible<EPFleetCard.creditLimit>(CurrentFleetCard.Cache, null, itemExt.UsrIsFleetCard == true);


                CurrentFleetCard.AllowSelect = itemExt.UsrIsFleetCard == true;
            }
            
            Base.ClaimDetails.AllowUpdate = row.Released == false;
            Base.CurrentClaimDetails.AllowUpdate = row.Released == false;

            if(FleetCardHelper.HasClaim(Base, row))
                Base.Claim.SetEnabled(false);
        }

        public void EPExpenseClaimDetails_UsrFleetCardID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (EPExpenseClaimDetails)e.Row;
            if (row == null) return;

            cache.SetValue<EPExpenseClaimDetailsExtFC.usrTruckID>(row, null);
        }

        public void EPExpenseClaimDetails_EmployeeID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (EPExpenseClaimDetails)e.Row;
            if (row == null) return;
            
            var rowExt = PXCache<EPExpenseClaimDetails>.GetExtension<EPExpenseClaimDetailsExtFC>(row);
            EPFleetCard card = PXSelect<
                EPFleetCard,
                Where<EPFleetCard.fleetCardID, Equal<Required<EPFleetCard.fleetCardID>>,
                    And<EPFleetCard.cardHolder, Equal<Required<EPFleetCard.cardHolder>>>>>
                .Select(Base, rowExt.UsrFleetCardID, row.EmployeeID);
            if (card == null)
                cache.SetValueExt<EPExpenseClaimDetailsExtFC.usrFleetCardID>(row, null);
        }
        
        public void EPExpenseClaimDetails_CuryExtCost_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            var row = (EPExpenseClaimDetails)e.Row;
            if (row == null) return;

            decimal? cost = (decimal?)e.NewValue;

            if(ExceedsLimit(CurrentFleetCard.Current, cost))
                throw new PXSetPropertyException("Claim amount exceeds credit limit for this fleet card. Please change to continue.");
        }

        public void EPExpenseClaimDetails_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        {
            var row = (EPExpenseClaimDetails)e.Row;
            if (row == null) return;

            if (ExceedsLimit(CurrentFleetCard.Current, row.CuryExtCost))
                cache.RaiseExceptionHandling<EPExpenseClaimDetails.curyExtCost>(row, row.CuryExtCost,
                    new PXSetPropertyException("Claim amount exceeds credit limit. Please change to continue."));
        }
        #endregion

        #region Overrides

        public delegate IEnumerable claimDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable claim(PXAdapter adapter, claimDelegate baseMethod)
        {
            EPExpenseClaimDetails det = Base.CurrentClaimDetails.Current;
            if (FleetCardHelper.ReceiptIsFleetCard(Base, det))
            {
                var graph = PXGraph.CreateInstance<ExpenseClaimEntry>();

                graph.ExpenseClaim.Insert(new EPExpenseClaim()
                {
                    DocDesc = det.TranDesc
                });
                graph.GetExtension<ExpenseClaimEntryExtFC>().FleetCards.Insert(new EPExpenseFleetCard()
                {
                    ClaimDetailID = det.ClaimDetailID
                });

                throw new PXRedirectRequiredException(graph, null);
            }
            else
                return baseMethod(adapter);
        }
        #endregion

        #region Methods
        public bool ExceedsLimit(EPFleetCard card, decimal? cost)
        {
            if (card != null)
                if (cost > card.CreditLimit)
                    return true;
            
            return false;
        }
        #endregion
    }
}]]></CDATA>
</Graph>