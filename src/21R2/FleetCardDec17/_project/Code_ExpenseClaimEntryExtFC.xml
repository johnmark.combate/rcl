﻿<Graph ClassName="ExpenseClaimEntryExtFC" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PX.Common;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.CT;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.TX;
using PX.TM;
using PX.Objects.PM;
using PX.Objects.Common.Extensions;
using FleetCard;
using static PX.Objects.EP.ExpenseClaimEntry;

namespace PX.Objects.EP
{
    public class FleetCardHelper
    {
        public static bool ReceiptIsFleetCard(PXGraph graph, EPExpenseClaimDetails claimDet)
        {
            InventoryItem invItem = PXSelectJoin<
                InventoryItem,
                InnerJoin<EPExpenseClaimDetails,
                    On<EPExpenseClaimDetails.inventoryID, Equal<InventoryItem.inventoryID>>>,
                Where<EPExpenseClaimDetails.claimDetailID, Equal<Required<EPExpenseClaimDetails.claimDetailID>>>>
                .Select(graph, claimDet.ClaimDetailID);
            if (invItem != null)
            {
                var invItemExt = PXCache<InventoryItem>.GetExtension<InventoryItemExtFC>(invItem);

                if (invItemExt.UsrIsFleetCard == true)
                    return true;
            }

            return false;
        }

        public static bool HasClaim(PXGraph graph, EPExpenseClaimDetails claimDet)
        {
            EPExpenseFleetCard claim = PXSelect<
                EPExpenseFleetCard, 
                Where<EPExpenseFleetCard.claimDetailID, Equal<Required<EPExpenseFleetCard.claimDetailID>>>>
                .Select(graph, claimDet.ClaimDetailID);
            if (claim != null) return true;

            return false;
        }
    }

    public class ExpenseClaimEntryExtFC : PXGraphExtension<ExpenseClaimEntry>
    {
        public PXSelect<
            EPFleetCard,
            Where<EPFleetCard.fleetCardID, Equal<Required<EPFleetCard.fleetCardID>>>>
            FleetCardSelect;
        public PXSelectJoin<
            EPExpenseFleetCard, 
            LeftJoin<EPExpenseClaimDetails, 
                On<EPExpenseClaimDetails.claimDetailID, Equal<EPExpenseFleetCard.claimDetailID>>>,
            Where<EPExpenseFleetCard.claimRefNbr, Equal<Current<EPExpenseClaim.refNbr>>>> 
            FleetCards;

        #region Overrides
        protected virtual IEnumerable receiptsforsubmit()
        {
            PXSelectBase<EPExpenseClaimDetailsForSubmit> receiptsForSubmit = new PXSelect<EPExpenseClaimDetailsForSubmit,
             Where2<Where<EPExpenseClaimDetailsForSubmit.refNbr, IsNull,
                          Or<EPExpenseClaimDetailsForSubmit.refNbr, Equal<Current<EPExpenseClaim.refNbr>>>>,
                    And<Where<EPExpenseClaimDetailsForSubmit.rejected, NotEqual<True>,
                    And<EPExpenseClaimDetailsForSubmit.employeeID, Equal<Current<EPExpenseClaim.employeeID>>>>>>>(Base);

            if (Base.epsetup.Current.AllowMixedTaxSettingInClaims != true)
            {
                receiptsForSubmit.WhereAnd<
                    Where<EPExpenseClaimDetailsForSubmit.taxCalcMode, Equal<Current2<EPExpenseClaim.taxCalcMode>>,
                    And<Where<EPExpenseClaimDetailsForSubmit.taxZoneID, Equal<Current2<EPExpenseClaim.taxZoneID>>,
                        Or<Where<EPExpenseClaimDetailsForSubmit.taxZoneID, IsNull,
                            And<Current2<EPExpenseClaim.taxZoneID>, IsNull>>>>>>>();
            }

            HashSet<Int32?> receiptsInClaim = new HashSet<Int32?>();
            foreach (EPExpenseClaimDetails receiptInClaim in Base.ExpenseClaimDetails.Select())
            {
                receiptsInClaim.Add(receiptInClaim.ClaimDetailID);
            }

            foreach (EPExpenseFleetCard receiptInClaim in FleetCards.Select())
            {
                receiptsInClaim.Add(receiptInClaim.ClaimDetailID);
            }

            foreach (EPExpenseClaimDetailsForSubmit receiptForSubmit in receiptsForSubmit.Select())
            {
                if (receiptsInClaim.Contains(receiptForSubmit.ClaimDetailID))
                {
                    continue;
                }

                if(receiptForSubmit.Released == false)
                    yield return receiptForSubmit;
            }
        }

        /***   ENABLE AUTOMATIC ADDING OF FLEET CARDS IN FLEET CARDS TAB IF INVOICING FLEET CARD IS NOT ALLOWED   ***/
        public delegate IEnumerable SubmitReceiptDelegate(PXAdapter adapter);
        [PXOverride]
        public virtual IEnumerable SubmitReceipt(PXAdapter adapter, SubmitReceiptDelegate baseMethod)
        {
            if (Base.ExpenseClaim.Current != null)
            {
                foreach (EPExpenseClaimDetails item in Base.ReceiptsForSubmit.Select())
                {
                    if (item.Selected == true)
                    {
                        if (FleetCardHelper.ReceiptIsFleetCard(Base, item))
                        {
                            item.Selected = false;

                            FleetCards.Insert(new EPExpenseFleetCard() {
                                ClaimDetailID = item.ClaimDetailID
                            });
                        }
                        else
                        {
                            EPExpenseClaimDetails details = (EPExpenseClaimDetails)Base.ExpenseClaimDetails.Cache.CreateCopy(item);
                            Base.FindImplementation<ExpenseClaimEntryReceiptExt>().SubmitReceiptExt(Base.ExpenseClaim.Cache, Base.ExpenseClaimDetails.Cache, Base.ExpenseClaim.Current, details);
                        }
                    }
                }
            }
            Base.ReceiptsForSubmit.Cache.Clear();
            return adapter.Get();
        }

        /***   MARK FLEET CARDS AS RELEASED   ***/
        public delegate IEnumerable ReleaseDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable Release(PXAdapter adapter, ReleaseDelegate baseMethod)
        {
            if (FleetCards.Select().Count > 0)
            {
                var graph = PXGraph.CreateInstance<ExpenseClaimDetailEntry>();
                foreach (EPExpenseFleetCard fc in FleetCards.Select())
                {
                    EPExpenseClaimDetails det = PXSelect<
                        EPExpenseClaimDetails,
                        Where<EPExpenseClaimDetails.claimDetailID, Equal<Required<EPExpenseClaimDetails.claimDetailID>>>>
                        .Select(Base, fc.ClaimDetailID);
                    if (det != null)
                    {
                        det.Released = true;
                        det.Status = EPExpenseClaimDetailsStatus.ReleasedStatus;
                        graph.ClaimDetails.Update(det);

                        graph.Persist();
                    }
                }
            }

            if (Base.ExpenseClaimDetails.Select().Count > 0)
                return baseMethod(adapter);
            
            // Force close if there are no expense claim details in standard table
            ForceClose(Base.ExpenseClaim.Current);  

            return adapter.Get();
        }
        #endregion

        #region Event Handlers
        public void EPExpenseClaimDetails_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (EPExpenseClaimDetails)e.Row;
            if (row == null) return;

            InventoryItem item = PXSelect<
                InventoryItem,
                Where<InventoryItem.inventoryID, Equal<Required<InventoryItem.inventoryID>>>>
                .Select(Base, row.InventoryID);
            if (item != null)
            {
                var itemExt = PXCache<InventoryItem>.GetExtension<InventoryItemExtFC>(item);

                PXUIFieldAttribute.SetEnabled<EPExpenseClaimDetailsExtFC.usrFleetCardID>(cache, row, itemExt.UsrIsFleetCard == true);
            }
        }

        public void EPExpenseClaimDetails_CuryExtCost_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            var row = (EPExpenseClaimDetails)e.Row;
            if (row == null) return;

            decimal? cost = (decimal?)e.NewValue;

            var rowExt = PXCache<EPExpenseClaimDetails>.GetExtension<EPExpenseClaimDetailsExtFC>(row);
            EPFleetCard card = FleetCardSelect.Select(rowExt.UsrFleetCardID);
            if(card != null)
                if (ExceedsLimit(card, cost))
                    throw new PXSetPropertyException("Claim amount exceeds credit limit for this fleet card. Please change to continue.");
        }
        #endregion

        public void ForceClose(EPExpenseClaim claim)
        {
            Base.ExpenseClaim.SetValueExt<EPExpenseClaim.released>(claim, true);
            Base.ExpenseClaim.SetValueExt<EPExpenseClaim.status>(claim, EPExpenseClaimStatus.ReleasedStatus);

            Base.ExpenseClaim.Update(claim);

            Base.Persist();
        }

        public bool ExceedsLimit(EPFleetCard card, decimal? cost)
        {
            if (card != null)
                if (cost > card.CreditLimit)
                    return true;

            return false;
        }
    }
}]]></CDATA>
</Graph>