﻿<Graph ClassName="RCLJobOrder" Source="#CDATA" IsNew="True" FileType="NewDac">
    <CDATA name="Source"><![CDATA[using System;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.PO;
using PX.Objects.RQ;

namespace RCLJO
{
    [Serializable]
    [PXCacheName("RCLJobOrder")]
    public class RCLJobOrder : IBqlTable
    {
        #region JORefNbr
        [PXDBString(20, IsKey = true, IsUnicode = true, InputMask = "")]
        [PXSelector(typeof(RCLJobOrder.jORefNbr),
          typeof(RCLJobOrder.jORefNbr),
          typeof(RCLJobOrder.reqRefNbr),
          typeof(RCLJobOrder.project) 
        )]
        [AutoNumber(typeof(RCLJobOrderSetup.jobOrderNumberingID), typeof(AccessInfo.businessDate))]
        [PXUIField(DisplayName = "JO Ref. Nbr.")]
        public virtual string JORefNbr { get; set; }
        public abstract class jORefNbr : PX.Data.BQL.BqlString.Field<jORefNbr> { }
        #endregion

        #region ReqRefNbr
        [PXDBString(20, IsUnicode = true, InputMask = "", IsKey = true)]
        [PXDefault()]
        [PXSelector(typeof(RQRequest.orderNbr),
          typeof(RQRequest.orderNbr),
          typeof(RQRequest.reqClassID),
          typeof(RQRequest.description),
          typeof(RQRequest.employeeID),
          typeof(RQRequest.locationID),
          DescriptionField = typeof(RQRequest.description)
        )]
        [PXUIField(DisplayName = "Request Ref. Nbr.", Enabled = true)]
        public virtual string ReqRefNbr { get; set; }
        public abstract class reqRefNbr : PX.Data.BQL.BqlString.Field<reqRefNbr> { }
        #endregion

        #region Project
        [PXDBString(256, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Project")]
        public virtual string Project { get; set; }
        public abstract class project : PX.Data.BQL.BqlString.Field<project> { }
        #endregion

        #region ScheduledDate
        [PXDBDate()]
        [PXDefault(typeof(AccessInfo.businessDate))]
        [PXUIField(DisplayName = "Scheduled Date")]
        public virtual DateTime? ScheduledDate { get; set; }
        public abstract class scheduledDate : PX.Data.BQL.BqlDateTime.Field<scheduledDate> { }
        #endregion

        #region BranchID
        [PXDBInt()]
        [PXSelector(typeof(Search<Branch.branchID, Where<Branch.active, Equal<True>>>),
          typeof(Branch.branchCD),
          typeof(Branch.acctName),
          typeof(Branch.active),
          SubstituteKey = typeof(Branch.branchCD),
          DescriptionField = typeof(Branch.acctName)
        )]
        [PXDefault(typeof(AccessInfo.branchID))]
        [PXUIField(DisplayName = "Branch")]
        public virtual int? BranchID { get; set; }
        public abstract class branchID : PX.Data.BQL.BqlInt.Field<branchID> { }
        #endregion

        #region DeptID
        [PXDBString(10)]
        [PXSelector(typeof(Search<EPDepartment.departmentID>))]
        [PXDefault(typeof(RQRequest.departmentID), PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Department")]
        public virtual string DeptID { get; set; }
        public abstract class deptID : PX.Data.BQL.BqlInt.Field<deptID> { }
        #endregion

        #region SampleProvided
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Sample Provided")]
        public virtual bool? SampleProvided { get; set; }
        public abstract class sampleProvided : PX.Data.BQL.BqlBool.Field<sampleProvided> { }
        #endregion

        #region IsThirdParty
        [PXDBBool()]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Third Party")]
        public virtual bool? IsThirdParty { get; set; }
        public abstract class isThirdParty : PX.Data.BQL.BqlBool.Field<isThirdParty> { }
        #endregion

        #region Instruction
        [PXDBString(256, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Job Request/Instruction")]
        public virtual string Instruction { get; set; }
        public abstract class instruction : PX.Data.BQL.BqlString.Field<instruction> { }
        #endregion

        #region ServDescr
        [PXDBString(220, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Service Description")]
        public virtual string ServDescr { get; set; }
        public abstract class servDescr : PX.Data.BQL.BqlString.Field<servDescr> { }
        #endregion

        #region Location
        [PXDBString(30)]
        [PXUIField(DisplayName = "Section/Location")]
        public virtual string Location { get; set; }
        public abstract class location : PX.Data.BQL.BqlInt.Field<location> { }
        #endregion

        #region POType
        [PXDBString(2, IsFixed = true, InputMask = "")]
        [PXDefault(POOrderType.RegularOrder)]
        [POOrderType.List()]
        [PXUIField(DisplayName = "PO Type", Visible = false, Visibility = PXUIVisibility.Invisible)]
        public virtual string POType { get; set; }
        public abstract class pOType : PX.Data.BQL.BqlString.Field<pOType> { }
        #endregion

        #region PONbr
        [PXDBString(15, IsUnicode = true, InputMask = "")]
        [PXSelector(typeof(Search<POOrder.orderNbr, Where<POOrder.orderType, Equal<Current<RCLJobOrder.pOType>>>>),
          typeof(POOrder.orderNbr),
          typeof(POOrder.orderType),
          typeof(POOrder.status),
          typeof(POOrder.orderDate),
          typeof(POOrder.employeeID),
          typeof(POOrder.orderDesc),
          typeof(POOrder.vendorID),
          typeof(POOrder.vendorID_Vendor_acctName),
          SubstituteKey = typeof(POOrder.orderNbr),
          DescriptionField = typeof(POOrder.orderDesc)
        )]
        [PXUIField(DisplayName = "PO Nbr")]
        public virtual string PONbr { get; set; }
        public abstract class pONbr : PX.Data.BQL.BqlString.Field<pONbr> { }
        #endregion
        
        #region VendorID
        [Vendor]
        public virtual int? VendorID { get; set; }
        public abstract class vendorID : PX.Data.BQL.BqlInt.Field<vendorID> { }
        #endregion

        #region Assessment
        [PXDBString(320, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Assessment")]
        public virtual string Assessment { get; set; }
        public abstract class assessment : PX.Data.BQL.BqlString.Field<assessment> { }
        #endregion

        #region Recommendation
        [PXDBString(320, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Recommendation")]
        public virtual string Recommendation { get; set; }
        public abstract class recommendation : PX.Data.BQL.BqlString.Field<recommendation> { }
        #endregion

        #region CompletionDate
        [PXDBDateAndTime(DisplayNameDate = "Date Completed", DisplayNameTime = "Time Completed")]
        [PXUIField(DisplayName = "Completion Date")]
        public virtual DateTime? CompletionDate { get; set; }
        public abstract class completionDate : PX.Data.BQL.BqlDateTime.Field<completionDate> { }
        #endregion

        #region PreparedBy
        [PXDBInt]
        [PXEPEmployeeSelector]
        [PXUIField(DisplayName = "Prepared By")]
        public virtual int? PreparedBy { get; set; }
        public abstract class preparedBy : PX.Data.BQL.BqlGuid.Field<preparedBy> { }
        #endregion

        #region ConformedBy
        [PXDBInt]
        [PXEPEmployeeSelector]
        [PXUIField(DisplayName = "Conformed By")]
        public virtual int? ConformedBy { get; set; }
        public abstract class conformedBy : PX.Data.BQL.BqlGuid.Field<conformedBy> { }
        #endregion

        #region AcknowledgedBy
        [PXDBInt]
        [PXEPEmployeeSelector]
        [PXUIField(DisplayName = "Acknowledged By")]
        public virtual int? AcknowledgedBy { get; set; }
        public abstract class acknowledgedBy : PX.Data.BQL.BqlString.Field<acknowledgedBy> { }
        #endregion

        #region NotedBy
        [PXDBInt]
        [PXEPEmployeeSelector]
        [PXUIField(DisplayName = "Noted By")]
        public virtual int? NotedBy { get; set; }
        public abstract class notedBy : PX.Data.BQL.BqlString.Field<notedBy> { }
        #endregion

        #region Closed
        [PXDBBool]
        [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
        [PXUIField(DisplayName = "Closed")]
        public virtual bool? Closed { get; set; }
        public abstract class closed : PX.Data.BQL.BqlBool.Field<closed> { }
        #endregion

        #region Tstamp
        [PXDBTimestamp()]
        [PXUIField(DisplayName = "Tstamp")]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion

        #region NoteID
        [PXNote]
        [PXUIField(DisplayName = "Note ID")]
        public virtual Guid? NoteID { get; set; }
        public abstract class noteID : PX.Data.BQL.BqlGuid.Field<noteID> { }
        #endregion

        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion
    }
}]]></CDATA>
</Graph>