﻿<Graph ClassName="RQRequestEntryExtJO" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PX.Data;
using PX.Objects.PO;
using PX.Objects.CS;
using PX.Objects.AP;
using PX.Objects.EP;
using PX.Objects.CR;
using System.Collections;
using PX.Data.DependencyInjection;
using PX.LicensePolicy;
using PX.Objects.IN;
using PX.Objects.AR;
using PX.Objects.GL;
using PX.Objects.CM;
using PX.Objects.GL.FinPeriods;
using PX.Objects.GL.FinPeriods.TableDefinition;
using PX.TM;
using PX.Objects.Common;
using PX.Objects.Common.Bql;
using PX.Objects;
using PX.Objects.RQ;
using RCLJO;
using PX.Data.BQL.Fluent;
using PX.Objects.FA;
using P2P;
using PX.Web.UI;

namespace PX.Objects.RQ
{
    public class RQRequestEntryExtJO : PXGraphExtension<RQRequestEntry>
    {
        [PXCopyPasteHiddenView]
        public PXSelect<
            RCLJobOrder,
            Where<RCLJobOrder.reqRefNbr, Equal<Current<RQRequest.orderNbr>>>>
            JobOrder;

        [PXCopyPasteHiddenView]
        public PXSelect<
            RCLJobOrder,
            Where<RCLJobOrder.jORefNbr, Equal<Current<RCLJobOrder.jORefNbr>>>>
            CurrentJobOrder;
        public PXSetup<RCLJobOrderSetup> Setup;

        /* Job Order Tables */
        [PXCopyPasteHiddenView]
        public SelectFrom<RCLMachineServ>
            .LeftJoin<FADetails>.On<FADetails.assetID.IsEqual<RCLMachineServ.assetID>>
            .Where<RCLMachineServ.jORefNbr.IsEqual<RCLJobOrder.jORefNbr.FromCurrent>>.View MachineServices;

        [PXCopyPasteHiddenView]
        public SelectFrom<RCLInfraServ>.Where<RCLInfraServ.jORefNbr.IsEqual<RCLJobOrder.jORefNbr.FromCurrent>>.View InfraServices;
        //public SelectFrom<RCLBom>
        //    .LeftJoin<INTran>.On<INTran.docType.IsEqual<RCLBom.iNType>.And<INTran.refNbr.IsEqual<RCLBom.refNbr>>>
        //    .LeftJoin<InventoryItem>.On<InventoryItem.inventoryID.IsEqual<INTran.inventoryID>>
        //    .Where<RCLBom.jORefNbr.IsEqual<RCLJobOrder.jORefNbr.FromCurrent>>.View BillOfMaterials;

        [PXCopyPasteHiddenView]
        public PXSelectJoin<
            RCLBom, 
            LeftJoin<INTran, 
                On<INTran.docType, Equal<RCLBom.iNType>, 
                And<INTran.refNbr, Equal<RCLBom.iNRefNbr>, 
                And<INTran.lineNbr, Equal<RCLBom.refLineNbr>>>>>,
            Where<RCLBom.jORefNbr, Equal<Current<RCLJobOrder.jORefNbr>>>>
            BillOfMaterials;

        [PXImport(typeof(RCLJobOrder))]
        [PXCopyPasteHiddenView]
        public SelectFrom<RCLLaborServ>.Where<RCLLaborServ.jORefNbr.IsEqual<RCLJobOrder.jORefNbr.FromCurrent>>.View LaborHours;

        [PXCopyPasteHiddenView]
        public SelectFrom<RCLSanitationCheck>.Where<RCLSanitationCheck.jORefNbr.IsEqual<RCLJobOrder.jORefNbr.FromCurrent>>.View SanitationConformances;

        [PXImport(typeof(RCLJobOrder))]
        [PXCopyPasteHiddenView]
        public SelectFrom<RCLJORelatedRQ>
            .LeftJoin<RQRequest>.On<RQRequest.orderNbr.IsEqual<RCLJORelatedRQ.rQOrderNbr>>
            .Where<RCLJORelatedRQ.jORefNbr.IsEqual<RCLJobOrder.jORefNbr.FromCurrent>>.View RelatedRQ;

        [PXCopyPasteHiddenView]
        public SelectFrom<RCLToolsAndEquipment>.Where<RCLToolsAndEquipment.jORefNbr.IsEqual<RCLJobOrder.jORefNbr.FromCurrent>>.View ToolsAndEquipment;

        [PXCopyPasteHiddenView]
        public SelectFrom<RCLJORelatedRQ>.LeftJoin<RCLJobOrder>.On<RCLJORelatedRQ.jORefNbr.IsEqual<RCLJobOrder.jORefNbr>>.Where<RCLJORelatedRQ.rQOrderNbr.IsEqual<RQRequest.orderNbr.FromCurrent>>.View RelatedJO;

        public override void Initialize()
        {
            Base.report.AddMenuAction(PrintJO);
            Base.report.AddMenuAction(PrintSR);

            MainCommand.AddMenuAction(RelateToJO);
            MainCommand.AddMenuAction(CreateJO);
            MainCommand.AddMenuAction(CloseJO);
            
            base.Initialize();
        }

        public void RQRequest_UsrOrderTime_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (RQRequest)e.Row;
            if (row == null) return;
            
            e.NewValue = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Taipei Standard Time");
        }
        
        public void RQRequest_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (RQRequest)e.Row;
            if (row == null) return;

            RelatedJO.AllowSelect = RelatedJO.Select().Count > 0;

            RQRequestClass reqClass = Base.reqclass.Current;

            JobOrder.AllowSelect = false;
            MainCommand.SetVisible(false);

            var rowExt = PXCache<RQRequest>.GetExtension<RQRequestExtJO>(row);
            SetAsThirdParty.SetVisible(rowExt.UsrShowInReq == false && row.Approved == true);

            if (reqClass != null)
            {
                var reqClassExt = PXCache<RQRequestClass>.GetExtension<RQRequestClassExt>(reqClass);

                MainCommand.SetVisible(reqClassExt.UsrRelateToJO == true || reqClassExt.UsrEnableJobOrder == true);

                RelateToJO.SetEnabled(reqClassExt.UsrRelateToJO == true);
                CreateJO.SetEnabled(reqClassExt.UsrEnableJobOrder == true && 
                    row.tstamp != null && JobOrder.Select().Count == 0 && row.Approved == false);
            }
        }

        public void RCLJoborder_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (RCLJobOrder)e.Row;
            if (row == null) return;

            var rq = Base.Document.Current;
            RQRequestClass reqClass = Base.reqclass.Current;
            if (reqClass != null)
            {
                var reqClassExt = PXCache<RQRequestClass>.GetExtension<RQRequestClassExt>(reqClass);

                RQRequestType reqType = PXSelect<
                    RQRequestType,
                    Where<RQRequestType.rQTypeID, Equal<Current<RQRequestP2P.usrRQTypeID>>>>
                    .Select(Base);

                if (reqClassExt.UsrEnableJobOrder == true)
                {
                    JobOrder.AllowSelect = JobOrder.Select().Count > 0;

                    CreateJO.SetEnabled(rq.tstamp != null && JobOrder.Select().Count == 0 && rq.Approved == false);
                    RCLJobOrder jo = JobOrder.Current;

                    if (rq.Approved == true && reqType != null && jo != null)
                    {
                        JobOrder.Cache.AllowUpdate =
                           MachineServices.Cache.AllowUpdate =
                           InfraServices.Cache.AllowUpdate =
                           BillOfMaterials.Cache.AllowUpdate =
                           SanitationConformances.Cache.AllowUpdate =
                           RelatedRQ.Cache.AllowUpdate =
                           LaborHours.Cache.AllowUpdate =
                           RelatedRQ.Cache.AllowUpdate =
                           ToolsAndEquipment.Cache.AllowUpdate =

                            JobOrder.Cache.AllowInsert =
                           MachineServices.Cache.AllowInsert =
                           InfraServices.Cache.AllowInsert =
                           BillOfMaterials.Cache.AllowInsert =
                           SanitationConformances.Cache.AllowInsert =
                           LaborHours.Cache.AllowInsert =
                           RelatedRQ.Cache.AllowInsert =
                           ToolsAndEquipment.Cache.AllowInsert =

                          JobOrder.Cache.AllowDelete =
                           MachineServices.Cache.AllowDelete =
                           InfraServices.Cache.AllowDelete =
                           BillOfMaterials.Cache.AllowDelete =
                           SanitationConformances.Cache.AllowDelete =
                           LaborHours.Cache.AllowDelete =
                           RelatedRQ.Cache.AllowDelete =
                           ToolsAndEquipment.Cache.AllowDelete =

                           jo.Closed == false;



                        OpenChecklist.SetEnabled(jo.Closed == true);
                        ClearChecklist.SetEnabled(jo.Closed == true);
                        RefreshChecklist.SetEnabled(jo.Closed == true);

                        CloseJO.SetEnabled(jo.Closed == false);
                    }
                }
            }
        }

        public void RQRequest_ReqClassID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (RQRequest)e.Row;
            if (row == null) return;

            var reqClass = Base.reqclass.Current;
            if (reqClass != null)
            {
                var reqClassExt = PXCache<RQRequestClass>.GetExtension<RQRequestClassExtRFT>(reqClass);

                if (reqClassExt.UsrBlockInRequisitions == true)
                    cache.SetValue<RQRequestExtJO.usrShowInReq>(row, false);
                else //if false or null
                    cache.SetValue<RQRequestExtJO.usrShowInReq>(row, true);
            }
        }

        protected void RCLJobOrder_PONbr_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (RCLJobOrder)e.Row;
            if (row == null) return;

            POOrder order = PXSelect<
                POOrder, 
                Where<POOrder.orderType, Equal<Required<POOrder.orderType>>, 
                    And<POOrder.orderNbr, Equal<Required<POOrder.orderNbr>>>>>
                .Select(Base, row.POType, row.PONbr);

            if (order != null)
                cache.SetValue<RCLJobOrder.vendorID>(row, order.VendorID);
        }

        protected void RCLBom_RefLineNbr_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (RCLBom)e.Row;
            if (row == null) return;
            
            INTran lineItem = PXSelect<
                INTran,
                Where<INTran.refNbr, Equal<Required<INTran.refNbr>>,
                    And<INTran.lineNbr, Equal<Required<INTran.lineNbr>>,
                    And<INTran.docType, Equal<Required<INTran.docType>>>>>>
                .Select(Base, row.INRefNbr, row.RefLineNbr, row.INType);

            if (lineItem != null)
                cache.SetValue<RCLBom.qty>(row, lineItem.Qty);
        }

        protected void RCLBom_Qty_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            var row = (RCLBom)e.Row;
            if (row == null) return;

            INTran lineItem = PXSelect<
                INTran,
                Where<INTran.refNbr, Equal<Required<INTran.refNbr>>,
                    And<INTran.lineNbr, Equal<Required<INTran.lineNbr>>,
                    And<INTran.docType, Equal<Required<INTran.docType>>>>>>
                .Select(Base, row.INRefNbr, row.RefLineNbr, row.INType);

            if (lineItem != null)
            {
                if ((decimal)e.NewValue > lineItem.Qty)
                {
                    e.NewValue = lineItem.Qty;
                    cache.RaiseExceptionHandling<RCLBom.qty>(row, e.NewValue, new PXSetPropertyException("Item qty must not exceed the receipt qty", PXErrorLevel.Warning));
                }
            }
            else
                e.NewValue = 0m;
        }

        protected void RCLMachineServ_BranchID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (RCLMachineServ)e.Row;
            if (row == null) return;

            cache.SetValue<RCLMachineServ.assetID>(row, null);

        }

        public PXAction<RQRequest> MainCommand;
        [PXButton(SpecialType = PXSpecialButtonType.Report, Tooltip = "Job Order Actions", MenuAutoOpen = true, CommitChanges = true)]
        [PXUIField(DisplayName = "Job Order", MapEnableRights = PXCacheRights.Delete, MapViewRights = PXCacheRights.Delete)]
        protected IEnumerable mainCommand(PXAdapter adapter)
        {
            return adapter.Get();
        }

        #region RelateToJO
        public PXAction<RQRequest> RelateToJO;
        [PXButton]
        [PXUIField(DisplayName = "Relate to JO")]
        public void relateToJO()
        {
            if (JOView.AskExt() == WebDialogResult.OK)
            {
                PXLongOperation.StartOperation(Base, delegate ()
                {
                    JOSelect jo = JOView.Current;
                    RQRequest rq = Base.Document.Current;

                    RCLJORelatedRQ rqJO = PXSelect<
                        RCLJORelatedRQ,
                        Where<RCLJORelatedRQ.jORefNbr, Equal<Required<RCLJORelatedRQ.jORefNbr>>,
                            And<RCLJORelatedRQ.rQOrderNbr, Equal<Required<RCLJORelatedRQ.rQOrderNbr>>>>>
                        .Select(Base, jo.JORefNbr, Base.Document.Current.OrderNbr);
                    if (rqJO != null)
                        throw new PXException("This request is already related to JO#" + jo.JORefNbr);
                    
                    RelatedRQ.Insert(new RCLJORelatedRQ()
                    {
                        RQOrderNbr = rq.OrderNbr,
                        JORefNbr = jo.JORefNbr
                    });

                    Base.Persist();

                    var graph = PXGraph.CreateInstance<RQRequestEntry>();
                    graph.Clear();

                    RCLJobOrder order = PXSelect< 
                        RCLJobOrder,
                        Where<RCLJobOrder.jORefNbr, Equal<Required<RCLJobOrder.jORefNbr>>>>
                        .Select(Base, jo.JORefNbr);

                    graph.Document.Current = graph.Document.Search<RQRequest.orderNbr>(order.ReqRefNbr);

                    throw new PXRedirectRequiredException(graph, true, null);
                });
            }
        }

        public PXFilter<JOSelect> JOView;

        [Serializable]
        public class JOSelect : IBqlTable
        {
            #region JORefNbr
            [PXString]
            [PXUIField(DisplayName = "JO Reference Nbr.")]
            [PXSelector(typeof(Search<RCLJobOrder.jORefNbr>), typeof(RCLJobOrder.jORefNbr),
                typeof(RCLJobOrder.project), typeof(RCLJobOrder.scheduledDate), typeof(RCLJobOrder.reqRefNbr))]
            public virtual string JORefNbr { get; set; }
            public abstract class jORefNbr : PX.Data.BQL.BqlInt.Field<jORefNbr> { }
            #endregion
        }
        #endregion

        public PXAction<RQRequest> CloseJO;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Close JO")]
        public void closeJO()
        {
            var jo = JobOrder.Current;
            var rq = Base.Document.Current;

            if (Base.Document.Ask("Confirm Action", "Continue to close JO#" + jo.JORefNbr + " ?", MessageButtons.OKCancel) == WebDialogResult.OK)
            {
                PXLongOperation.StartOperation(Base, delegate ()
                {
                    JobOrder.Cache.SetValueExt<RCLJobOrder.closed>(jo, true);
                    JobOrder.Update(jo);

                    Base.Document.SetValueExt<RQRequest.status>(rq, RQRequestStatus.Closed);
                    Base.Document.SetValueExt<RQRequest.openOrderQty>(rq, 0m);
                    Base.Document.Update(rq);

                    Base.Persist();
                });
            }
        }

        public PXAction<RQRequest> CreateJO;
        [PXButton]
        [PXUIField(DisplayName = "Create JO", Enabled = false)]
        public void createJO()
        {
            JobOrder.Insert(new RCLJobOrder()
            {
                ReqRefNbr = Base.Document.Current.OrderNbr
            });
            Base.Persist();
        }

        public PXAction<RQRequest> PrintJO;
        [PXButton]
        [PXUIField(DisplayName = "Print JO")]
        public void printJO()
        {
            RCLJobOrder row = JobOrder.Current;
            if (row == null) return;

            if (row != null && row.JORefNbr != null)
            {
                //// Add your report parameters to a Dictionary<string, string> collection.
                //// The dictionary key is the parameter name as shown in the Report Designer.
                //// The dictionary value is the value you assign to this parameter.
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters["JORefNbr"] = row.JORefNbr;

                //// Provide the report ID
                string reportID = "JORCL001";

                //// Provide a title name for your report page
                string reportName = "Job Order Form";

                //// Redirect to report page by throwing a PXReportRequiredException object 
                //throw new PXReportRequiredException(parameters, reportID, reportName);
                throw new PXReportRequiredException(parameters, reportID, PXBaseRedirectException.WindowMode.New, reportName);
            }
        }

        public PXAction<RQRequest> PrintSR;
        [PXButton]
        [PXUIField(DisplayName = "Print Service Report")]
        public void printSR()
        {
            RCLJobOrder row = JobOrder.Current;
            if (row == null) return;

            if (row != null && row.JORefNbr != null)
            {
                //// Add your report parameters to a Dictionary<string, string> collection.
                //// The dictionary key is the parameter name as shown in the Report Designer.
                //// The dictionary value is the value you assign to this parameter.
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters["JORefNbr"] = row.JORefNbr;

                //// Provide the report ID
                string reportID = "PSRCL001";

                //// Provide a title name for your report page
                string reportName = "Service Report Form";

                //// Redirect to report page by throwing a PXReportRequiredException object 
                //throw new PXReportRequiredException(parameters, reportID, reportName);
                throw new PXReportRequiredException(parameters, reportID, PXBaseRedirectException.WindowMode.New, reportName);
            }
        }

        public PXAction<RQRequest> OpenChecklist;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Open Checklist")]
        protected void openChecklist()
        {
            SanitationMaint graph = PXGraph.CreateInstance<SanitationMaint>();

            PXRedirectHelper.TryRedirect(graph, PXRedirectHelper.WindowMode.NewWindow);
        }

        public PXAction<RQRequest> RefreshChecklist;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Load")]
        protected virtual void refreshChecklist()
        {
            foreach (RCLSanitationList checkItem in PXSelectReadonly<
                RCLSanitationList, 
                Where<RCLSanitationList.isActive, Equal<True>>>
                .Select(Base))
            {
                RCLSanitationCheck newItem = SanitationConformances.Insert();
                newItem.Checkpoint = checkItem.Checkpoint;
                SanitationConformances.Update(newItem);
            }
            Base.Actions.PressSave();
        }

        public PXAction<RQRequest> ClearChecklist;
        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Clear")]
        protected void clearChecklist()
        {
            if (SanitationConformances.Select().Count > 0)
            {
                foreach (RCLSanitationCheck checkItem in SanitationConformances.Select())
                {
                    SanitationConformances.Delete(checkItem);
                }
                Base.Actions.PressSave();
            }
        }

        public PXAction<RQRequest> GoToReq;
        [PXButton]
        [PXUIField(DisplayName = "")]
        public void goToReq()
        {
            RCLJORelatedRQ joRQ = RelatedRQ.Current;

            var graph = PXGraph.CreateInstance<RQRequestEntry>();

            RCLJobOrder jo = PXSelect<RCLJobOrder, Where<RCLJobOrder.jORefNbr, Equal<Required<RCLJobOrder.jORefNbr>>>>.Select(Base, joRQ.JORefNbr);
            if (jo != null)
            {
                graph.Document.Current = graph.Document.Search<RQRequest.orderNbr>(jo.ReqRefNbr);

                throw new PXRedirectRequiredException(graph, true, null);
            }
        }

        public PXAction<RQRequest> SetAsThirdParty;
        [PXButton]
        [PXUIField(DisplayName = "Third Party?")]
        public void setAsThirdParty()
        {
            RQRequest req = Base.Document.Current;

            if (req != null)
            {
                Base.Document.SetValueExt<RQRequestExtJO.usrShowInReq>(req, true);
                Base.Document.Update(req);

                Base.Persist();
            }
        }
    }
}]]></CDATA>
</Graph>