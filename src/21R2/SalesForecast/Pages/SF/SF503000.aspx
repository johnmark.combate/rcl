<%@ Page Language="C#" MasterPageFile="~/MasterPages/ListView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="SF503000.aspx.cs" Inherits="Page_SF503000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/ListView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="SF.SFDisattachSOProcess"
        PrimaryView="SOToDisattach"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phL" runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="PrimaryInquire" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="SOToDisattach">
			    <Columns>
				<px:PXGridColumn AllowCheckAll="True" DataField="UsrSFSelected" Width="30" Type="CheckBox" TextAlign="Center" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__OrderType" Width="70" />
				<px:PXGridColumn DataField="SOOrder__OrderNbr" Width="140" LinkCommand="SOOrderEntry" />
				<px:PXGridColumn DataField="SOOrder__CustomerID" Width="140" />
				<px:PXGridColumn DataField="SOOrder__CustomerID_BAccountR_acctName" Width="220" />
				<px:PXGridColumn DataField="SOOrder__OrderDate" Width="90" />
				<px:PXGridColumn DataField="InventoryID" Width="70" LinkCommand="InventoryItemMaint" />
				<px:PXGridColumn DataField="InventoryID_description" Width="280" />
				<px:PXGridColumn DataField="OrderQty" Width="100" />
				<px:PXGridColumn DataField="UsrSFVarianceQty" Width="100" />
				<px:PXGridColumn DataField="UOM" Width="72" />
				<px:PXGridColumn DataField="SiteID" Width="140" /></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
