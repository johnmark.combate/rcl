<%@ Page Language="C#" MasterPageFile="~/MasterPages/TabView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="SF101000.aspx.cs" Inherits="Page_SF101000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/TabView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="SF.SFSetupMaint"
        PrimaryView="Setup"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXTab DataMember="Setup" ID="tab" runat="server" DataSourceID="ds" Height="150px" Style="z-index: 100" Width="100%" AllowAutoHide="false">
		<Items>
			<px:PXTabItem Text="General Settings">
				
				<Template>
					<px:PXLayoutRule runat="server" ID="CstPXLayoutRule2" StartColumn="True" ></px:PXLayoutRule>
									<px:PXSelector runat="server" ID="CstPXSelector3" DataField="SFNumberingID" AllowEdit="True" /></Template></px:PXTabItem>
			<px:PXTabItem Text="Approval">
			
				<Template>
								<px:PXPanel runat="server" ID="CstPanel7">
									<px:PXLayoutRule LabelsWidth="S" runat="server" ID="CstLayoutRule9" ControlSize="XM" ></px:PXLayoutRule>
									<px:PXCheckBox CommitChanges="True" runat="server" ID="CstPXCheckBox8" DataField="SFRequestApproval" ></px:PXCheckBox></px:PXPanel>
								<px:PXGrid SkinID="Details" Width="100%" runat="server" ID="CstPXGrid5">
									<Levels>
										<px:PXGridLevel DataMember="ApprovalSetup" >
											<Columns>
												<px:PXGridColumn DataField="AssignmentMapID" Width="70" />
												<px:PXGridColumn DataField="AssignmentNotificationID" Width="280" /></Columns></px:PXGridLevel></Levels>
									<AutoSize Enabled="True" ></AutoSize></px:PXGrid></Template></px:PXTabItem>
		</Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" ></AutoSize>
	</px:PXTab>
</asp:Content>