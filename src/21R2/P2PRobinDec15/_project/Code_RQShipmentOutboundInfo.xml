﻿<Graph ClassName="RQShipmentOutboundInfo" Source="#CDATA" IsNew="True" FileType="NewDac">
    <CDATA name="Source"><![CDATA[using System;
using PX.Data;
using PX.Objects.IN;
using PX.Objects.RQ;

namespace P2P
{
    [Serializable]
    [PXCacheName("RQShipmentOutboundInfo")]
    public class RQShipmentOutboundInfo : IBqlTable
    {
        #region DocName
        [PXDBString(50, IsUnicode = true, InputMask = "", IsKey = true)]
        [PXUIField(DisplayName = "Doc Name")]
        [PXDefault(typeof(RQInboundDocument.docName))]
        public virtual string DocName { get; set; }
        public abstract class docName : PX.Data.BQL.BqlString.Field<docName> { }
        #endregion

        #region ReqNbr
        [PXDBString(20, IsUnicode = true, InputMask = "", IsKey = true)]
        [PXUIField(DisplayName = "Request Nbr.")]
        [PXDBDefault(typeof(RQRequest.orderNbr))]
        [PXParent(typeof(Select<
            RQInboundDocument,
            Where<RQInboundDocument.docName, Equal<Current<docName>>,
                And<RQInboundDocument.reqNbr, Equal<Current<reqNbr>>>>>))]
        public virtual string ReqNbr { get; set; }
        public abstract class reqNbr : PX.Data.BQL.BqlString.Field<reqNbr> { }
        #endregion
        
        #region InventoryID
        [PXDBInt(IsKey = true)]
        [PXUIField(DisplayName = "Inventory ID")]
        [PXSelector(typeof(Search<InventoryItem.inventoryID>), SubstituteKey = typeof(InventoryItem.inventoryCD),
            DescriptionField = typeof(InventoryItem.descr))]
        public virtual int? InventoryID { get; set; }
        public abstract class inventoryID : PX.Data.BQL.BqlInt.Field<inventoryID> { }
        #endregion

        #region Qty
        [PXDBDecimal()]
        [PXDefault(TypeCode.Decimal, "0.00")]
        [PXUIField(DisplayName = "Qty")]
        public virtual Decimal? Qty { get; set; }
        public abstract class qty : PX.Data.BQL.BqlDecimal.Field<qty> { }
        #endregion

        #region CBM
        [PXDBDecimal()]
        [PXDefault(TypeCode.Decimal, "0.00")]
        [PXUIField(DisplayName = "CBM", Enabled = false)]
        public virtual Decimal? CBM { get; set; }
        public abstract class cBM : PX.Data.BQL.BqlDecimal.Field<cBM> { }
        #endregion

        #region Weight
        [PXDBDecimal()]
        [PXDefault(TypeCode.Decimal, "0.00")]
        [PXUIField(DisplayName = "Weight", Enabled = false)]
        public virtual Decimal? Weight { get; set; }
        public abstract class weight : PX.Data.BQL.BqlDecimal.Field<weight> { }
        #endregion

        #region TotalWeight
        [PXDBDecimal()]
        [PXDefault(TypeCode.Decimal, "0.00")]
        [PXFormula(typeof(Mult<qty, weight>))]
        [PXUIField(DisplayName = "Total Weight", Enabled = false)]
        public virtual Decimal? TotalWeight { get; set; }
        public abstract class totalWeight : PX.Data.BQL.BqlDecimal.Field<totalWeight> { }
        #endregion

        #region TotalCBM
        [PXDBDecimal()]
        [PXDefault(TypeCode.Decimal, "0.00")]
        [PXFormula(typeof(Mult<qty, cBM>))]
        [PXUIField(DisplayName = "Total CBM", Enabled = false)]
        public virtual Decimal? TotalCBM { get; set; }
        public abstract class totalCBM : PX.Data.BQL.BqlDecimal.Field<totalCBM> { }
        #endregion

        #region PackageNo
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Package No.")]
        public virtual string PackageNo { get; set; }
        public abstract class packageNo : PX.Data.BQL.BqlString.Field<packageNo> { }
        #endregion

        #region Remarks
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Remarks")]
        public virtual string Remarks { get; set; }
        public abstract class remarks : PX.Data.BQL.BqlString.Field<remarks> { }
        #endregion

        #region CartonSize
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Carton Size")]
        public virtual string CartonSize { get; set; }
        public abstract class cartonSize : PX.Data.BQL.BqlString.Field<cartonSize> { }
        #endregion

        #region Carton
        [PXDBString(50, IsUnicode = true, InputMask = "")]
        [PXUIField(DisplayName = "Carton")]
        public virtual string Carton { get; set; }
        public abstract class carton : PX.Data.BQL.BqlString.Field<carton> { }
        #endregion

        #region Tstamp
        [PXDBTimestamp()]
        [PXUIField(DisplayName = "Tstamp")]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion

        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion
    }
}]]></CDATA>
</Graph>