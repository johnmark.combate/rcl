<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormTab.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="BC300001.aspx.cs" Inherits="Page_BC300001" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormTab.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="BOC.BCBillofCollectionEntry"
        PrimaryView="Collection">
		<CallbackCommands>

<px:PXDSCallbackCommand Name="AddDocs" Visible = "False"></px:PXDSCallbackCommand>
<px:PXDSCallbackCommand DependOnGrid="CstPXGrid47" Name="RemoveDoc" Visible = "False"></px:PXDSCallbackCommand>
<px:PXDSCallbackCommand Name="ClearDocs" Visible = "False"></px:PXDSCallbackCommand>
		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Collection" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector8" DataField="RefNbr" ></px:PXSelector>
			<px:PXSegmentMask CommitChanges="False" runat="server" ID="CstPXSegmentMask6" DataField="CustomerID" ></px:PXSegmentMask>
			<px:PXSegmentMask runat="server" ID="CstPXSegmentMask9" DataField="BranchID" ></px:PXSegmentMask>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit38" DataField="CounterDate" ></px:PXDateTimeEdit>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit18" DataField="DateSubmitted" ></px:PXDateTimeEdit>
			<px:PXLayoutRule runat="server" ID="CstLayoutRule17" ColumnSpan="2" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit15" DataField="Remarks" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule16" StartColumn="True" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit7" DataField="Location" ></px:PXTextEdit>
			<px:PXSegmentMask runat="server" ID="CstPXSegmentMask39" DataField="PreparedBy" />
			<px:PXSegmentMask runat="server" ID="CstPXSegmentMask21" DataField="Collector" ></px:PXSegmentMask>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit23" DataField="ReceiverName" ></px:PXTextEdit>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit20" DataField="ReceivedDate" ></px:PXDateTimeEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXTab ID="tab" runat="server" Width="100%" Height="150px" DataSourceID="ds" AllowAutoHide="false">
		<Items>
			
			<px:PXTabItem Text="Transactions">
				<Template>
					<px:PXGrid Width="100%" DataSourceID="ds" SkinID="Details" runat="server" ID="CstPXGrid47">
						<Levels>
							<px:PXGridLevel DataMember="ARDocs" >
								<Columns>
									<px:PXGridColumn DataField="DocType" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="RefNbr" Width="140" ></px:PXGridColumn>
									<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
									<px:PXGridColumn DataField="FinPeriodID" Width="72" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DocDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="DueDate" Width="90" ></px:PXGridColumn>
									<px:PXGridColumn DataField="CuryOrigDocAmt" Width="100" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ARInvoice__TermsID" Width="120" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ARInvoice__InvoiceNbr" Width="180" ></px:PXGridColumn>
									<px:PXGridColumn DataField="UsrRemarks" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn DataField="ARInvoice__UsrExtRefNbr" Width="140" />
									<px:PXGridColumn DataField="ARInvoice__UsrGRRef" Width="140" /></Columns>
								<RowTemplate>
									<px:PXSelector AllowEdit="True" runat="server" ID="CstPXSelector48" DataField="RefNbr" ></px:PXSelector></RowTemplate></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize>
<ActionBar>
			  <CustomItems>
                           <px:PXToolBarButton>
			      <AutoCallBack Target="ds" Command="AddDocs" ></AutoCallBack>
                            </px:PXToolBarButton>
                            <px:PXToolBarButton>
			      <AutoCallBack Target="ds" Command="RemoveDoc" ></AutoCallBack>
                            </px:PXToolBarButton>
                            <px:PXToolBarButton>
			      <AutoCallBack Target="ds" Command="ClearDocs" ></AutoCallBack>
                            </px:PXToolBarButton>


                          </CustomItems>
                        </ActionBar>

						<AutoCallBack Command="" ></AutoCallBack>
						<AutoCallBack Enabled="True" ></AutoCallBack></px:PXGrid></Template></px:PXTabItem>
			<px:PXTabItem Text="Follow Up" >
				<Template>
					<px:PXGrid SkinID="Details" Width="100%" runat="server" ID="CstPXGrid12">
						<Levels>
							<px:PXGridLevel DataMember="FollowUp" >
								<Columns>
									<px:PXGridColumn DataField="Remarks" Width="280" ></px:PXGridColumn>
									<px:PXGridColumn DataField="FollowUpDate" Width="90" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
						<AutoSize Enabled="True" ></AutoSize></px:PXGrid></Template></px:PXTabItem></Items>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
	</px:PXTab>
	<px:PXSmartPanel AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" CommandSourceID="ds" AutoCallBack-Target="BOCDetails" Height="" DesignView="Content" Width="500" CaptionVisible="True" Caption="Update Details" Key="Details" runat="server" ID="CstSmartPanel24">
		<px:PXFormView RenderStyle="Simple" DataMember="Details" runat="server" ID="BOCDetails" >
			<Template>
				<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector30" DataField="PreparedBy" ></px:PXSelector>
				<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit29" DataField="DateSubmitted" ></px:PXDateTimeEdit>
				<px:PXDateTimeEdit CommitChanges="True" runat="server" ID="CstPXDateTimeEdit31" DataField="ReceivedDate" ></px:PXDateTimeEdit>
				<px:PXLayoutRule runat="server" ID="CstPXLayoutRule33" StartColumn="True" ></px:PXLayoutRule>
				<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector28" DataField="Collector" ></px:PXSelector>
				<px:PXTextEdit CommitChanges="True" runat="server" ID="CstPXTextEdit32" DataField="ReceiverName" ></px:PXTextEdit></Template></px:PXFormView>
		<px:PXLayoutRule runat="server" ID="CstPXLayoutRule37" StartRow="True" ></px:PXLayoutRule>
		<px:PXPanel SkinID="Buttons" RenderStyle="Simple" runat="server" ID="CstPanel34" >
			<px:PXButton runat="server" ID="CstButton35" DialogResult="OK" Text="OK" ></px:PXButton>
			<px:PXButton DialogResult="Cancel" Text="Cancel" runat="server" ID="CstButton36" ></px:PXButton></px:PXPanel></px:PXSmartPanel>
	<px:PXSmartPanel AutoCallBack-Target="Docs" Height="450" AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" Caption="Add Documents" CaptionVisible="True" DesignView="Content" CommandSourceID="ds" Key="DocsToAdd" runat="server" ID="CstSmartPanel40">
		<px:PXGrid AllowPaging="False" SkinID="Inquire" runat="server" ID="Docs">
			<Levels>
				<px:PXGridLevel DataMember="DocsToAdd" >
					<Columns>
						<px:PXGridColumn AllowCheckAll="True" Type="CheckBox" CommitChanges="True" DataField="Selected" Width="60" ></px:PXGridColumn>
						<px:PXGridColumn DataField="DocType" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn DataField="RefNbr" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="DocDate" Width="90" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Status" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn DataField="FinPeriodID" Width="72" ></px:PXGridColumn>
						<px:PXGridColumn DataField="CuryOrigDocAmt" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Customer__AcctCD" Width="140" ></px:PXGridColumn></Columns>
					<RowTemplate>
						<px:PXSelector runat="server" ID="CstPXSelector46" DataField="RefNbr" AllowEdit="True" ></px:PXSelector></RowTemplate></px:PXGridLevel></Levels>
			<AutoSize Enabled="True" ></AutoSize>
			<ContentLayout OuterSpacing="Around" ></ContentLayout></px:PXGrid>
		<px:PXPanel runat="server" ID="CstPanel43" SkinID="Buttons" >
			<px:PXButton runat="server" ID="CstButton44" DialogResult="OK" Text="OK" ></px:PXButton>
			<px:PXButton DialogResult="Cancel" Text="Cancel" runat="server" ID="CstButton45" ></px:PXButton></px:PXPanel></px:PXSmartPanel></asp:Content>