﻿<Graph ClassName="ProdDetailExtPC" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections;
using System.Collections.Generic;
using PX.Objects.PO;
using System.Linq;
using PX.Data;
using PX.Data.EP;
using PX.Objects.CS;
using PX.Objects.IN;
using PX.Common;
using POLine = PX.Objects.PO.POLine;
using POOrder = PX.Objects.PO.POOrder;
using PX.Objects.AP;
using PX.Objects.CR;
using PX.Objects;

using ReqQtyCalculator;
using System.Data;
using PX.Objects.SO;
using PX.Objects.AM.Attributes;

namespace PX.Objects.AM
{
    public class ProdDetailExtPC : PXGraphExtension<ProdDetail>
    {
        #region Selects/Views
        public PXSelectJoin<
            PCProdItem,
            InnerJoin<AMProdMatl,
                On<AMProdMatl.lineID, Equal<PCProdItem.prodMatlLineNbr>,
                And<AMProdMatl.orderType, Equal<PCProdItem.orderType>,
                And<AMProdMatl.prodOrdID, Equal<PCProdItem.prodOrdID>,
                    And<AMProdMatl.operationID, Equal<PCProdItem.operationID>>>>>>,
            Where<PCProdItem.orderType, Equal<Current<AMProdOper.orderType>>,
                And<PCProdItem.prodOrdID, Equal<Current<AMProdOper.prodOrdID>>>>>
            MaterialComputation;

        [PXImport(typeof(AMProdItem))]
        public PXSelect<
            PCProdCalculation,
            Where<PCProdCalculation.orderType, Equal<Current<AMProdOper.orderType>>,
                And<PCProdCalculation.prodOrdID, Equal<Current<AMProdOper.prodOrdID>>,
                And<PCProdCalculation.prodMatlLineNbr, Equal<Current<PCProdItem.prodMatlLineNbr>>>>>>
            MaterialFormula;

        #endregion

        #region Actions
        /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 8/18/2021 12:50:05 PM
    [PURPOSE]: TO AUTOMATICALLY LOAD ALL ITEMS FROM MATERIALS TAB TO MATERIAL COMPUTATION TAB*/
        public PXAction<AMProdItem> LoadPCItems;
        [PXUIField(DisplayName = "Load Items")]
        [PXButton]
        public virtual IEnumerable loadPCItems(PXAdapter adapter)
        {
            foreach (AMProdMatl prodMatl in Base.ProdMatlRecords.Select())
            {
                if (!MaterialComputation.Select().RowCast<PCProdItem>().Where(p => p.ProdMatlLineNbr == prodMatl.LineID).Any())
                {
                    MaterialComputation.Insert(new PCProdItem()
                    {
                        ProdMatlLineNbr = prodMatl.LineID,
                        OperationID = prodMatl.OperationID
                    });
                }
            }

            return adapter.Get();
        }

        /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 8/18/2021 12:50:39 PM
    [PURPOSE]: TO OPEN FORMULA POPUP. SAVE QTY REQUIRED IF CONFIRM IS CLICKED*/
        public PXAction<AMProdItem> ViewFormulation;
        [PXUIField(DisplayName = "Formula")]
        [PXButton]
        public virtual IEnumerable viewFormulation(PXAdapter adapter)
        {
            /*[COMMENT START]
      [DEVELOPER NAME]: JOHN COMBATE
      [DATE TIME]: 8/19/2021 12:51:54 PM
      [PURPOSE]: IF OK IS CLICKED, AUTO COMPUTE AND SAVE ON QTY REQUIRED THE VALUE COMPUTED*/
            PCProdItem prodItem = MaterialComputation.Current;
            if (prodItem == null) return adapter.Get();

            if (MaterialFormula.AskExt() == WebDialogResult.OK)
            {
                DataTable dt = new DataTable();
                decimal? answer = 0m;
                string expression = "";

                List<PCProdCalculation> calcList = MaterialFormula.Select().RowCast<PCProdCalculation>().ToList();
                foreach (PCProdCalculation calc in calcList)
                {
                    if (calcList[calcList.Count - 1] != calc)
                        expression = String.Concat(expression, calc.Value.ToString(), calc.Operation);
                    else
                        expression = String.Concat(expression, calc.Value.ToString());
                }

                answer = Convert.ToDecimal(dt.Compute(expression, ""));

                MaterialComputation.SetValueExt<PCProdItem.qtyRequired>(prodItem, answer);
                MaterialComputation.Update(prodItem);
                Base.Save.Press();
            }

            return adapter.Get();
        }
        #endregion

        #region Event Handlers
        /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 8/18/2021 12:51:05 PM
    [PURPOSE]: SET DEFAULT VALUE*/
        public void PCProdCalculation_ProdCalcID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (PCProdCalculation)e.Row;
            if (row == null) return;

            PCProdItem prodItem = MaterialComputation.Current;
            if (prodItem == null) return;

            AMProdMatl prodMatl = AMProdMatl.PK.Find(Base, row.OrderType, row.ProdOrdID, prodItem.OperationID, prodItem.ProdMatlLineNbr);
            if (prodMatl == null) return;

            PCProdInventoryAnswer answer = PCProdInventoryAnswer.PK.Find(Base, prodMatl.InventoryID, row.ProdCalcID);
            if (answer != null)
            {
                cache.SetValue<PCProdCalculation.value>(row, ProdCalcvalue(answer));
            }
        }

        /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 8/19/2021 1:13:03 PM
    [PURPOSE]: UPDATE IN MATERIALS TAB IF QTYREQUIRED IS UPDATED IN MATERIAL COMPUTATION TAB*/
        public void PCProdItem_QtyRequired_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            var row = (PCProdItem)e.Row;
            if (row == null) return;

            AMProdMatl prodMatl = AMProdMatl.PK.Find(Base, row.OrderType, row.ProdOrdID, row.OperationID, row.ProdMatlLineNbr);
            if (prodMatl == null) return;

            Base.ProdMatlRecords.SetValueExt<AMProdMatl.qtyReq>(prodMatl, row.QtyRequired);
            Base.ProdMatlRecords.Update(prodMatl);
        }

        /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 9/14/2021 11:53:54 AM
    [PURPOSE]: DISABLE FORMULA POPUP IF PROD. ORDER IS IN PROCESS*/
        public void AMProdItem_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (AMProdItem)e.Row;
            if (row == null) return;

            if (
                row.StatusID == ProductionOrderStatus.InProcess ||
                row.StatusID == ProductionOrderStatus.Completed ||
                row.StatusID == ProductionOrderStatus.Cancel ||
                row.StatusID == ProductionOrderStatus.Closed
                )
            {
                LoadPCItems.SetEnabled(false);
                MaterialFormula.Cache.AllowUpdate = MaterialFormula.Cache.AllowInsert = MaterialFormula.Cache.AllowDelete = false;
            }
        }
        #endregion

        #region Methods
        /*[COMMENT START]
    [DEVELOPER NAME]: JOHN COMBATE
    [DATE TIME]: 9/14/2021 11:53:33 AM
    [PURPOSE]: RETURN VALUE BASED ON SPECIAL VALUE SET IN PRODUCTION CALCULATOR ATTRIBUTES*/
        public decimal? ProdCalcvalue(PCProdInventoryAnswer answer)
        {
            if (answer.SpecialValue == PCAttribute.PCAttributeSpecialValue.None || answer.SpecialValue == null)
                return answer.Value;
            else if (answer.SpecialValue == PCAttribute.PCAttributeSpecialValue.SalesOrderQty)
            {
                AMProdItem prodItem = Base.ProdItemRecords.Current;
                if (prodItem != null)
                {
                    SOLine srcLine = SOLine.PK.Find(Base, prodItem.OrdTypeRef, prodItem.OrdNbr, prodItem.OrdLineRef);
                    if (srcLine != null)
                        return srcLine.OrderQty;
                    else
                        return answer.Value;
                }
            }
            else if (answer.SpecialValue == PCAttribute.PCAttributeSpecialValue.ProdOrderQty)
            {
                AMProdItem prodItem = Base.ProdItemRecords.Current;
                if (prodItem != null)
                    return prodItem.QtytoProd;
            }

            return 0m;
        }

        #endregion
    }
}]]></CDATA>
</Graph>