﻿<Graph ClassName="BackloadTransferEntry" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using PX.Data;
using PX.Objects.IN;
using DispatchSetup;
using System.Collections;
using System.Collections.Generic;
using Dispatch;

namespace DispatchExtension
{
    public class BackloadTransferEntry : PXGraph<BackloadTransferEntry>
    {
        public PXSave<BackloadTransfer> Save;
        public PXCancel<BackloadTransfer> Cancel;
        public PXDelete<BackloadTransfer> Delete;

        public PXSetup<DispatchNumberingSetup> Setup;
        public PXSelect<BackloadTransfer> BackloadTransfers;
        public PXSelectJoin<
            BackloadLineTransfer,
            LeftJoin<INTran,
                On<INTran.refNbr, Equal<BackloadLineTransfer.transferNbr>,
                And<INTran.lineNbr, Equal<BackloadLineTransfer.transferLineNbr>>>,
            LeftJoin<INRegister,
                On<INRegister.refNbr, Equal<INTran.refNbr>,
                And<INRegister.docType, Equal<INDocType.transfer>>>>>,
            Where<BackloadLineTransfer.backloadNbr, Equal<Current<BackloadTransfer.backloadNbr>>>>
            Transactions;
        public PXSelectJoin<
            INTran, 
            LeftJoin<INRegister, 
                On<INRegister.docType, Equal<INTran.docType>, 
                And<INRegister.refNbr, Equal<INTran.refNbr>>>>,
            Where<INTranExt.usrBackloadNbr, Equal<Current<BackloadTransfer.backloadNbr>>>> 
            Adjustments;

        public void BackloadLineTransfer_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        {
            var row = (BackloadLineTransfer)e.Row;
            if (row == null) return;

            decimal? availQty = DispatchExtHelper.IntransitQty(this, row.TransferNbr, row.TransferLineNbr) - DispatchExtHelper.QtyInAdjustments(this, row.TransferNbr, row.TransferLineNbr);

            PXTrace.WriteInformation(availQty.ToString());
            if (availQty < 0m)
                cache.RaiseExceptionHandling<BackloadLineTransfer.qty>(row, row.Qty, 
                    new PXSetPropertyException("Qty exceeds available qty for backload."));
        }

        public void BackloadTransfer_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (BackloadTransfer)e.Row;
            if (row == null) return;

            ReleaseDoc.SetEnabled(row.Released == false);
            Transactions.AllowUpdate = row.Released == false;
            BackloadTransfers.Cache.AllowDelete = Transactions.AllowDelete =  row.Released == false;

            PXUIFieldAttribute.SetVisible<BackloadLineTransfer.docNbr>(Transactions.Cache, null, row.Released == true);
        }

        public PXAction<BackloadTransfer> ReleaseDoc;
        [PXButton]
        [PXUIField(DisplayName = "Release")]
        public virtual IEnumerable releaseDoc(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                var doc = BackloadTransfers.Current;
                List<BackloadLineTransfer> backloadForAdjusment = new List<BackloadLineTransfer>();

                foreach (BackloadLineTransfer line in Transactions.Select())
                {
                    BackloadType lineType = PXSelect<
                        BackloadType,
                        Where<BackloadType.backloadTypeCD, Equal<Required<BackloadType.backloadTypeCD>>>>
                        .Select(this, line.BackloadType);
                    if (lineType == null) continue;

                    var lineTypeExt = PXCache<BackloadType>.GetExtension<BackloadTypeExt>(lineType);
                    if (lineTypeExt.UsrCreateAdjustment == true)
                        backloadForAdjusment.Add(line);
                }

                if (backloadForAdjusment.Count > 0)
                {
                    var graph = PXGraph.CreateInstance<INAdjustmentEntry>();
                    INRegister adj = graph.adjustment.Insert(new INRegister()
                    {
                    });

                    foreach (BackloadLineTransfer line in backloadForAdjusment)
                    {
                        INTran transferLine = TransferDocLine(line);

                        var newAdj = graph.transactions.Insert(new INTran()
                        {
                            InventoryID = transferLine.InventoryID,
                            SiteID = transferLine.SiteID,
                            LocationID = transferLine.LocationID,
                            Qty = line.Qty,
                            LotSerialNbr = transferLine.LotSerialNbr
                        });

                        graph.transactions.SetValueExt<INTranExtRCL.usrBackloadNbr>(newAdj, line.BackloadNbr);
                        graph.transactions.SetValueExt<INTranExtRCL.usrBackloadLineNbr>(newAdj, line.LineNbr);
                        graph.transactions.Update(newAdj);

                    }

                    graph.Persist();

                    BackloadTransfers.SetValueExt<BackloadTransfer.released>(BackloadTransfers.Current, true);
                    BackloadTransfers.Update(BackloadTransfers.Current);

                    foreach (BackloadLineTransfer line in backloadForAdjusment)
                    {
                        Transactions.SetValueExt<BackloadLineTransfer.docNbr>(line, adj.RefNbr);
                        Transactions.Update(line);
                    }
                    Persist();
                }
            });

            return adapter.Get();
        }

        public INTran TransferDocLine(BackloadLineTransfer line)
        {
            return PXSelect<
                INTran,
                Where<INTran.refNbr, Equal<Required<INTran.refNbr>>,
                    And<INTran.lineNbr, Equal<Required<INTran.lineNbr>>,
                    And<INTran.docType, Equal<INDocType.transfer>>>>>
                .Select(this, line.TransferNbr, line.TransferLineNbr);

        }
        
        public PXAction<BackloadTransfer> GoToDoc;
        [PXUIField(DisplayName = "")]
        [PXButton]
        public void goToDoc()
        {
            var graph = PXGraph.CreateInstance<INAdjustmentEntry>();

            INRegister adjustment = graph.adjustment.Search<INRegister.docType, INRegister.refNbr>(INDocType.Adjustment, Transactions.Current.DocNbr);
            if (adjustment != null)
                throw new PXRedirectRequiredException(graph, null);
        }
    }
}]]></CDATA>
</Graph>