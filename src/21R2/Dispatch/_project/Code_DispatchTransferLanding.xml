﻿<Graph ClassName="DispatchTransferLanding" Source="#CDATA" IsNew="True" FileType="NewGraph">
    <CDATA name="Source"><![CDATA[using System;
using System.Collections.Generic;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.IN;

namespace DispatchExtension
{
    public class DispatchTransferLanding : PXGraph<DispatchTransferLanding>
    {
        public PXCancel<TransferDetails> Cancel;
        public PXFilter<TransferFilter> Filter;

        public PXProcessing<
            TransferDetails,
            Where2<
                Where<TransferDetails.status, Equal<INDocStatus.balanced>,
                    And<TransferDetails.docType, Equal<INDocType.transfer>,
                    And<TransferDetails.usrDispatchType, IsNull>>>,
                And2<
                    Where<TransferDetails.tranDate, GreaterEqual<Current<TransferFilter.dateFrom>>, 
                        Or<Current<TransferFilter.dateFrom>, IsNull>>,
                    And2<
                        Where<TransferDetails.tranDate, LessEqual<Current<TransferFilter.dateTo>>,
                            Or<Current<TransferFilter.dateTo>, IsNull>>, 
                        And2<
                            Where<TransferDetails.siteID, Equal<Current<TransferFilter.srcSiteID>>, 
                                Or<Current<TransferFilter.srcSiteID>, IsNull>>, 
                            And2<
                                Where<TransferDetails.toSiteID, Equal<Current<TransferFilter.destSiteID>>, 
                                    Or<Current<TransferFilter.destSiteID>, IsNull>>, 
                                And<Where<TransferDetails.refNbr, Equal<Current<TransferFilter.transferNbr>>, 
                                    Or<Current<TransferFilter.transferNbr>, IsNull>>>>>>>>>
            DispatchTransfers;

        public PXSelect<INTran> Transfers;
        public DispatchTransferLanding()
        {
            DispatchTransfers.SetProcessDelegate(SetDispatchType);
        }

        public static void SetDispatchType(List<TransferDetails> transactions)
        {
            foreach (TransferDetails tran in transactions)
            {
                PXTrace.WriteInformation(tran.ActionLineItem);

                PXDatabase.Update<INTran>(
                                new PXDataFieldAssign<INTranExtRCL.usrDispatchType>(tran.ActionLineItem),
                                new PXDataFieldRestrict<INTran.docType>(INDocType.Transfer),
                                new PXDataFieldRestrict<INTran.refNbr>(tran.RefNbr),
                                new PXDataFieldRestrict<INTran.lineNbr>(tran.LineNbr)
                                );
            }
        }

        public void TransferDetails_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (TransferDetails)e.Row;
            if (row == null) return;

            PXUIFieldAttribute.SetEnabled<TransferDetails.actionLineItem>(cache, null, true);
        }

        [PXProjection(typeof(Select2<
            INTran,
            InnerJoin<INRegister,
                On<INRegister.docType, Equal<INTran.docType>,
                And<INRegister.refNbr, Equal<INTran.refNbr>>>>>))]
        [Serializable]
        public class TransferDetails : IBqlTable, ILSMaster
        {
            #region Selected
            public abstract class selected : IBqlField { }
            [PXBool()]
            [PXUIField(DisplayName = "Selected")]
            [PXDefault(false, PersistingCheck = PXPersistingCheck.Nothing)]
            public virtual bool? Selected { get; set; }
            #endregion

            #region ActionLineItem
            public abstract class actionLineItem : PX.Data.IBqlField { }
            [PXString(1)]
            [PXUIField(DisplayName = "Action")]
            [dispatchType.List]
            public virtual string ActionLineItem { get; set; }
            #endregion

            #region DispatchType
            public abstract class usrDispatchType : PX.Data.IBqlField { }
            [PXDBString(BqlField = typeof(INTranExtRCL.usrDispatchType))]
            [PXUIField(DisplayName = "Dispatch Type")]
            public virtual string UsrDispatchType { get; set; }
            #endregion

            #region DocType
            public abstract class docType : PX.Data.IBqlField { }
            [PXDBString(BqlField = typeof(INTran.docType), IsKey = true)]
            [PXUIField(DisplayName = "Action")]
            public virtual string DocType { get; set; }
            #endregion

            #region TransferType
            public abstract class transferType : PX.Data.BQL.BqlString.Field<transferType> { }
            protected String _TransferType;
            [PXDBString(1, IsFixed = true, BqlField = typeof(INRegister.transferType))]
            [INTransferType.List()]
            [PXUIField(DisplayName = "Transfer Type")]
            public virtual String TransferType
            {
                get
                {
                    return this._TransferType;
                }
                set
                {
                    this._TransferType = value;
                }
            }
            #endregion
            
            #region TransferNbr
            [PXDBString(BqlField = typeof(INRegister.refNbr), IsKey = true)]
            [PXSelector(typeof(Search<
                INRegister.refNbr,
                Where<INRegister.docType, Equal<INDocType.transfer>>>))]
            [PXUIField(DisplayName = "Transfer Reference Nbr")]
            public virtual string RefNbr { get; set; }
            public abstract class refNbr : IBqlField { }
            #endregion

            #region LineNbr
            public abstract class lineNbr : PX.Data.IBqlField { }
            [PXDBInt(BqlField = typeof(INTran.lineNbr), IsKey = true)]
            [PXUIField(DisplayName = "Line Nbr.")]
            public virtual int? LineNbr { get; set; }
            #endregion

            #region Status
            public abstract class status : PX.Data.BQL.BqlString.Field<status> { }
            protected String _Status;
            [PXDBString(1, IsFixed = true, BqlField = typeof(INRegister.status))]
            [PXDefault()]
            [PXUIField(DisplayName = "Status", Visibility = PXUIVisibility.SelectorVisible, Enabled = false)]
            [INDocStatus.List()]
            public virtual String Status { get; set; }
            #endregion

            #region Date
            [PXDBDate(BqlField = typeof(INRegister.tranDate))]
            [PXUIField(DisplayName = "Date")]
            public virtual DateTime? TranDate { get; set; }
            public abstract class tranDate : IBqlField { }
            #endregion

            #region SiteID
            [Site(BqlField = typeof(INRegister.siteID))]
            [PXUIField(DisplayName = "Source Warehouse")]
            public virtual int? SiteID { get; set; }
            public abstract class siteID : IBqlField { }
            #endregion

            #region ToSiteID
            [Site(BqlField = typeof(INRegister.toSiteID))]
            [PXUIField(DisplayName = "Destination Warehouse")]
            public virtual int? ToSiteID { get; set; }
            public abstract class toSiteID : IBqlField { }
            #endregion

            #region InventoryID
            protected Int32? _InventoryID;
            [StockItem(DisplayName = "Inventory ID", BqlField = typeof(INTran.inventoryID))]
            public virtual Int32? InventoryID
            {
                get
                {
                    return this._InventoryID;
                }
                set
                {
                    this._InventoryID = value;
                }
            }
            #endregion

            #region TranDesc
            public abstract class tranDesc : PX.Data.BQL.BqlString.Field<tranDesc> { }
            protected String _TranDesc;
            [PXDBString(256, IsUnicode = true, BqlField = typeof(INTran.tranDesc))]
            [PXUIField(DisplayName = "Description")]
            public virtual String TranDesc
            {
                get
                {
                    return this._TranDesc;
                }
                set
                {
                    this._TranDesc = value;
                }
            }
            #endregion

            #region LocationID
            public abstract class locationID : PX.Data.BQL.BqlInt.Field<locationID> { }
            protected Int32? _LocationID;
            [PX.Objects.IN.LocationAvail(typeof(INTran.inventoryID), typeof(INTran.subItemID), typeof(INTran.siteID), typeof(INTran.tranType), typeof(INTran.invtMult), BqlField = typeof(INTran.locationID))]
            public virtual Int32? LocationID
            {
                get
                {
                    return this._LocationID;
                }
                set
                {
                    this._LocationID = value;
                }
            }
            #endregion
            
            #region ToLocationID
            public abstract class toLocationID : PX.Data.BQL.BqlInt.Field<toLocationID> { }
            protected Int32? _ToLocationID;
            [PX.Objects.IN.LocationAvail(typeof(INTran.inventoryID), typeof(INTran.subItemID), typeof(INTran.toSiteID), false, false, true, DisplayName = "To Location ID", BqlField = typeof(INTran.toLocationID))]
            public virtual Int32? ToLocationID
            {
                get
                {
                    return this._ToLocationID;
                }
                set
                {
                    this._ToLocationID = value;
                }
            }
            #endregion

            #region UOM
            public abstract class uOM : PX.Data.BQL.BqlString.Field<uOM> { }
            protected String _UOM;
            [PXDefault()]
            [INUnit(typeof(INTran.inventoryID), BqlField = typeof(INTran.uOM))]
            public virtual String UOM
            {
                get
                {
                    return this._UOM;
                }
                set
                {
                    this._UOM = value;
                }
            }
            #endregion

            #region Qty
            public abstract class qty : PX.Data.BQL.BqlDecimal.Field<qty> { }
            protected Decimal? _Qty;
            [PXDBQuantity(typeof(INTran.uOM), typeof(INTran.baseQty), InventoryUnitType.BaseUnit, BqlField = typeof(INTran.qty))]
            [PXUIField(DisplayName = "Quantity")]
            public virtual Decimal? Qty
            {
                get
                {
                    return this._Qty;
                }
                set
                {
                    this._Qty = value;
                }
            }
            #endregion

#region IsIntercompany
        [PXBool()]
    [PXUIField(DisplayName = "Is Intercompany")]
    public virtual bool? IsIntercompany{ get; set; }
    public abstract class isIntercompany : PX.Data.BQL.BqlBool.Field<isIntercompany> { }
        #endregion
      
            #region LotSerialNbr
            public abstract class lotSerialNbr : PX.Data.BQL.BqlString.Field<lotSerialNbr> { }
            protected String _LotSerialNbr;
            [INLotSerialNbr(typeof(INTran.inventoryID), typeof(INTran.subItemID), typeof(INTran.locationID), PersistingCheck = PXPersistingCheck.Nothing, FieldClass = "LotSerial", BqlField = typeof(INTran.lotSerialNbr))]
            public virtual String LotSerialNbr
            {
                get
                {
                    return this._LotSerialNbr;
                }
                set
                {
                    this._LotSerialNbr = value;
                }
            }
            #endregion

            /* ILSMASTER */
            #region TranType
            public abstract class tranType : PX.Data.BQL.BqlString.Field<tranType> { }
            protected String _TranType;
            [PXDBString(3, IsFixed = true, BqlField = typeof(INTran.tranType))]
            [PXDefault()]
            [INTranType.List()]
            [PXUIField(DisplayName = "Tran. Type")]
            public virtual String TranType
            {
                get
                {
                    return this._TranType;
                }
                set
                {
                    this._TranType = value;
                }
            }
            #endregion
                
            #region InvtMult
            public abstract class invtMult : PX.Data.BQL.BqlShort.Field<invtMult> { }
            protected Int16? _InvtMult;
            [PXDBShort(BqlField = typeof(INTran.invtMult))]
            [PXDefault()]
            [PXUIField(DisplayName = "Multiplier")]
            public virtual Int16? InvtMult
            {
                get
                {
                    return this._InvtMult;
                }
                set
                {
                    this._InvtMult = value;
                }
            }
            #endregion
            #region SubItemID
            public abstract class subItemID : PX.Data.BQL.BqlInt.Field<subItemID> { }
            protected Int32? _SubItemID;
            [PXDefault(typeof(Search<
                InventoryItem.defaultSubItemID,
                Where<InventoryItem.inventoryID, Equal<Current<INTran.inventoryID>>,
                    And<InventoryItem.defaultSubItemOnEntry, Equal<boolTrue>>>>),
                PersistingCheck = PXPersistingCheck.Nothing)]
            [PX.Objects.IN.SubItem(
                typeof(INTran.inventoryID),
                typeof(LeftJoin<INSiteStatus,
                    On<INSiteStatus.subItemID, Equal<INSubItem.subItemID>,
                    And<INSiteStatus.inventoryID, Equal<Optional<INTran.inventoryID>>,
                    And<INSiteStatus.siteID, Equal<Optional<INTran.siteID>>>>>>), BqlField = typeof(INTran.subItemID))]
            [PXFormula(typeof(Default<INTran.inventoryID>))]
            public virtual Int32? SubItemID
            {
                get
                {
                    return this._SubItemID;
                }
                set
                {
                    this._SubItemID = value;
                }
            }
            #endregion
            #region ExpireDate
            public abstract class expireDate : PX.Data.BQL.BqlDateTime.Field<expireDate> { }
            protected DateTime? _ExpireDate;
            [INExpireDate(typeof(INTran.inventoryID), PersistingCheck = PXPersistingCheck.Nothing, FieldClass = "LotSerial", BqlField = typeof(INTran.expireDate))]
            public virtual DateTime? ExpireDate
            {
                get
                {
                    return this._ExpireDate;
                }
                set
                {
                    this._ExpireDate = value;
                }
            }
            #endregion
            #region BaseQty
            public abstract class baseQty : PX.Data.BQL.BqlDecimal.Field<baseQty> { }
            protected Decimal? _BaseQty;
            [PXDBQuantity(BqlField = typeof(INTran.baseQty))]
            [PXDefault(TypeCode.Decimal, "0.0")]
            public virtual Decimal? BaseQty
            {
                get
                {
                    return this._BaseQty;
                }
                set
                {
                    this._BaseQty = value;
                }
            }
            #endregion
            #region ProjectID
            public abstract class projectID : PX.Data.BQL.BqlInt.Field<projectID> { }
            protected int? _ProjectID;
            public virtual int? ProjectID
            {
                get
                {
                    return this._ProjectID;
                }
                set
                {
                    this._ProjectID = value;
                }
            }
            #endregion
            #region TaskID
            protected Int32? _TaskID;
            public virtual Int32? TaskID { get; set; }
            public abstract class taskID : PX.Data.BQL.BqlInt.Field<taskID> { }
            #endregion
        }
  }
}]]></CDATA>
</Graph>