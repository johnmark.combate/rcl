<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="PL302000.aspx.cs" Inherits="Page_PL302000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.PLPickListTransferEntry"
        PrimaryView="PickTransfers"
        >
		<CallbackCommands>
			<px:PXDSCallbackCommand Name="AddTransfers" Visible="False" CommitChanges="True" /></CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="PickTransfers" Width="100%" Height="100px" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
			<px:PXLayoutRule ControlSize="S" LabelsWidth="S" runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector6" DataField="PickListNbr" ></px:PXSelector>
			<px:PXDropDown runat="server" ID="CstPXDropDown9" DataField="Status" />
			<px:PXLayoutRule runat="server" ID="CstLayoutRule8" ColumnSpan="2" ></px:PXLayoutRule>
			<px:PXTextEdit runat="server" ID="CstPXTextEdit4" DataField="Description" ></px:PXTextEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule2" StartColumn="True" ></px:PXLayoutRule>
			<px:PXSelector runat="server" ID="CstPXSelector3" DataField="CreatedByID" ></px:PXSelector>
			<px:PXDateTimeEdit runat="server" ID="CstPXDateTimeEdit5" DataField="PickDate" ></px:PXDateTimeEdit></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Details" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="PickTransferLines">
			    <Columns>
				<px:PXGridColumn DataField="INRegister__TransferType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="INTransferEntry" CommitChanges="True" DataField="INTranNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TranDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__ToSiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__LocationID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__ToLocationID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__Qty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__InventoryID" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__TranDesc" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INTran__UsrOpenPickQty" Width="100" />
				<px:PXGridColumn CommitChanges="True" DataField="PickQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="Picker" Width="220" ></px:PXGridColumn>
				<px:PXGridColumn DataField="Picker_description" Width="220" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		
			<CustomItems>
				<px:PXToolBarButton>
					<AutoCallBack Target="ds" Command="AddTransfers" /></px:PXToolBarButton></CustomItems></ActionBar>
	
		<Mode AllowAddNew="False" AutoInsert="False" ></Mode></px:PXGrid>
	<px:PXSmartPanel Height="500" Key="Transfers" Width="800" AutoCallBack-Command="Refresh" AutoCallBack-Enabled="True" AutoCallBack-Target="CstPXGrid11" Caption="Add Transfers" CaptionVisible="True" runat="server" ID="CstSmartPanel10">
		<px:PXGrid SkinID="Inquire" Width="100%" runat="server" ID="CstPXGrid11">
			<Levels>
				<px:PXGridLevel DataMember="Transfers" >
					<Columns>
						<px:PXGridColumn Type="CheckBox" TextAlign="Center" DataField="Selected" Width="30" AllowCheckAll="True" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__DocType" Width="70" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__RefNbr" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="InventoryID" Width="70" LinkCommand="InventoryMaint" ></px:PXGridColumn>
						<px:PXGridColumn DataField="TranDesc" Width="280" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__TranDate" Width="90" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__ToSiteID" Width="140" ></px:PXGridColumn>
						<px:PXGridColumn DataField="Qty" Width="100" ></px:PXGridColumn>
						<px:PXGridColumn DataField="UsrOpenPickQty" Width="100" />
						<px:PXGridColumn DataField="INRegister__ExtRefNbr" Width="180" ></px:PXGridColumn>
						<px:PXGridColumn DataField="INRegister__TransferType" Width="70" ></px:PXGridColumn></Columns></px:PXGridLevel></Levels>
			<AutoSize Enabled="True" ></AutoSize>
			<AutoSize MinHeight="100" ></AutoSize>
			<AutoSize MinWidth="100" ></AutoSize>
			<ActionBar>
				<CustomItems></CustomItems></ActionBar></px:PXGrid>
		<px:PXPanel SkinID="Buttons" runat="server" ID="CstPanel12">
			<px:PXButton DialogResult="OK" Text="OK" runat="server" ID="CstButton13" ></px:PXButton>
			<px:PXButton DialogResult="Cancel" Text="Cancel" runat="server" ID="CstButton14" ></px:PXButton></px:PXPanel></px:PXSmartPanel></asp:Content>
