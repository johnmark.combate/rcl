<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="LP501040.aspx.cs" Inherits="Page_LP501040" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.LoadPlanTransferProcess"
        PrimaryView="Transfers"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Filter" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXSelector runat="server" DataField="TransferNbr" CommitChanges="True" ID="CstPXSelector3" ></px:PXSelector>
			<px:PXSegmentMask runat="server" DataField="SrcSiteID" CommitChanges="True" ID="CstPXSegmentMask5" ></px:PXSegmentMask>
			<px:PXSegmentMask runat="server" DataField="DestSiteID" CommitChanges="True" ID="CstPXSegmentMask4" ></px:PXSegmentMask>
			<px:PXLayoutRule runat="server" StartColumn="True" ID="CstPXLayoutRule2" ></px:PXLayoutRule>
			<px:PXDateTimeEdit runat="server" CommitChanges="True" DataField="DateFrom" ID="CstPXDateTimeEdit6" ></px:PXDateTimeEdit>
			<px:PXDateTimeEdit runat="server" CommitChanges="True" DataField="DateTo" ID="CstPXDateTimeEdit7" ></px:PXDateTimeEdit>
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule13" StartRow="True" />
			<px:PXLayoutRule runat="server" ID="CstPXLayoutRule1" StartColumn="True" ></px:PXLayoutRule>
			<px:PXFormView RenderStyle="Fieldset" CaptionVisible="True" Caption="Truck Info" runat="server" ID="CstFormView2" SkinID="" DataMember="CurrentTruck" >
				<Template>
					<px:PXLayoutRule ControlSize="SM" LabelsWidth="M" runat="server" ID="CstPXLayoutRule3" StartColumn="True" ></px:PXLayoutRule>
					<px:PXSelector CommitChanges="True" runat="server" ID="CstPXSelector12" DataField="TruckID" ></px:PXSelector>
					<px:PXSelector runat="server" ID="CstPXSelector14" DataField="ContainerNo" />
					<px:PXNumberEdit runat="server" ID="CstPXNumberEdit11" DataField="TotalTruckCapacityKGS" ></px:PXNumberEdit>
					<px:PXNumberEdit runat="server" ID="CstPXNumberEdit9" DataField="RemTruckBalKGS" ></px:PXNumberEdit>
					<px:PXNumberEdit runat="server" ID="CstPXNumberEdit6" DataField="CurrentCapacityKGS" ></px:PXNumberEdit>
					<px:PXLayoutRule ControlSize="SM" LabelsWidth="M" runat="server" ID="CstPXLayoutRule4" StartColumn="True" ></px:PXLayoutRule>
					<px:PXTextEdit runat="server" ID="CstPXTextEdit7" DataField="DriverName" ></px:PXTextEdit>
					<px:PXNumberEdit runat="server" ID="CstPXNumberEdit10" DataField="TotalTruckCapacityCBM" ></px:PXNumberEdit>
					<px:PXNumberEdit runat="server" ID="CstPXNumberEdit8" DataField="RemTruckBalCBM" ></px:PXNumberEdit>
					<px:PXNumberEdit runat="server" ID="CstPXNumberEdit5" DataField="CurrentCapacityCBM" ></px:PXNumberEdit></Template></px:PXFormView></Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="PrimaryInquire" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="Transfers">
			    <Columns>
				<px:PXGridColumn CommitChanges="True" Type="CheckBox" TextAlign="Center" DataField="Selected" Width="30" AllowCheckAll="True" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TransferType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__RefNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn CommitChanges="True" DataField="UsrLoadQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="InventoryID" Width="70" LinkCommand="InventoryMaint" ></px:PXGridColumn>
				<px:PXGridColumn DataField="TranDesc" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="UsrAvailLoadQty" Width="100" />
				<px:PXGridColumn DataField="Qty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__TranDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__SiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__ToSiteID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__ExtRefNbr" Width="180" ></px:PXGridColumn>
				<px:PXGridColumn DataField="INRegister__Status" Width="70" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" ></AutoSize>
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
