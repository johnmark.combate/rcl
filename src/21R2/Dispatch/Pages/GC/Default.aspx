﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Pages_GC_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

<link href='fullcalendar/core/main.css' rel='stylesheet' />
<link href='fullcalendar/daygrid/main.css' rel='stylesheet' />
<link href='fullcalendar/timegrid/main.css' rel='stylesheet' />
<script src="fullcalendar/jquery-3.5.1.min.js" type="text/javascript"></script>
 
<script src="fullcalendar/moment.js" type="text/javascript"></script>

<script src='fullcalendar/core/main.js'></script>
<script src='fullcalendar/daygrid/main.js'></script>
<script src='fullcalendar/timegrid/main.js'></script>

</head>
<body>
    <div>
       
        <label for="DispatchStatus">Status:</label>
        <select id="DispatchStatus">
            <option value="0"> - Select -</option>
            <option value="1">Pending Delivery</option>
            <option value="2">Scheduled Delivery</option>
            <option value="3">Actual Delivery</option>
            </select>
    </div>
    <div>
        <label for="DispatchFilters">Filter by:</label>
        <select id="DispatchFilters">
            <option value="0"> - Select - </option>
            <option value="1">Customer Name</option>
            <option value="2">Truck</option>
            <option value="3">Driver</option>
            <option value="4">Shipping Type</option>
            <option value="5">Area</option>
            <option value="6">OrderNbr</option>
            <option value="7">InvoiceNbr</option>
        </select>
        <label for="DispatchValue">Value: </label>
        <select id="DispatchValue">
            <option value="0">- Select -</option>
        </select>
    </div>

    <br />

    <div id="calendar"></div>

   <script type="text/javascript">
      $(document).ready(function (){
           var calendarEl = document.getElementById('calendar');
           var calendar = new FullCalendar.Calendar(calendarEl, {
               plugins: ['dayGrid','dayGridWeek'],
               defaultView: 'dayGridMonth',
               header: {
                   left: 'prev,next today',
                   center: 'title',
                   right: 'dayGridMonth,dayGridWeek'
               }
           });

           calendar.render();
           $("#Button1").click(function (event) {
               calendar.today();
           });
          
          $(function () {
              var strUrl = "http://acumatica.cwhomedepot.com/CWDev/OData/DispatchForecastingCalendar?$format=json";
              var statusDefaultValue = 0
                  $.ajax({
                      type: "GET",
                      beforeSend: function (xhr) {
                          xhr.setRequestHeader("Authorization", "Basic " + btoa("admin" + ":" + "p@ssw0rd"));
                      },
                      url: strUrl,
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      success: function (data) {
                          var values = data["value"];
                          //#region GroupByCustomerOdataValue
                          var groupBy = function (xs, key) {
                              return xs.reduce(function (rv, x) {
                                  (rv[x[key]] = rv[x[key]] || []).push(x);
                                  return rv;
                              }, {});
                          };

                          document.getElementById("DispatchFilters").onchange = function () {

                              $('#DispatchValue').empty().append("<option value=0>" + "- Select -" + "</option>")
                              var e = document.getElementById("DispatchFilters");
                              var str = '';

                              var filterDefault = e.options[e.selectedIndex].value;
                              if (filterDefault == 0) {
                                  str = '';
                              }
                              else if (filterDefault == 1) {
                                  str = 'CustomerName'
                              } else if (filterDefault == 2) {
                                  str = 'Truck'
                              } else if (filterDefault == 3) {
                                  str = 'Driver'
                              } else if (filterDefault == 4) {
                                  str = 'ShippingType'
                              } else if (filterDefault == 5) {
                                  str = '';
                              } else if (filterDefault == 6) {
                                  str = 'OrderNbr';
                              } else if (filterDefault == 7) {
                                  str = 'InvoiceNbr';
                              }
                              var filterValue = groupBy(values, str);
                              Object.keys(filterValue).forEach(function (category) {
                                  $("#DispatchValue").append("<option value='" + category + "'>" + category + "</option>");
                                  $("#DispatchValue option:contains(undefined)").remove();
                                  $("#DispatchValue option:contains(null)").remove();
                              });

                          }

                          //#endregion
                          values.forEach(function (item, index) {
                              var test = {
                                  title: item["OrderNbr"] + " " + item["CustomerName"],
                                  start: item["ForecastDate"],
                                  allDay: true
                              }

                              calendar.addEvent(test);

                          });
                      },
                      error: null
                  });
              
                document.getElementById("DispatchStatus").onchange = function () {
                    var e = document.getElementById("DispatchStatus");
                    //var statusUsr = e.options[e.selectedIndex].text;
                    var statusDefault = e.options[e.selectedIndex].value;

                    var statusIdUrl = "http://acumatica.cwhomedepot.com/CWDev/OData/DispatchForecastingCalendar?$format=json";
                    if (statusDefault == 0) {
                        statusIdUrl = statusIdUrl;
                        console.log(statusIdUrl);
                    } else if (statusDefault == 1) {
                        statusIdUrl += "&$filter=Status eq 'Pending'";
                        console.log(statusIdUrl);
                        $.ajax({
                            type: "GET",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Authorization", "Basic " + btoa("admin" + ":" + "p@ssw0rd"));
                            },
                            url: statusIdUrl,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var values = data["value"];
                                values.forEach(function (item, index) {

                                    var test = {
                                        title: item["OrderNbr"] + " " + item["CustomerName"],
                                        start: item["RequestedOn"],
                                        //end: item["ForeastDate"]
                                        allDay: true
                                    }
                                    calendar.addEvent(test);

                                });
                            },
                            error: null
                        });
                    } else if (statusDefault == 2) {
                        statusIdUrl += "&$filter=ForecastDate ne null&$filter=ShipmentNbr eq null";
                    } else if (statusDefault == 3) {
                        statusIdUrl += "&$filter=ShipmentNbr ne null&$filter=ShipmentStatus eq 'Completed'";
                    }
                    var tempEvents = calendar.getEvents();
                    for (var x = 0; x < tempEvents.length; x++) {
                        tempEvents[x].remove();
                    }

                    if (statusDefault != 1) {
                        $.ajax({
                            type: "GET",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Authorization", "Basic " + btoa("admin" + ":" + "p@ssw0rd"));
                            },
                            url: statusIdUrl,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var values = data["value"];
                                values.forEach(function (item, index) {

                                    var test = {
                                        title: item["OrderNbr"] + " " + item["CustomerName"],
                                        start: item["ForecastDate"],
                                        //end: item["ForeastDate"]
                                        allDay: true
                                    }

                                    calendar.addEvent(test);

                                });
                            },
                            error: null
                        });
                    }
                    

              }
              document.getElementById("DispatchValue").onchange = function () {
                  var e = document.getElementById("DispatchValue");
                  var eStatus = document.getElementById("DispatchFilters");
                  var eStatusStr = eStatus.options[eStatus.selectedIndex].text.replace(/ /g,'');
                  var eValueDefault = e.options[e.selectedIndex].value;
                  var eStr = e.options[e.selectedIndex].text;
                  var idUrl = "http://acumatica.cwhomedepot.com/CWDev/OData/DispatchForecastingCalendar?$format=json";
                  console.log(eStatusStr);
                  var tempEvents = calendar.getEvents();
                  for (var x = 0; x < tempEvents.length; x++) {
                      tempEvents[x].remove();
                  }
                  idUrl += "&$filter="+ eStatusStr +" eq '" + eStr + "'";
                  console.log(idUrl);
                  $.ajax({
                      type: "GET",
                      beforeSend: function (xhr) {
                          xhr.setRequestHeader("Authorization", "Basic " + btoa("admin" + ":" + "p@ssw0rd"));
                      },
                      url: idUrl,
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      success: function (data) {
                          var values = data["value"];
                          values.forEach(function (item, index) {

                              var test = {
                                  title: item["OrderNbr"] + " " + item["CustomerName"],
                                  start: item["ForecastDate"],
                                  allDay: true
                              }

                              calendar.addEvent(test);

                          });
                      },
                      error: null
                  });
              }
            });

      });

    </script>
</body>
</html>
