<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="DP301100.aspx.cs" Inherits="Page_DP301100" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%"
        TypeName="DispatchExtension.SOScheduling"
        PrimaryView="SalesOrders"
        >
		<CallbackCommands>

		</CallbackCommands>
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" DataMember="Filter" Width="100%" Height="" AllowAutoHide="false">
		<Template>
			<px:PXLayoutRule ID="PXLayoutRule1" runat="server" StartRow="True"></px:PXLayoutRule>
		</Template>
	</px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" Runat="Server">
	<px:PXGrid ID="grid" runat="server" DataSourceID="ds" Width="100%" Height="150px" SkinID="Details" AllowAutoHide="false">
		<Levels>
			<px:PXGridLevel DataMember="SalesOrders">
			    <Columns>
				<px:PXGridColumn Type="CheckBox" AllowCheckAll="True" TextAlign="Center" DataField="UsrSelected" Width="30" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__OrderType" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="SOOrderEntry" DataField="SOOrder__OrderNbr" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="UsrForeCastDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__Status" Width="70" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__OrderDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn DataField="RequestDate" Width="90" ></px:PXGridColumn>
				<px:PXGridColumn LinkCommand="CustomerMaint" DataField="SOOrder__CustomerID" Width="140" ></px:PXGridColumn>
				<px:PXGridColumn DataField="SOOrder__CustomerID_BAccountR_acctName" Width="280" ></px:PXGridColumn>
				<px:PXGridColumn DataField="OrderQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="OpenQty" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CuryUnitPrice" Width="100" ></px:PXGridColumn>
				<px:PXGridColumn DataField="CuryLineAmt" Width="100" ></px:PXGridColumn></Columns>
			</px:PXGridLevel>
		</Levels>
		<AutoSize Container="Window" Enabled="True" MinHeight="150" />
		<ActionBar >
		</ActionBar>
	</px:PXGrid>
</asp:Content>
